-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Oct 30, 2022 at 08:04 PM
-- Server version: 10.3.36-MariaDB-cll-lve
-- PHP Version: 7.4.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `blindpe1_titi`
--

-- --------------------------------------------------------

--
-- Table structure for table `auth_group`
--

CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `auth_group_permissions`
--

CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `auth_permission`
--

CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `auth_permission`
--

INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES
(1, 'Can add log entry', 1, 'add_logentry'),
(2, 'Can change log entry', 1, 'change_logentry'),
(3, 'Can delete log entry', 1, 'delete_logentry'),
(4, 'Can view log entry', 1, 'view_logentry'),
(5, 'Can add permission', 2, 'add_permission'),
(6, 'Can change permission', 2, 'change_permission'),
(7, 'Can delete permission', 2, 'delete_permission'),
(8, 'Can view permission', 2, 'view_permission'),
(9, 'Can add group', 3, 'add_group'),
(10, 'Can change group', 3, 'change_group'),
(11, 'Can delete group', 3, 'delete_group'),
(12, 'Can view group', 3, 'view_group'),
(13, 'Can add content type', 4, 'add_contenttype'),
(14, 'Can change content type', 4, 'change_contenttype'),
(15, 'Can delete content type', 4, 'delete_contenttype'),
(16, 'Can view content type', 4, 'view_contenttype'),
(17, 'Can add session', 5, 'add_session'),
(18, 'Can change session', 5, 'change_session'),
(19, 'Can delete session', 5, 'delete_session'),
(20, 'Can view session', 5, 'view_session'),
(21, 'Can add attachment', 6, 'add_attachment'),
(22, 'Can change attachment', 6, 'change_attachment'),
(23, 'Can delete attachment', 6, 'delete_attachment'),
(24, 'Can view attachment', 6, 'view_attachment'),
(25, 'Can add site', 7, 'add_site'),
(26, 'Can change site', 7, 'change_site'),
(27, 'Can delete site', 7, 'delete_site'),
(28, 'Can view site', 7, 'view_site'),
(29, 'Can add sequence', 8, 'add_sequence'),
(30, 'Can change sequence', 8, 'change_sequence'),
(31, 'Can delete sequence', 8, 'delete_sequence'),
(32, 'Can view sequence', 8, 'view_sequence'),
(33, 'Can add user', 9, 'add_user'),
(34, 'Can change user', 9, 'change_user'),
(35, 'Can delete user', 9, 'delete_user'),
(36, 'Can view user', 9, 'view_user'),
(37, 'Can add print card', 10, 'add_printcard'),
(38, 'Can change print card', 10, 'change_printcard'),
(39, 'Can delete print card', 10, 'delete_printcard'),
(40, 'Can view print card', 10, 'view_printcard'),
(41, 'Can add provinsi', 11, 'add_provinsi'),
(42, 'Can change provinsi', 11, 'change_provinsi'),
(43, 'Can delete provinsi', 11, 'delete_provinsi'),
(44, 'Can view provinsi', 11, 'view_provinsi'),
(45, 'Can add kabupaten', 12, 'add_kabupaten'),
(46, 'Can change kabupaten', 12, 'change_kabupaten'),
(47, 'Can delete kabupaten', 12, 'delete_kabupaten'),
(48, 'Can view kabupaten', 12, 'view_kabupaten'),
(49, 'Can add kecamatan', 13, 'add_kecamatan'),
(50, 'Can change kecamatan', 13, 'change_kecamatan'),
(51, 'Can delete kecamatan', 13, 'delete_kecamatan'),
(52, 'Can view kecamatan', 13, 'view_kecamatan'),
(53, 'Can add kelurahan', 14, 'add_kelurahan'),
(54, 'Can change kelurahan', 14, 'change_kelurahan'),
(55, 'Can delete kelurahan', 14, 'delete_kelurahan'),
(56, 'Can view kelurahan', 14, 'view_kelurahan');

-- --------------------------------------------------------

--
-- Table structure for table `django_admin_log`
--

CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext DEFAULT NULL,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) UNSIGNED NOT NULL CHECK (`action_flag` >= 0),
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `django_admin_log`
--

INSERT INTO `django_admin_log` (`id`, `action_time`, `object_id`, `object_repr`, `action_flag`, `change_message`, `content_type_id`, `user_id`) VALUES
(1, '2022-09-03 16:32:00.159696', '32676', 'Jawa Tengah', 1, 'new through import_export', 11, 1),
(2, '2022-09-03 16:32:25.702084', '41165', 'Brebes', 1, 'new through import_export', 12, 1),
(3, '2022-09-03 16:32:38.517745', '34', 'Wanasari', 1, 'new through import_export', 13, 1),
(4, '2022-09-03 16:32:38.519117', '33', 'Tonjong', 1, 'new through import_export', 13, 1),
(5, '2022-09-03 16:32:38.520137', '32', 'Tanjung', 1, 'new through import_export', 13, 1),
(6, '2022-09-03 16:32:38.520938', '31', 'Songgom', 1, 'new through import_export', 13, 1),
(7, '2022-09-03 16:32:38.521749', '30', 'Sirampog', 1, 'new through import_export', 13, 1),
(8, '2022-09-03 16:32:38.522445', '29', 'Salem', 1, 'new through import_export', 13, 1),
(9, '2022-09-03 16:32:38.523198', '28', 'Paguyangan', 1, 'new through import_export', 13, 1),
(10, '2022-09-03 16:32:38.523971', '27', 'Losari', 1, 'new through import_export', 13, 1),
(11, '2022-09-03 16:32:38.524678', '26', 'Larangan', 1, 'new through import_export', 13, 1),
(12, '2022-09-03 16:32:38.525353', '25', 'Ketanggungan', 1, 'new through import_export', 13, 1),
(13, '2022-09-03 16:32:38.526076', '24', 'Kersana', 1, 'new through import_export', 13, 1),
(14, '2022-09-03 16:32:38.526817', '23', 'Jatibarang', 1, 'new through import_export', 13, 1),
(15, '2022-09-03 16:32:38.527497', '22', 'Bumiayu', 1, 'new through import_export', 13, 1),
(16, '2022-09-03 16:32:38.528245', '21', 'Bulakamba', 1, 'new through import_export', 13, 1),
(17, '2022-09-03 16:32:38.528929', '20', 'Brebes', 1, 'new through import_export', 13, 1),
(18, '2022-09-03 16:32:38.529737', '19', 'Bantarkawung', 1, 'new through import_export', 13, 1),
(19, '2022-09-03 16:32:38.530713', '18', 'Banjarharjo', 1, 'new through import_export', 13, 1),
(20, '2022-09-03 16:32:55.286460', '41479', 'Tiwulandu', 1, 'new through import_export', 14, 1),
(21, '2022-09-03 16:32:55.287715', '41478', 'Tegalreja', 1, 'new through import_export', 14, 1),
(22, '2022-09-03 16:32:55.288782', '41477', 'Sukareja', 1, 'new through import_export', 14, 1),
(23, '2022-09-03 16:32:55.289857', '41476', 'Sindangheula', 1, 'new through import_export', 14, 1),
(24, '2022-09-03 16:32:55.290857', '41475', 'Pende', 1, 'new through import_export', 14, 1),
(25, '2022-09-03 16:32:55.291816', '41474', 'Penanggapan', 1, 'new through import_export', 14, 1),
(26, '2022-09-03 16:32:55.292794', '41473', 'Parereja', 1, 'new through import_export', 14, 1),
(27, '2022-09-03 16:32:55.293750', '41472', 'Malahayu', 1, 'new through import_export', 14, 1),
(28, '2022-09-03 16:32:55.294699', '41471', 'Kubangjero', 1, 'new through import_export', 14, 1),
(29, '2022-09-03 16:32:55.295649', '41470', 'Kertasari', 1, 'new through import_export', 14, 1),
(30, '2022-09-03 16:32:55.296536', '41469', 'Karangmaja', 1, 'new through import_export', 14, 1),
(31, '2022-09-03 16:32:55.297498', '41468', 'Dukuhjeruk', 1, 'new through import_export', 14, 1),
(32, '2022-09-03 16:32:55.298436', '41467', 'Cipajang', 1, 'new through import_export', 14, 1),
(33, '2022-09-03 16:32:55.299373', '41466', 'Cimunding', 1, 'new through import_export', 14, 1),
(34, '2022-09-03 16:32:55.300229', '41465', 'Cikuya', 1, 'new through import_export', 14, 1),
(35, '2022-09-03 16:32:55.301192', '41464', 'Cikakak', 1, 'new through import_export', 14, 1),
(36, '2022-09-03 16:32:55.302591', '41463', 'Cihaur', 1, 'new through import_export', 14, 1),
(37, '2022-09-03 16:32:55.303556', '41462', 'Cigadung', 1, 'new through import_export', 14, 1),
(38, '2022-09-03 16:32:55.304570', '41461', 'Cibuniwangi', 1, 'new through import_export', 14, 1),
(39, '2022-09-03 16:32:55.305534', '41460', 'Cibendung', 1, 'new through import_export', 14, 1),
(40, '2022-09-03 16:32:55.306487', '41459', 'Ciawi', 1, 'new through import_export', 14, 1),
(41, '2022-09-03 16:32:55.307443', '41458', 'Blandongan', 1, 'new through import_export', 14, 1),
(42, '2022-09-03 16:32:55.308263', '41457', 'Banjarharjo', 1, 'new through import_export', 14, 1),
(43, '2022-09-03 16:32:55.308948', '41456', 'Banjarlor', 1, 'new through import_export', 14, 1),
(44, '2022-09-03 16:32:55.309633', '41455', 'Bandungsari', 1, 'new through import_export', 14, 1),
(45, '2022-09-03 16:32:55.310522', '41453', 'Padakaton', 1, 'new through import_export', 14, 1),
(46, '2022-09-03 16:32:55.311474', '41451', 'Karang Malang', 1, 'new through import_export', 14, 1),
(47, '2022-09-03 16:32:55.312433', '41450', 'Tanggungsari', 1, 'new through import_export', 14, 1),
(48, '2022-09-03 16:32:55.313323', '41448', 'Pamedaran', 1, 'new through import_export', 14, 1),
(49, '2022-09-03 16:32:55.314220', '41447', 'Kubangsari', 1, 'new through import_export', 14, 1),
(50, '2022-09-03 16:32:55.315062', '41446', 'Kubangwungu', 1, 'new through import_export', 14, 1),
(51, '2022-09-03 16:32:55.315963', '41445', 'Kubangjati', 1, 'new through import_export', 14, 1),
(52, '2022-09-03 16:32:55.316863', '41444', 'Ketanggungan', 1, 'new through import_export', 14, 1),
(53, '2022-09-03 16:32:55.317695', '41443', 'Karangbandung', 1, 'new through import_export', 14, 1),
(54, '2022-09-03 16:32:55.318523', '41442', 'Jemasih', 1, 'new through import_export', 14, 1),
(55, '2022-09-03 16:32:55.319359', '41441', 'Dukuhtengah', 1, 'new through import_export', 14, 1),
(56, '2022-09-03 16:32:55.320329', '41440', 'Dukuhbadag', 1, 'new through import_export', 14, 1),
(57, '2022-09-03 16:32:55.321188', '41439', 'Ciseureuh', 1, 'new through import_export', 14, 1),
(58, '2022-09-03 16:32:55.322051', '41438', 'Cikeusal Lor', 1, 'new through import_export', 14, 1),
(59, '2022-09-03 16:32:55.322956', '41437', 'Cikeusal Kidul', 1, 'new through import_export', 14, 1),
(60, '2022-09-03 16:32:55.323806', '41436', 'Ciduwet', 1, 'new through import_export', 14, 1),
(61, '2022-09-03 16:32:55.324668', '41435', 'Bulakelor', 1, 'new through import_export', 14, 1),
(62, '2022-09-03 16:32:55.325494', '41434', 'Buara', 1, 'new through import_export', 14, 1),
(63, '2022-09-03 16:32:55.326332', '41433', 'Baros', 1, 'new through import_export', 14, 1),
(64, '2022-09-03 16:32:55.327173', '41431', 'Siandong', 1, 'new through import_export', 14, 1),
(65, '2022-09-03 16:32:55.328043', '41430', 'Wlahar', 1, 'new through import_export', 14, 1),
(66, '2022-09-03 16:32:55.328927', '41429', 'Slatri', 1, 'new through import_export', 14, 1),
(67, '2022-09-03 16:32:55.329830', '41428', 'Sitanggal', 1, 'new through import_export', 14, 1),
(68, '2022-09-03 16:32:55.330729', '41427', 'Rengaspendawa', 1, 'new through import_export', 14, 1),
(69, '2022-09-03 16:32:55.331606', '41426', 'Pamulihan', 1, 'new through import_export', 14, 1),
(70, '2022-09-03 16:32:55.332506', '41425', 'Luwunggede', 1, 'new through import_export', 14, 1),
(71, '2022-09-03 16:32:55.333388', '41424', 'Larangan', 1, 'new through import_export', 14, 1),
(72, '2022-09-03 16:32:55.334265', '41423', 'Kedungbokor', 1, 'new through import_export', 14, 1),
(73, '2022-09-03 16:32:55.335132', '41422', 'Karangbale', 1, 'new through import_export', 14, 1),
(74, '2022-09-03 16:32:55.335973', '41421', 'Kamal', 1, 'new through import_export', 14, 1),
(75, '2022-09-03 16:32:55.336797', '41419', 'Tegalglagah', 1, 'new through import_export', 14, 1),
(76, '2022-09-03 16:32:55.337695', '41418', 'Siwuluh', 1, 'new through import_export', 14, 1),
(77, '2022-09-03 16:32:55.338604', '41417', 'Rancawuluh', 1, 'new through import_export', 14, 1),
(78, '2022-09-03 16:32:55.339515', '41416', 'Pulogading', 1, 'new through import_export', 14, 1),
(79, '2022-09-03 16:32:55.340487', '41415', 'Petunjungan', 1, 'new through import_export', 14, 1),
(80, '2022-09-03 16:32:55.341470', '41414', 'Pakijangan', 1, 'new through import_export', 14, 1),
(81, '2022-09-03 16:32:55.342465', '41413', 'Luwungragi', 1, 'new through import_export', 14, 1),
(82, '2022-09-03 16:32:55.343441', '41412', 'Kluwut', 1, 'new through import_export', 14, 1),
(83, '2022-09-03 16:32:55.344417', '41411', 'Karangsari', 1, 'new through import_export', 14, 1),
(84, '2022-09-03 16:32:55.345460', '41410', 'Jubang', 1, 'new through import_export', 14, 1),
(85, '2022-09-03 16:32:55.346420', '41409', 'Grinting', 1, 'new through import_export', 14, 1),
(86, '2022-09-03 16:32:55.347728', '41408', 'Dukuhlo', 1, 'new through import_export', 14, 1),
(87, '2022-09-03 16:32:55.348739', '41407', 'Cipelem', 1, 'new through import_export', 14, 1),
(88, '2022-09-03 16:32:55.349698', '41406', 'Cimohong', 1, 'new through import_export', 14, 1),
(89, '2022-09-03 16:32:55.350616', '41405', 'Bulusari', 1, 'new through import_export', 14, 1),
(90, '2022-09-03 16:32:55.351638', '41404', 'Bulakparen', 1, 'new through import_export', 14, 1),
(91, '2022-09-03 16:32:55.352898', '41403', 'Bulakamba', 1, 'new through import_export', 14, 1),
(92, '2022-09-03 16:32:55.353836', '41402', 'Banjaratma', 1, 'new through import_export', 14, 1),
(93, '2022-09-03 16:32:55.354972', '41401', 'Bangsri', 1, 'new through import_export', 14, 1),
(94, '2022-09-03 16:32:55.355975', '41399', 'Tengguli', 1, 'new through import_export', 14, 1),
(95, '2022-09-03 16:32:55.356873', '41398', 'Tegongan', 1, 'new through import_export', 14, 1),
(96, '2022-09-03 16:32:55.357763', '41397', 'Tanjung', 1, 'new through import_export', 14, 1),
(97, '2022-09-03 16:32:55.358453', '41396', 'Sidakaton', 1, 'new through import_export', 14, 1),
(98, '2022-09-03 16:32:55.359189', '41395', 'Sengon', 1, 'new through import_export', 14, 1),
(99, '2022-09-03 16:32:55.359937', '41394', 'Sarireja', 1, 'new through import_export', 14, 1),
(100, '2022-09-03 16:32:55.360699', '41393', 'Pengaradan', 1, 'new through import_export', 14, 1),
(101, '2022-09-03 16:32:55.361383', '41392', 'Pejagan', 1, 'new through import_export', 14, 1),
(102, '2022-09-03 16:32:55.362064', '41391', 'Mundu', 1, 'new through import_export', 14, 1),
(103, '2022-09-03 16:32:55.362951', '41390', 'Luwungbata', 1, 'new through import_export', 14, 1),
(104, '2022-09-03 16:32:55.363751', '41389', 'Lemahabang', 1, 'new through import_export', 14, 1),
(105, '2022-09-03 16:32:55.364466', '41387', 'Kubangputat', 1, 'new through import_export', 14, 1),
(106, '2022-09-03 16:32:55.365165', '41386', 'Krakahan', 1, 'new through import_export', 14, 1),
(107, '2022-09-03 16:32:55.366031', '41385', 'Kemurang Wetan', 1, 'new through import_export', 14, 1),
(108, '2022-09-03 16:32:55.366762', '41384', 'Kemurang Kulon', 1, 'new through import_export', 14, 1),
(109, '2022-09-03 16:32:55.367581', '41383', 'Kedawung', 1, 'new through import_export', 14, 1),
(110, '2022-09-03 16:32:55.368305', '41382', 'Karangreja', 1, 'new through import_export', 14, 1),
(111, '2022-09-03 16:32:55.369252', '41380', 'Rungkang', 1, 'new through import_export', 14, 1),
(112, '2022-09-03 16:32:55.370240', '41379', 'Randusari', 1, 'new through import_export', 14, 1),
(113, '2022-09-03 16:32:55.370917', '41378', 'Randegan', 1, 'new through import_export', 14, 1),
(114, '2022-09-03 16:32:55.371657', '41377', 'Prapag Lor', 1, 'new through import_export', 14, 1),
(115, '2022-09-03 16:32:55.372340', '41376', 'Prapag Kidul', 1, 'new through import_export', 14, 1),
(116, '2022-09-03 16:32:55.373100', '41375', 'Pengabean', 1, 'new through import_export', 14, 1),
(117, '2022-09-03 16:32:55.373787', '41374', 'Pekauman', 1, 'new through import_export', 14, 1),
(118, '2022-09-03 16:32:55.374515', '41373', 'Negla', 1, 'new through import_export', 14, 1),
(119, '2022-09-03 16:32:55.375218', '41372', 'Losari Lor', 1, 'new through import_export', 14, 1),
(120, '2022-09-03 16:32:55.375903', '41371', 'Losari Kidul', 1, 'new through import_export', 14, 1),
(121, '2022-09-03 16:32:55.376707', '41369', 'Kedungneng', 1, 'new through import_export', 14, 1),
(122, '2022-09-03 16:32:55.377380', '41368', 'Kecipir', 1, 'new through import_export', 14, 1),
(123, '2022-09-03 16:32:55.378070', '41367', 'Karangsambung', 1, 'new through import_export', 14, 1),
(124, '2022-09-03 16:32:55.378749', '41366', 'Karangjunti', 1, 'new through import_export', 14, 1),
(125, '2022-09-03 16:32:55.379415', '41365', 'Karangdempel', 1, 'new through import_export', 14, 1),
(126, '2022-09-03 16:32:55.380119', '41364', 'Kalibuntu', 1, 'new through import_export', 14, 1),
(127, '2022-09-03 16:32:55.380796', '41362', 'Dukuhsalam', 1, 'new through import_export', 14, 1),
(128, '2022-09-03 16:32:55.381495', '41361', 'Bojongsari', 1, 'new through import_export', 14, 1),
(129, '2022-09-03 16:32:55.382180', '41360', 'Blubuk', 1, 'new through import_export', 14, 1),
(130, '2022-09-03 16:32:55.382859', '41359', 'Babakan', 1, 'new through import_export', 14, 1),
(131, '2022-09-03 16:32:55.383701', '41357', 'Sutamaja', 1, 'new through import_export', 14, 1),
(132, '2022-09-03 16:32:55.384367', '41356', 'Sindangjaya', 1, 'new through import_export', 14, 1),
(133, '2022-09-03 16:32:55.385047', '41354', 'Limbangan', 1, 'new through import_export', 14, 1),
(134, '2022-09-03 16:32:55.385791', '41353', 'Kubangpari', 1, 'new through import_export', 14, 1),
(135, '2022-09-03 16:32:55.386474', '41352', 'Kramatsampang', 1, 'new through import_export', 14, 1),
(136, '2022-09-03 16:32:55.387231', '41351', 'Kersana', 1, 'new through import_export', 14, 1),
(137, '2022-09-03 16:32:55.387946', '41350', 'Kradenan', 1, 'new through import_export', 14, 1),
(138, '2022-09-03 16:32:55.389029', '41349', 'Kemukten', 1, 'new through import_export', 14, 1),
(139, '2022-09-03 16:32:55.389733', '41348', 'Jagapura', 1, 'new through import_export', 14, 1),
(140, '2022-09-03 16:32:55.390405', '41347', 'Cikandang', 1, 'new through import_export', 14, 1),
(141, '2022-09-03 16:32:55.391110', '41346', 'Cigedog', 1, 'new through import_export', 14, 1),
(142, '2022-09-03 16:32:55.391848', '41345', 'Ciampel', 1, 'new through import_export', 14, 1),
(143, '2022-09-03 16:32:55.392510', '41343', 'Songgom Lor', 1, 'new through import_export', 14, 1),
(144, '2022-09-03 16:32:55.393180', '41342', 'Jatimakmur', 1, 'new through import_export', 14, 1),
(145, '2022-09-03 16:32:55.393872', '41341', 'Gegerkunci', 1, 'new through import_export', 14, 1),
(146, '2022-09-03 16:32:55.394946', '41340', 'Wanatawang', 1, 'new through import_export', 14, 1),
(147, '2022-09-03 16:32:55.395608', '41339', 'Wanacala', 1, 'new through import_export', 14, 1),
(148, '2022-09-03 16:32:55.396314', '41338', 'Songgom', 1, 'new through import_export', 14, 1),
(149, '2022-09-03 16:32:55.396980', '41337', 'Karang Sembung', 1, 'new through import_export', 14, 1),
(150, '2022-09-03 16:32:55.397662', '41336', 'Jatirokeh', 1, 'new through import_export', 14, 1),
(151, '2022-09-03 16:32:55.398333', '41335', 'Dukuhmaja', 1, 'new through import_export', 14, 1),
(152, '2022-09-03 16:32:55.399002', '41334', 'Cenang', 1, 'new through import_export', 14, 1),
(153, '2022-09-03 16:32:55.399670', '41332', 'Wangandalem', 1, 'new through import_export', 14, 1),
(154, '2022-09-03 16:32:55.400334', '41331', 'Terlangu', 1, 'new through import_export', 14, 1),
(155, '2022-09-03 16:32:55.401001', '41330', 'Tengki', 1, 'new through import_export', 14, 1),
(156, '2022-09-03 16:32:55.401670', '41329', 'Sigambir', 1, 'new through import_export', 14, 1),
(157, '2022-09-03 16:32:55.402329', '41328', 'Randusanga Wetan', 1, 'new through import_export', 14, 1),
(158, '2022-09-03 16:32:55.403001', '41327', 'Randusanga Kulon', 1, 'new through import_export', 14, 1),
(159, '2022-09-03 16:32:55.403692', '41326', 'Pulosari', 1, 'new through import_export', 14, 1),
(160, '2022-09-03 16:32:55.404355', '41325', 'Pemaron', 1, 'new through import_export', 14, 1),
(161, '2022-09-03 16:32:55.405021', '41324', 'Pagejugan', 1, 'new through import_export', 14, 1),
(162, '2022-09-03 16:32:55.405696', '41323', 'Padasugih', 1, 'new through import_export', 14, 1),
(163, '2022-09-03 16:32:55.406734', '41322', 'Lembarawa', 1, 'new through import_export', 14, 1),
(164, '2022-09-03 16:32:55.407480', '41321', 'Krasak', 1, 'new through import_export', 14, 1),
(165, '2022-09-03 16:32:55.408170', '41320', 'Kedunguter', 1, 'new through import_export', 14, 1),
(166, '2022-09-03 16:32:55.408847', '41319', 'Kaliwlingi', 1, 'new through import_export', 14, 1),
(167, '2022-09-03 16:32:55.409501', '41318', 'Kalimati', 1, 'new through import_export', 14, 1),
(168, '2022-09-03 16:32:55.410182', '41317', 'Kaligangsa Wetan', 1, 'new through import_export', 14, 1),
(169, '2022-09-03 16:32:55.410848', '41316', 'Kaligangsa Kulon', 1, 'new through import_export', 14, 1),
(170, '2022-09-03 16:32:55.411508', '41315', 'Banjaranyar', 1, 'new through import_export', 14, 1),
(171, '2022-09-03 16:32:55.412182', '41314', 'Pasarbatang', 1, 'new through import_export', 14, 1),
(172, '2022-09-03 16:32:55.412870', '41313', 'Limbangan Wetan', 1, 'new through import_export', 14, 1),
(173, '2022-09-03 16:32:55.413971', '41312', 'Limbangan Kulon', 1, 'new through import_export', 14, 1),
(174, '2022-09-03 16:32:55.414653', '41311', 'Gandasuli', 1, 'new through import_export', 14, 1),
(175, '2022-09-03 16:32:55.415353', '41310', 'Brebes', 1, 'new through import_export', 14, 1),
(176, '2022-09-03 16:32:55.416049', '41308', 'Wanasari', 1, 'new through import_export', 14, 1),
(177, '2022-09-03 16:32:55.416753', '41307', 'Tegalgandu', 1, 'new through import_export', 14, 1),
(178, '2022-09-03 16:32:55.417447', '41306', 'Tanjungsari', 1, 'new through import_export', 14, 1),
(179, '2022-09-03 16:32:55.418143', '41305', 'Siwungkuk', 1, 'new through import_export', 14, 1),
(180, '2022-09-03 16:32:55.418833', '41304', 'Sisalam', 1, 'new through import_export', 14, 1),
(181, '2022-09-03 16:32:55.419446', '41303', 'Sigentong', 1, 'new through import_export', 14, 1),
(182, '2022-09-03 16:32:55.420174', '41302', 'Sidamulya', 1, 'new through import_export', 14, 1),
(183, '2022-09-03 16:32:55.420870', '41301', 'Siasem', 1, 'new through import_export', 14, 1),
(184, '2022-09-03 16:32:55.421519', '41300', 'Sawojajar', 1, 'new through import_export', 14, 1),
(185, '2022-09-03 16:32:55.422143', '41299', 'Pesantunan', 1, 'new through import_export', 14, 1),
(186, '2022-09-03 16:32:55.422896', '41298', 'Pebatan', 1, 'new through import_export', 14, 1),
(187, '2022-09-03 16:32:55.423856', '41297', 'Lengkong', 1, 'new through import_export', 14, 1),
(188, '2022-09-03 16:32:55.424813', '41296', 'Kupu', 1, 'new through import_export', 14, 1),
(189, '2022-09-03 16:32:55.425773', '41295', 'Klampok', 1, 'new through import_export', 14, 1),
(190, '2022-09-03 16:32:55.426739', '41294', 'Kertabesuki', 1, 'new through import_export', 14, 1),
(191, '2022-09-03 16:32:55.427589', '41293', 'Keboledan', 1, 'new through import_export', 14, 1),
(192, '2022-09-03 16:32:55.428544', '41292', 'Jagalempeni', 1, 'new through import_export', 14, 1),
(193, '2022-09-03 16:32:55.429477', '41291', 'Dumeling', 1, 'new through import_export', 14, 1),
(194, '2022-09-03 16:32:55.430436', '41290', 'Dukuhwringin', 1, 'new through import_export', 14, 1),
(195, '2022-09-03 16:32:55.431227', '41289', 'Glonggong', 1, 'new through import_export', 14, 1),
(196, '2022-09-03 16:32:55.431935', '41287', 'Tembelang', 1, 'new through import_export', 14, 1),
(197, '2022-09-03 16:32:55.432592', '41286', 'Tegalwulung', 1, 'new through import_export', 14, 1),
(198, '2022-09-03 16:32:55.433285', '41285', 'Rengasbandung', 1, 'new through import_export', 14, 1),
(199, '2022-09-03 16:32:55.434109', '41284', 'Pedeslohor', 1, 'new through import_export', 14, 1),
(200, '2022-09-03 16:32:55.434816', '41283', 'Pamengger', 1, 'new through import_export', 14, 1),
(201, '2022-09-03 16:32:55.435478', '41282', 'Kramat', 1, 'new through import_export', 14, 1),
(202, '2022-09-03 16:32:55.436168', '41281', 'Klikiran', 1, 'new through import_export', 14, 1),
(203, '2022-09-03 16:32:55.436852', '41280', 'Klampis', 1, 'new through import_export', 14, 1),
(204, '2022-09-03 16:32:55.437543', '41279', 'Kertasinduyasa', 1, 'new through import_export', 14, 1),
(205, '2022-09-03 16:32:55.438222', '41278', 'Kendawa', 1, 'new through import_export', 14, 1),
(206, '2022-09-03 16:32:55.438931', '41277', 'Kemiriamba', 1, 'new through import_export', 14, 1),
(207, '2022-09-03 16:32:55.439592', '41276', 'Kedungtukang', 1, 'new through import_export', 14, 1),
(208, '2022-09-03 16:32:55.440273', '41275', 'Kebonagung', 1, 'new through import_export', 14, 1),
(209, '2022-09-03 16:32:55.440958', '41274', 'Kebogadung', 1, 'new through import_export', 14, 1),
(210, '2022-09-03 16:32:55.441643', '41273', 'Karanglo', 1, 'new through import_export', 14, 1),
(211, '2022-09-03 16:32:55.442506', '41272', 'Kalipucang', 1, 'new through import_export', 14, 1),
(212, '2022-09-03 16:32:55.443198', '41271', 'Kalialang', 1, 'new through import_export', 14, 1),
(213, '2022-09-03 16:32:55.443871', '41270', 'Jatibarang Lor', 1, 'new through import_export', 14, 1),
(214, '2022-09-03 16:32:55.444542', '41269', 'Jatibarang Kidul', 1, 'new through import_export', 14, 1),
(215, '2022-09-03 16:32:55.445267', '41268', 'Janegara', 1, 'new through import_export', 14, 1),
(216, '2022-09-03 16:32:55.445973', '41267', 'Buaran', 1, 'new through import_export', 14, 1),
(217, '2022-09-03 16:32:55.446662', '41266', 'Bojong', 1, 'new through import_export', 14, 1),
(218, '2022-09-03 16:32:55.447361', '41264', 'Watujaya', 1, 'new through import_export', 14, 1),
(219, '2022-09-03 16:32:55.448070', '41263', 'Tonjong', 1, 'new through import_export', 14, 1),
(220, '2022-09-03 16:32:55.448752', '41262', 'Tanggeran', 1, 'new through import_export', 14, 1),
(221, '2022-09-03 16:32:55.449442', '41261', 'Purwodadi', 1, 'new through import_export', 14, 1),
(222, '2022-09-03 16:32:55.450151', '41260', 'Rajawetan', 1, 'new through import_export', 14, 1),
(223, '2022-09-03 16:32:55.451007', '41259', 'Purbayasa', 1, 'new through import_export', 14, 1),
(224, '2022-09-03 16:32:55.451679', '41258', 'Pepedan', 1, 'new through import_export', 14, 1),
(225, '2022-09-03 16:32:55.452422', '41257', 'Negarayu', 1, 'new through import_export', 14, 1),
(226, '2022-09-03 16:32:55.453113', '41256', 'Linggapura', 1, 'new through import_export', 14, 1),
(227, '2022-09-03 16:32:55.453784', '41255', 'Kutayu', 1, 'new through import_export', 14, 1),
(228, '2022-09-03 16:32:55.454528', '41254', 'Kutamendala', 1, 'new through import_export', 14, 1),
(229, '2022-09-03 16:32:55.455217', '41253', 'Karangjongkeng', 1, 'new through import_export', 14, 1),
(230, '2022-09-03 16:32:55.455923', '41252', 'Kalijurang', 1, 'new through import_export', 14, 1),
(231, '2022-09-03 16:32:55.456591', '41251', 'Galuh Timur', 1, 'new through import_export', 14, 1),
(232, '2022-09-03 16:32:55.457289', '41249', 'Wanareja', 1, 'new through import_export', 14, 1),
(233, '2022-09-03 16:32:55.457979', '41248', 'Plompong', 1, 'new through import_export', 14, 1),
(234, '2022-09-03 16:32:55.458672', '41247', 'Sridadi', 1, 'new through import_export', 14, 1),
(235, '2022-09-03 16:32:55.459352', '41246', 'Mlayang', 1, 'new through import_export', 14, 1),
(236, '2022-09-03 16:32:55.460070', '41245', 'Mendala', 1, 'new through import_export', 14, 1),
(237, '2022-09-03 16:32:55.460749', '41244', 'Manggis', 1, 'new through import_export', 14, 1),
(238, '2022-09-03 16:32:55.461430', '41243', 'Kaliloka', 1, 'new through import_export', 14, 1),
(239, '2022-09-03 16:32:55.462121', '41242', 'Kaligiri', 1, 'new through import_export', 14, 1),
(240, '2022-09-03 16:32:55.462830', '41241', 'Igirklanceng', 1, 'new through import_export', 14, 1),
(241, '2022-09-03 16:32:55.463571', '41240', 'Dawuhan', 1, 'new through import_export', 14, 1),
(242, '2022-09-03 16:32:55.464282', '41239', 'Buniwah', 1, 'new through import_export', 14, 1),
(243, '2022-09-03 16:32:55.465088', '41238', 'Benda', 1, 'new through import_export', 14, 1),
(244, '2022-09-03 16:32:55.465811', '41237', 'Batursari', 1, 'new through import_export', 14, 1),
(245, '2022-09-03 16:32:55.466463', '41235', 'Winduaji', 1, 'new through import_export', 14, 1),
(246, '2022-09-03 16:32:55.467147', '41234', 'Wanatirta', 1, 'new through import_export', 14, 1),
(247, '2022-09-03 16:32:55.467878', '41233', 'Taraban', 1, 'new through import_export', 14, 1),
(248, '2022-09-03 16:32:55.468541', '41232', 'Ragatunjung', 1, 'new through import_export', 14, 1),
(249, '2022-09-03 16:32:55.469217', '41231', 'Pandansari', 1, 'new through import_export', 14, 1),
(250, '2022-09-03 16:32:55.469893', '41230', 'Pakujati', 1, 'new through import_export', 14, 1),
(251, '2022-09-03 16:32:55.470569', '41229', 'Paguyangan', 1, 'new through import_export', 14, 1),
(252, '2022-09-03 16:32:55.471295', '41228', 'Pagojengan', 1, 'new through import_export', 14, 1),
(253, '2022-09-03 16:32:55.471970', '41227', 'Kretek', 1, 'new through import_export', 14, 1),
(254, '2022-09-03 16:32:55.472660', '41226', 'Kedungoleng', 1, 'new through import_export', 14, 1),
(255, '2022-09-03 16:32:55.473333', '41225', 'Cipetung', 1, 'new through import_export', 14, 1),
(256, '2022-09-03 16:32:55.474081', '41224', 'Cilibur', 1, 'new through import_export', 14, 1),
(257, '2022-09-03 16:32:55.474757', '41222', 'Pruwatan', 1, 'new through import_export', 14, 1),
(258, '2022-09-03 16:32:55.475459', '41221', 'Penggarutan', 1, 'new through import_export', 14, 1),
(259, '2022-09-03 16:32:55.476148', '41220', 'Pamijen', 1, 'new through import_export', 14, 1),
(260, '2022-09-03 16:32:55.476834', '41219', 'Negaradaha', 1, 'new through import_export', 14, 1),
(261, '2022-09-03 16:32:55.477518', '41218', 'Laren', 1, 'new through import_export', 14, 1),
(262, '2022-09-03 16:32:55.478201', '41217', 'Langkap', 1, 'new through import_export', 14, 1),
(263, '2022-09-03 16:32:55.478891', '41216', 'Kaliwadas', 1, 'new through import_export', 14, 1),
(264, '2022-09-03 16:32:55.479559', '41215', 'Kalisumur', 1, 'new through import_export', 14, 1),
(265, '2022-09-03 16:32:55.480250', '41214', 'Kalinusu', 1, 'new through import_export', 14, 1),
(266, '2022-09-03 16:32:55.480944', '41213', 'Kalilangkap', 1, 'new through import_export', 14, 1),
(267, '2022-09-03 16:32:55.481609', '41212', 'Kalierang', 1, 'new through import_export', 14, 1),
(268, '2022-09-03 16:32:55.482287', '41211', 'Jatisawit', 1, 'new through import_export', 14, 1),
(269, '2022-09-03 16:32:55.482973', '41210', 'Dukuhturi', 1, 'new through import_export', 14, 1),
(270, '2022-09-03 16:32:55.483667', '41209', 'Bumiayu', 1, 'new through import_export', 14, 1),
(271, '2022-09-03 16:32:55.484338', '41208', 'Adisana', 1, 'new through import_export', 14, 1),
(272, '2022-09-03 16:32:55.485021', '41206', 'Waru', 1, 'new through import_export', 14, 1),
(273, '2022-09-03 16:32:55.485703', '41205', 'Telaga', 1, 'new through import_export', 14, 1),
(274, '2022-09-03 16:32:55.486377', '41204', 'Terlaya', 1, 'new through import_export', 14, 1),
(275, '2022-09-03 16:32:55.487064', '41203', 'Tambakserang', 1, 'new through import_export', 14, 1),
(276, '2022-09-03 16:32:55.487744', '41202', 'Sindangwangi', 1, 'new through import_export', 14, 1),
(277, '2022-09-03 16:32:55.488417', '41201', 'Pengarasan', 1, 'new through import_export', 14, 1),
(278, '2022-09-03 16:32:55.489160', '41200', 'Pangebatan', 1, 'new through import_export', 14, 1),
(279, '2022-09-03 16:32:55.489918', '41199', 'Legok', 1, 'new through import_export', 14, 1),
(280, '2022-09-03 16:32:55.490649', '41198', 'Kebandungan', 1, 'new through import_export', 14, 1),
(281, '2022-09-03 16:32:55.491316', '41197', 'Karangpari', 1, 'new through import_export', 14, 1),
(282, '2022-09-03 16:32:55.491992', '41196', 'Jipang', 1, 'new through import_export', 14, 1),
(283, '2022-09-03 16:32:55.492672', '41195', 'Ciomas', 1, 'new through import_export', 14, 1),
(284, '2022-09-03 16:32:55.493347', '41194', 'Cinanas', 1, 'new through import_export', 14, 1),
(285, '2022-09-03 16:32:55.494025', '41193', 'Cibentang', 1, 'new through import_export', 14, 1),
(286, '2022-09-03 16:32:55.494729', '41192', 'Bantarwaru', 1, 'new through import_export', 14, 1),
(287, '2022-09-03 16:32:55.495486', '41191', 'Bantarkawung', 1, 'new through import_export', 14, 1),
(288, '2022-09-03 16:32:55.496160', '41190', 'Banjarsari', 1, 'new through import_export', 14, 1),
(289, '2022-09-03 16:32:55.496881', '41189', 'Bangbayang', 1, 'new through import_export', 14, 1),
(290, '2022-09-03 16:32:55.497548', '41187', 'Windusakti', 1, 'new through import_export', 14, 1),
(291, '2022-09-03 16:32:55.498230', '41186', 'Winduasri', 1, 'new through import_export', 14, 1),
(292, '2022-09-03 16:32:55.498991', '41185', 'Wanoja', 1, 'new through import_export', 14, 1),
(293, '2022-09-03 16:32:55.499706', '41184', 'Tembongraja', 1, 'new through import_export', 14, 1),
(294, '2022-09-03 16:32:55.500355', '41183', 'Salem', 1, 'new through import_export', 14, 1),
(295, '2022-09-03 16:32:55.501145', '41182', 'Pasirpanjang', 1, 'new through import_export', 14, 1),
(296, '2022-09-03 16:32:55.501891', '41181', 'Pabuaran', 1, 'new through import_export', 14, 1),
(297, '2022-09-03 16:32:55.502520', '41180', 'Kadumanis', 1, 'new through import_export', 14, 1),
(298, '2022-09-03 16:32:55.503126', '41179', 'Indrajaya', 1, 'new through import_export', 14, 1),
(299, '2022-09-03 16:32:55.503814', '41178', 'Gunungtajem', 1, 'new through import_export', 14, 1),
(300, '2022-09-03 16:32:55.504575', '41177', 'Gunungsugih', 1, 'new through import_export', 14, 1),
(301, '2022-09-03 16:32:55.505262', '41176', 'Gununglarang', 1, 'new through import_export', 14, 1),
(302, '2022-09-03 16:32:55.506011', '41175', 'Gunungjaya', 1, 'new through import_export', 14, 1),
(303, '2022-09-03 16:32:55.506732', '41174', 'Gandoang', 1, 'new through import_export', 14, 1),
(304, '2022-09-03 16:32:55.507388', '41173', 'Ganggawang', 1, 'new through import_export', 14, 1),
(305, '2022-09-03 16:32:55.508087', '41172', 'Citimbang', 1, 'new through import_export', 14, 1),
(306, '2022-09-03 16:32:55.508769', '41171', 'Ciputih', 1, 'new through import_export', 14, 1),
(307, '2022-09-03 16:32:55.509425', '41170', 'Capar', 1, 'new through import_export', 14, 1),
(308, '2022-09-03 16:32:55.510253', '41169', 'Bentarsari', 1, 'new through import_export', 14, 1),
(309, '2022-09-03 16:32:55.511163', '41168', 'Bentar', 1, 'new through import_export', 14, 1),
(310, '2022-09-03 16:32:55.512313', '41167', 'Banjaran', 1, 'new through import_export', 14, 1);

-- --------------------------------------------------------

--
-- Table structure for table `django_content_type`
--

CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `django_content_type`
--

INSERT INTO `django_content_type` (`id`, `app_label`, `model`) VALUES
(1, 'admin', 'logentry'),
(2, 'auth', 'permission'),
(3, 'auth', 'group'),
(4, 'contenttypes', 'contenttype'),
(5, 'sessions', 'session'),
(6, 'django_summernote', 'attachment'),
(7, 'sites', 'site'),
(8, 'sequences', 'sequence'),
(9, 'user', 'user'),
(10, 'user', 'printcard'),
(11, 'provinsi', 'provinsi'),
(12, 'kabupaten', 'kabupaten'),
(13, 'kecamatan', 'kecamatan'),
(14, 'kelurahan', 'kelurahan');

-- --------------------------------------------------------

--
-- Table structure for table `django_migrations`
--

CREATE TABLE `django_migrations` (
  `id` int(11) NOT NULL,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime(6) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `django_migrations`
--

INSERT INTO `django_migrations` (`id`, `app`, `name`, `applied`) VALUES
(1, 'provinsi', '0001_initial', '2022-09-03 16:15:36.621152'),
(2, 'kabupaten', '0001_initial', '2022-09-03 16:15:37.785048'),
(3, 'kecamatan', '0001_initial', '2022-09-03 16:15:39.101452'),
(4, 'kelurahan', '0001_initial', '2022-09-03 16:15:40.227718'),
(5, 'contenttypes', '0001_initial', '2022-09-03 16:15:41.949426'),
(6, 'contenttypes', '0002_remove_content_type_name', '2022-09-03 16:15:41.971969'),
(7, 'auth', '0001_initial', '2022-09-03 16:15:45.856789'),
(8, 'auth', '0002_alter_permission_name_max_length', '2022-09-03 16:15:45.865932'),
(9, 'auth', '0003_alter_user_email_max_length', '2022-09-03 16:15:45.873684'),
(10, 'auth', '0004_alter_user_username_opts', '2022-09-03 16:15:45.880794'),
(11, 'auth', '0005_alter_user_last_login_null', '2022-09-03 16:15:45.888296'),
(12, 'auth', '0006_require_contenttypes_0002', '2022-09-03 16:15:45.889942'),
(13, 'auth', '0007_alter_validators_add_error_messages', '2022-09-03 16:15:45.896222'),
(14, 'auth', '0008_alter_user_username_max_length', '2022-09-03 16:15:45.902676'),
(15, 'auth', '0009_alter_user_last_name_max_length', '2022-09-03 16:15:45.910768'),
(16, 'auth', '0010_alter_group_name_max_length', '2022-09-03 16:15:45.919905'),
(17, 'auth', '0011_update_proxy_permissions', '2022-09-03 16:15:45.931995'),
(18, 'auth', '0012_alter_user_first_name_max_length', '2022-09-03 16:15:45.939012'),
(19, 'user', '0001_initial', '2022-09-03 16:15:58.253966'),
(20, 'admin', '0001_initial', '2022-09-03 16:16:01.715939'),
(21, 'admin', '0002_logentry_remove_auto_add', '2022-09-03 16:16:01.736759'),
(22, 'admin', '0003_logentry_add_action_flag_choices', '2022-09-03 16:16:01.793681'),
(23, 'django_summernote', '0001_initial', '2022-09-03 16:16:03.303397'),
(24, 'django_summernote', '0002_update-help_text', '2022-09-03 16:16:03.310936'),
(25, 'kabupaten', '0002_kabupaten_code', '2022-09-03 16:16:03.743396'),
(26, 'kecamatan', '0002_kecamatan_code', '2022-09-03 16:16:04.256466'),
(27, 'kelurahan', '0002_kelurahan_code', '2022-09-03 16:16:04.658222'),
(28, 'kelurahan', '0003_auto_20210725_0821', '2022-09-03 16:16:04.678037'),
(29, 'provinsi', '0002_provinsi_code', '2022-09-03 16:16:05.057999'),
(30, 'sequences', '0001_initial', '2022-09-03 16:16:06.132300'),
(31, 'sessions', '0001_initial', '2022-09-03 16:16:07.217797'),
(32, 'sites', '0001_initial', '2022-09-03 16:16:08.272317'),
(33, 'sites', '0002_alter_domain_unique', '2022-09-03 16:16:08.281348'),
(34, 'user', '0002_auto_20210620_0104', '2022-09-03 16:16:08.297352'),
(35, 'user', '0003_auto_20210620_0121', '2022-09-03 16:16:08.323225'),
(36, 'user', '0004_auto_20210620_0722', '2022-09-03 16:16:08.524228'),
(37, 'user', '0005_auto_20210620_1238', '2022-09-03 16:16:10.727441'),
(38, 'user', '0006_auto_20210623_0121', '2022-09-03 16:16:11.132371'),
(39, 'user', '0007_auto_20210725_0644', '2022-09-03 16:16:12.221060'),
(40, 'user', '0008_auto_20210725_0851', '2022-09-03 16:16:12.244201'),
(41, 'user', '0009_auto_20220828_0308', '2022-09-03 16:16:12.284481'),
(42, 'user', '0010_user_age', '2022-09-03 16:16:12.682003'),
(43, 'user', '0011_user_age_month', '2022-09-03 16:16:13.029798'),
(44, 'user', '0012_rename_age_user_age_year', '2022-09-03 16:16:13.050017'),
(45, 'user', '0013_user_age_string', '2022-09-03 16:16:13.380180'),
(46, 'user', '0014_user_is_pregnant', '2022-09-03 16:16:13.740266'),
(47, 'user', '0015_user_is_breeding', '2022-09-03 16:16:14.418064'),
(48, 'user', '0016_alter_user_place_of_birth', '2022-09-03 16:38:48.185715'),
(49, 'user', '0017_user_parent', '2022-09-11 04:49:02.038478'),
(50, 'user', '0018_auto_20220917_2302', '2022-09-17 23:10:07.195875');

-- --------------------------------------------------------

--
-- Table structure for table `django_session`
--

CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime(6) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `django_session`
--

INSERT INTO `django_session` (`session_key`, `session_data`, `expire_date`) VALUES
('9ulw0ufgpwxkun18w8wgxfd9iyhfew19', 'e30:1oUVvM:gv7CnDZy9EaXinCQzYu2beD6uVpDi-mJJ8wGaizbI4k', '2022-09-17 16:23:12.275063'),
('k6nasmt7hj2fhnfwgk11eaz7u9tjhh3w', 'e30:1oUVvd:E5_6CgPXU_FBruIFN9ntughzBGgVhF8D8pjC044VcmM', '2022-09-17 16:23:29.656475'),
('5oltzep1qnzj30kxyohltnbuxknag5qq', 'e30:1oUVwL:7hWCMCjT91j74EOf7Bt2ylcEC3YXJgnSd3rDbFyvi4w', '2022-09-17 16:24:13.403629'),
('g05xjo0r8zukal57a31jk53okxyc9psz', 'e30:1oUVwg:tZXp5s9A8bmkViO3bUVdNzig8sB1E7ny0_BDMlOM5xQ', '2022-09-17 16:24:34.567126'),
('pn3lfvzm58mjuvf5j4w82ch404opi06q', '.eJxVjEEOwiAQRe_C2hAGDBSX7j0DmekwUjWQlHbVeHdt0oVu_3vvbyrhupS09jynidVFgTr9boTjM9cd8APrvemx1WWeSO-KPmjXt8b5dT3cv4OCvXxr4iw5kLho0AiYCBAkkBdnIg0uoreWz2jFkAvGZwQS9h4GcAIUWL0_-AA4Iw:1oe6vP:eGPpCRkUz-_JrhjVpwo0On634igBTLr8ng4QOHvSExA', '2022-10-14 03:42:55.400411'),
('xm4nti1eqrx2h0tcy7gnujb8t9o1zkdx', 'e30:1oUcFU:ULRNFxKNcb2ZoV-aSzJt5N4LAeLGN_S2EWOEfr6WGNE', '2022-09-17 23:08:24.770021'),
('vqs3l2bxeqshuy600f2b6psyq35wigw2', 'e30:1oUe1F:CLFz1unmRWLWk6Gh8ZeIMosjwwoNPLsRJRqQ3ozdp7E', '2022-09-18 01:01:49.882455'),
('xjyopatvpuuzvxgk6n5eg610c84mb51m', '.eJxVjEEOwiAQRe_C2hAGDBSX7j0DmekwUjWQlHbVeHdt0oVu_3vvbyrhupS09jynidVFgTr9boTjM9cd8APrvemx1WWeSO-KPmjXt8b5dT3cv4OCvXxr4iw5kLho0AiYCBAkkBdnIg0uoreWz2jFkAvGZwQS9h4GcAIUWL0_-AA4Iw:1oUeKx:VDa_hwrw3FO6x8p2lOEEXCUJNkdBjDSPNHQzAIeCzrA', '2022-09-18 01:22:11.764603'),
('r1v20u49stbl16gevrwjlp8sixo15yix', '.eJxVjEEOwiAQRe_C2hAGDBSX7j0DmekwUjWQlHbVeHdt0oVu_3vvbyrhupS09jynidVFgTr9boTjM9cd8APrvemx1WWeSO-KPmjXt8b5dT3cv4OCvXxr4iw5kLho0AiYCBAkkBdnIg0uoreWz2jFkAvGZwQS9h4GcAIUWL0_-AA4Iw:1oUfvb:wlv0XW6y64gavvNIKzkpVYJBpTUYqq5DcA8K3wXbfMY', '2022-09-18 03:04:07.309389'),
('5hdwk8yymqbsdw5ept7txal7m8wd0r79', '.eJxVjDsOQiEQAO9CbQgILmBp7xnIwi7y1EDyPpXx7kryCm1nJvMSEbe1xm3hOU4kzsI7cfiFCfOD2zB0x3brMve2zlOSI5G7XeS1Ez8ve_s3qLjU8UUAqyGfWJNlgx5ZZ2VBAQQuxXilrDtqU-griipkFBlNTDlkFxKL9wcKhTiX:1oeS4I:hb0v6c-Y6US62EP8czmP2UCt3cQxj_NbAcI9gTW41_0', '2022-10-15 02:17:30.003355'),
('p9erkmncxm82nc72txrn13ccfip8r2nz', '.eJxVjMEOwiAQRP9lz4YQKBY8evcbCOwuUjWQlPZk_HdL0kObuc17M1_wYV2yXxvPfiK4gYPLsYsB31w6oFcozyqwlmWeouiK2GkTj0r8ue_u6SCHlvsaOVmFchxU1IOUEU3aogKNTlu6OlKR0KDhDelEBi3KROyUlZiY4PcHCqM5WQ:1oZRqT:ftRx_6K8yBwDq5omQykkjyIp14_JP9bKIRb_drsbA0E', '2022-10-01 07:02:33.476093'),
('nf02m9005brpmnzmw7rujrw0ervl5ss4', '.eJxVjMEOwiAQRP9lz4YQKBY8evcbCOwuUjWQlPZk_HdL0kObuc17M1_wYV2yXxvPfiK4gYPLsYsB31w6oFcozyqwlmWeouiK2GkTj0r8ue_u6SCHlvsaOVmFchxU1IOUEU3aogKNTlu6OlKR0KDhDelEBi3KROyUlZiY4PcHCqM5WQ:1oZh4U:gX9yOW76ucrlbryci5zpBZiTJCk7tphKp5zmz5R2aB4', '2022-10-01 23:18:02.615019'),
('pzal0mp9lfd6tachm6v0adoykn9i13ia', '.eJxVjEEOwiAQRe_C2hAGDBSX7j0DmekwUjWQlHbVeHdt0oVu_3vvbyrhupS09jynidVFgTr9boTjM9cd8APrvemx1WWeSO-KPmjXt8b5dT3cv4OCvXxr4iw5kLho0AiYCBAkkBdnIg0uoreWz2jFkAvGZwQS9h4GcAIUWL0_-AA4Iw:1oaP2z:yMyIoMmhElexTbl-nH2ORi9QRNtYDINBjwsuQAWr7UA', '2022-10-03 22:15:25.042754'),
('dj1rhjfdbscei5qvzh49lmh4oj6jyt15', '.eJxVjDsOwyAQBe9CHSEta34p0-cMiF1wcBKBZOzKyt0jJBdJ-2bmHSLEfSth73kNSxJXASAuvyNFfuU6SHrG-miSW93WheRQ5Em7vLeU37fT_TsosZdRO4dWJQOKiB1Yizx5JkqoZ0xMlo1WmEGrCcCxzhG9V7PTBrx1kxefL_7sN1o:1ohM6x:uNsxjq9cldoSLDfGSaexDTMVmrjQI3j9ElluNixvOUk', '2022-10-23 02:32:15.948078'),
('f8sm86koo18j8u8jgs7jg9ele50ltk4y', '.eJxVjEEOwiAQRe_C2hAGDBSX7j0DmekwUjWQlHbVeHdt0oVu_3vvbyrhupS09jynidVFgTr9boTjM9cd8APrvemx1WWeSO-KPmjXt8b5dT3cv4OCvXxr4iw5kLho0AiYCBAkkBdnIg0uoreWz2jFkAvGZwQS9h4GcAIUWL0_-AA4Iw:1oe6cP:xEa5pVVdUtXpKYDw0r1aIqsmFLbz84jTnAWUa-Kh2XM', '2022-10-14 03:23:17.480562'),
('nhcyd5bz1rdcdznh94m5g6961w8zx500', '.eJxVjDsOQiEQAO9CbQgILmBp7xnIwi7y1EDyPpXx7kryCm1nJvMSEbe1xm3hOU4kzsI7cfiFCfOD2zB0x3brMve2zlOSI5G7XeS1Ez8ve_s3qLjU8UUAqyGfWJNlgx5ZZ2VBAQQuxXilrDtqU-griipkFBlNTDlkFxKL9wcKhTiX:1oe9BN:MCrlbyFS8ekaOtL6Rc_RCDiiJ-N7XBxMOAn01JjRw8g', '2022-10-14 06:07:33.984954'),
('g6582amepe4go15hzxp94x8e96x69g9g', '.eJxVjssOgjAURP-la9OUWi-tS_d8A7kvBDVtQmFl_HchYaHbOWcm8zY9rsvYr1XnfhJzNfFiTr8hIT8170QemO_FcsnLPJHdFXvQarsi-rod7t_AiHXc2tSQshfPHjiAKA0uRQrsXFRJPAyxTQoNowYgakCSg3BufcLoMGyvPl86_zij:1oe6LB:uky0Z5fEhO9hrIhCUEtQRYYK4QIUpbY05gaFcnLLppA', '2022-10-14 03:05:29.366568'),
('yjd97rrz3gmsa4k097dhbf74viyl042f', '.eJxVjMEOwiAQRP-FsyGLQFg9evcbyC4LUjU0Ke2p8d9tkx70Npn3ZlYVaZlrXHqe4iDqqtCp02_JlF657USe1B6jTmObp4H1ruiDdn0fJb9vh_t3UKnXbQ3irDeSQKBYRktiHfkcAkIuHgmpXNiBmESYEpvC9lyEPEGALaD6fAEjKjkR:1oe6LL:eyBB9v2VKqmCAR0-vNyZ-s8Yw5TsUR3QNS3ii2z1xDo', '2022-10-14 03:05:39.138994'),
('ja30reli8ozs50y31by19wfw3vctvkcd', '.eJxVjDsOwjAQBe_iGln-4jUlfc5g7Xo3JIBiKZ8KcXeIlALaNzPvpQpu61C2ReYysroocOr0OxLWh0w74TtOt6Zrm9Z5JL0r-qCL7hrL83q4fwcDLsO3ttER2hi41mR6CRBJvJEMgTygc8Yn6HMAPjNITExgJTlxgVJPuZJ6fwAG-ThJ:1oe6Lc:6Yr2KnRjw8uUIPbKQsI8mLPLjTdF8Qg-aq0ytgcYwuE', '2022-10-14 03:05:56.122544'),
('s4bzyabu9gxqepcccbw8ijurwy6w0k9u', '.eJxVjMsOwiAQRf-FtSGUNy7d-w1khgGpGkhKuzL-uzbpQrf3nHNfLMK21riNvMSZ2Jl5y06_I0J65LYTukO7dZ56W5cZ-a7wgw5-7ZSfl8P9O6gw6rcG4UQu1ibQiUyRFCY0znipQigyCJUxiAm9SdpDstkQOYU6AChHFi17fwASqjhL:1oe6Ld:H9Fw8fg94L61YBAChhx-B2R1OCHJH6aknQnFNdTvu1k', '2022-10-14 03:05:57.881691'),
('sumaykz4klfvrpm4b3dlphiek6gcz85e', '.eJxVjDsKwzAQRO-iOghWf6VMnzMIaXcdOQkyWHZlcvfY4CKpBua9mU2kvC41rZ3nNJK4CrDi8luWjC9uB6Fnbo9J4tSWeSzyUORJu7xPxO_b6f4d1NzrvnYRFRkGsFYVzioABe8dFsY92QWN0WjjsYCLkIM1NFgCNWjFHIoVny__ejft:1oe6Ll:OFaNe9W6Mlb1DeIMu2nu8CPpbpM4RXWBEobmTpZzD84', '2022-10-14 03:06:05.556435'),
('uhhdtot24mv879q7hrqxbxymq6widqgz', '.eJxVjEEOwiAQRe_C2hCgQyku3XsGMsyAVA0kpV0Z765NutDtf-_9lwi4rSVsPS1hZnEWGsTpd4xIj1R3wnestyap1XWZo9wVedAur43T83K4fwcFe_nWw2BdTuCJNdjoJlQZjTLK0ojakCHF0WUEJg08gEt-YptGZbxFBEDx_gAGdjge:1oe6Lq:vWn4PN-fp7FudOT9vPRotDrTfbOao2HFTM_-Iyhqkag', '2022-10-14 03:06:10.814473'),
('etwpk5px51gdo16v8mnatp1iy835bgmt', '.eJxVjEEOwiAQRe_C2pAC0xZcuvcMzcDMSNVAUtqV8e7apAvd_vfef6kJtzVPW-NlmkmdlbHq9DtGTA8uO6E7llvVqZZ1maPeFX3Qpq-V-Hk53L-DjC1_axktBONDH8OYiIitBwPohCPHDg2nzo8CLMlhDyBDZHBCA3XJ2EFIvT8hsDkp:1oe6M1:VdIN8acgYk2Rsn5SWmINKOAtEWGgEmMg6XHCQbZf1iA', '2022-10-14 03:06:21.429271'),
('4to027v4oroab493dtwvfpnhq09u7jnr', '.eJxVjMsOgjAUBf-la9PApS9cuucbSO-jFjUlobAy_ruQsNDtmZnzVmPc1jxuVZZxYnVVbacuvyNGeko5CD9iuc-a5rIuE-pD0SetephZXrfT_TvIsea9JurRIvUhxMTgoEGxIXXQ2GS8txRBQufEGON3QRrHCMjsRCgEbkF9viVYOQA:1oe6Pi:MJ8TMxHOcy4TpSNQ4aRuTp1Zl6A982J9rjlsk380aY4', '2022-10-14 03:10:10.878550'),
('zg62d69sk6p3omdp9hslezdan7bnsx6f', '.eJxVjEEOgkAMRe8yazMpLR0Yl-49AynTIqgZEgZWxrsrCQvd_vfef7lOtnXstmJLN6k7u5bc6XfsJT0s70Tvkm-zT3Nel6n3u-IPWvx1VnteDvfvYJQyfmusgVNUqQK0NdKAgSAit9ZINTATmHADfSQSA-O6GhrTGBQVSQOye38A2vw3Eg:1oe6O1:Ab_Ys8guJpddXqMIGxBQ0pvWYdT21Kgr-wb8Z9Vahgs', '2022-10-14 03:08:25.214785'),
('jdvfojbhsa2p6vyzm00qol1yynotxlye', '.eJxVjDsOwjAQBe_iGln-4jUlfc5g7Xo3JIBiKZ8KcXeIlALaNzPvpQpu61C2ReYysroocOr0OxLWh0w74TtOt6Zrm9Z5JL0r-qCL7hrL83q4fwcDLsO3ttER2hi41mR6CRBJvJEMgTygc8Yn6HMAPjNITExgJTlxgVJPuZJ6fwAG-ThJ:1oe6Ot:vyU1vxgDIAWCEAGESmz5MgrvzS-gNQhXDPEAc3C-eAU', '2022-10-14 03:09:19.568996'),
('s1t6897ev4xmnhxk4ek29abrorspvl7o', '.eJxVjDsOwyAQBe9CHSEta34p0-cMiF1wcBKBZOzKyt0jJBdJ-2bmHSLEfSth73kNSxJXASAuvyNFfuU6SHrG-miSW93WheRQ5Em7vLeU37fT_TsosZdRO4dWJQOKiB1Yizx5JkqoZ0xMlo1WmEGrCcCxzhG9V7PTBrx1kxefL_7sN1o:1ohS3g:BEOT2xCocQ-7_xsZ9-Q6kEtMk_ez2IeG37PWyTt8sSk', '2022-10-23 08:53:16.387351'),
('rw8cvx0e28fn0i1tcyhrcc4xcvhq23hg', '.eJxVjMEOwiAQRP9lz4YQKBY8evcbCOwuUjWQlPZk_HdL0kObuc17M1_wYV2yXxvPfiK4gYPLsYsB31w6oFcozyqwlmWeouiK2GkTj0r8ue_u6SCHlvsaOVmFchxU1IOUEU3aogKNTlu6OlKR0KDhDelEBi3KROyUlZiY4PcHCqM5WQ:1olVjC:HIK7PqtNZr37KPmdMwRUpO7lIB9RVHze6OWC_9vI1hQ', '2022-11-03 13:36:54.487527'),
('anrywqs6svlh1algb65xmrfdtnhewvk3', '.eJxVjEEOwiAQRe_C2hAGDBSX7j0DmekwUjWQlHbVeHdt0oVu_3vvbyrhupS09jynidVFgTr9boTjM9cd8APrvemx1WWeSO-KPmjXt8b5dT3cv4OCvXxr4iw5kLho0AiYCBAkkBdnIg0uoreWz2jFkAvGZwQS9h4GcAIUWL0_-AA4Iw:1okm4F:pGGnbkgXviSEt_8RHu0TeUon55njUMtCFFGcbQ_cmgo', '2022-11-01 12:51:35.321085'),
('b693a2dfzb3i7xq8r9vwdknyj923hqhu', '.eJxVjEEOwiAQRe_C2hCmIAMu3XsGMjAgVVOS0q6Md7dNutDtf-_9twi0LjWsPc9hZHERDsTpd4yUnnnaCT9oujeZ2rTMY5S7Ig_a5a1xfl0P9--gUq9bDUahxqE4R4waChrPXivgEhHZJS7G4jkxqQLWkR4GdMpbgMQbYy8-X_NAN-0:1ohXBF:gqmw6ppyDn5duKReSZyQJGuJh0Lk4vMSyBwTEN3HOQI', '2022-10-23 14:21:25.798066'),
('hv09i16d36mksx84qh848c9wguq01xza', '.eJxVjEEOwiAQRe_C2hAGDBSX7j0DmekwUjWQlHbVeHdt0oVu_3vvbyrhupS09jynidVFgTr9boTjM9cd8APrvemx1WWeSO-KPmjXt8b5dT3cv4OCvXxr4iw5kLho0AiYCBAkkBdnIg0uoreWz2jFkAvGZwQS9h4GcAIUWL0_-AA4Iw:1okErr:svsOr6mMrYbVfwtBmaiyR2SIAApw7MdcYTzTedvd4Vw', '2022-10-31 01:24:35.383631'),
('uviz9ulmpj1iprugxemk8d8mazt6hqcp', '.eJxVjMEOwiAQRP9lz4YQKBY8evcbCOwuUjWQlPZk_HdL0kObuc17M1_wYV2yXxvPfiK4gYPLsYsB31w6oFcozyqwlmWeouiK2GkTj0r8ue_u6SCHlvsaOVmFchxU1IOUEU3aogKNTlu6OlKR0KDhDelEBi3KROyUlZiY4PcHCqM5WQ:1olhvU:vlmdiWjB1K9LumDHWpDURfCqC4KzcIVxdZfREBFeoy4', '2022-11-04 02:38:24.581446'),
('mwbz2k6pob6ytkds044xk5l2krylg2lt', '.eJxVjEEOwiAQRe_C2pCBdqC4dO8ZyAxDpWpoUtqV8e7apAvd_vfef6lI21ri1vISJ1Fn1YVBnX5XpvTIdUdyp3qbdZrrukysd0UftOnrLPl5Ody_g0KtfGuW1EFA7wYBoJzIgSOLIZvsRwgmsPU9MDIijMlKcD0SirUdGzAi6v0BLrE3-w:1okmYT:RFgMiaL9ti7T621MMtJz3svoNQy6zvXPoy_zD3krEPA', '2022-11-01 13:22:49.721831');

-- --------------------------------------------------------

--
-- Table structure for table `django_site`
--

CREATE TABLE `django_site` (
  `id` int(11) NOT NULL,
  `domain` varchar(100) NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `django_site`
--

INSERT INTO `django_site` (`id`, `domain`, `name`) VALUES
(1, 'example.com', 'example.com');

-- --------------------------------------------------------

--
-- Table structure for table `django_summernote_attachment`
--

CREATE TABLE `django_summernote_attachment` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `file` varchar(100) NOT NULL,
  `uploaded` datetime(6) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `kabupaten_kabupaten`
--

CREATE TABLE `kabupaten_kabupaten` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `provinsi_id` int(11) NOT NULL,
  `code` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kabupaten_kabupaten`
--

INSERT INTO `kabupaten_kabupaten` (`id`, `name`, `provinsi_id`, `code`) VALUES
(41165, 'Brebes', 32676, 3329);

-- --------------------------------------------------------

--
-- Table structure for table `kecamatan_kecamatan`
--

CREATE TABLE `kecamatan_kecamatan` (
  `id` int(11) NOT NULL,
  `name` varchar(254) NOT NULL,
  `kabupaten_id` int(11) NOT NULL,
  `code` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kecamatan_kecamatan`
--

INSERT INTO `kecamatan_kecamatan` (`id`, `name`, `kabupaten_id`, `code`) VALUES
(34, 'Wanasari', 41165, 332908),
(33, 'Tonjong', 41165, 332906),
(32, 'Tanjung', 41165, 332913),
(31, 'Songgom', 41165, 332910),
(30, 'Sirampog', 41165, 332905),
(29, 'Salem', 41165, 332901),
(28, 'Paguyangan', 41165, 332904),
(27, 'Losari', 41165, 332912),
(26, 'Larangan', 41165, 332915),
(25, 'Ketanggungan', 41165, 332916),
(24, 'Kersana', 41165, 332911),
(23, 'Jatibarang', 41165, 332907),
(22, 'Bumiayu', 41165, 332903),
(21, 'Bulakamba', 41165, 332914),
(20, 'Brebes', 41165, 332909),
(19, 'Bantarkawung', 41165, 332902),
(18, 'Banjarharjo', 41165, 332917);

-- --------------------------------------------------------

--
-- Table structure for table `kelurahan_kelurahan`
--

CREATE TABLE `kelurahan_kelurahan` (
  `id` int(11) NOT NULL,
  `name` varchar(254) NOT NULL,
  `kecamatan_id` int(11) NOT NULL,
  `code` varchar(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kelurahan_kelurahan`
--

INSERT INTO `kelurahan_kelurahan` (`id`, `name`, `kecamatan_id`, `code`) VALUES
(41479, 'Tiwulandu', 18, '3329172025'),
(41478, 'Tegalreja', 18, '3329172024'),
(41477, 'Sukareja', 18, '3329172023'),
(41476, 'Sindangheula', 18, '3329172022'),
(41475, 'Pende', 18, '3329172021'),
(41474, 'Penanggapan', 18, '3329172020'),
(41473, 'Parereja', 18, '3329172019'),
(41472, 'Malahayu', 18, '3329172018'),
(41471, 'Kubangjero', 18, '3329172017'),
(41470, 'Kertasari', 18, '3329172016'),
(41469, 'Karangmaja', 18, '3329172015'),
(41468, 'Dukuhjeruk', 18, '3329172014'),
(41467, 'Cipajang', 18, '3329172013'),
(41466, 'Cimunding', 18, '3329172012'),
(41465, 'Cikuya', 18, '3329172011'),
(41464, 'Cikakak', 18, '3329172010'),
(41463, 'Cihaur', 18, '3329172009'),
(41462, 'Cigadung', 18, '3329172008'),
(41461, 'Cibuniwangi', 18, '3329172007'),
(41460, 'Cibendung', 18, '3329172006'),
(41459, 'Ciawi', 18, '3329172005'),
(41458, 'Blandongan', 18, '3329172004'),
(41457, 'Banjarharjo', 18, '3329172003'),
(41456, 'Banjarlor', 18, '3329172002'),
(41455, 'Bandungsari', 18, '3329172001'),
(41453, 'Padakaton', 25, '3329162021'),
(41451, 'Karang Malang', 25, '3329162019'),
(41450, 'Tanggungsari', 25, '3329162018'),
(41448, 'Pamedaran', 25, '3329162016'),
(41447, 'Kubangsari', 25, '3329162015'),
(41446, 'Kubangwungu', 25, '3329162014'),
(41445, 'Kubangjati', 25, '3329162013'),
(41444, 'Ketanggungan', 25, '3329162012'),
(41443, 'Karangbandung', 25, '3329162011'),
(41442, 'Jemasih', 25, '3329162010'),
(41441, 'Dukuhtengah', 25, '3329162009'),
(41440, 'Dukuhbadag', 25, '3329162008'),
(41439, 'Ciseureuh', 25, '3329162007'),
(41438, 'Cikeusal Lor', 25, '3329162006'),
(41437, 'Cikeusal Kidul', 25, '3329162005'),
(41436, 'Ciduwet', 25, '3329162004'),
(41435, 'Bulakelor', 25, '3329162003'),
(41434, 'Buara', 25, '3329162002'),
(41433, 'Baros', 25, '3329162001'),
(41431, 'Siandong', 26, '3329152011'),
(41430, 'Wlahar', 26, '3329152010'),
(41429, 'Slatri', 26, '3329152009'),
(41428, 'Sitanggal', 26, '3329152008'),
(41427, 'Rengaspendawa', 26, '3329152007'),
(41426, 'Pamulihan', 26, '3329152006'),
(41425, 'Luwunggede', 26, '3329152005'),
(41424, 'Larangan', 26, '3329152004'),
(41423, 'Kedungbokor', 26, '3329152003'),
(41422, 'Karangbale', 26, '3329152002'),
(41421, 'Kamal', 26, '3329152001'),
(41419, 'Tegalglagah', 21, '3329142019'),
(41418, 'Siwuluh', 21, '3329142018'),
(41417, 'Rancawuluh', 21, '3329142017'),
(41416, 'Pulogading', 21, '3329142016'),
(41415, 'Petunjungan', 21, '3329142015'),
(41414, 'Pakijangan', 21, '3329142014'),
(41413, 'Luwungragi', 21, '3329142013'),
(41412, 'Kluwut', 21, '3329142012'),
(41411, 'Karangsari', 21, '3329142011'),
(41410, 'Jubang', 21, '3329142010'),
(41409, 'Grinting', 21, '3329142009'),
(41408, 'Dukuhlo', 21, '3329142008'),
(41407, 'Cipelem', 21, '3329142007'),
(41406, 'Cimohong', 21, '3329142006'),
(41405, 'Bulusari', 21, '3329142005'),
(41404, 'Bulakparen', 21, '3329142004'),
(41403, 'Bulakamba', 21, '3329142003'),
(41402, 'Banjaratma', 21, '3329142002'),
(41401, 'Bangsri', 21, '3329142001'),
(41399, 'Tengguli', 32, '3329132018'),
(41398, 'Tegongan', 32, '3329132017'),
(41397, 'Tanjung', 32, '3329132016'),
(41396, 'Sidakaton', 32, '3329132015'),
(41395, 'Sengon', 32, '3329132014'),
(41394, 'Sarireja', 32, '3329132013'),
(41393, 'Pengaradan', 32, '3329132012'),
(41392, 'Pejagan', 32, '3329132011'),
(41391, 'Mundu', 32, '3329132010'),
(41390, 'Luwungbata', 32, '3329132009'),
(41389, 'Lemahabang', 32, '3329132008'),
(41387, 'Kubangputat', 32, '3329132006'),
(41386, 'Krakahan', 32, '3329132005'),
(41385, 'Kemurang Wetan', 32, '3329132004'),
(41384, 'Kemurang Kulon', 32, '3329132003'),
(41383, 'Kedawung', 32, '3329132002'),
(41382, 'Karangreja', 32, '3329132001'),
(41380, 'Rungkang', 27, '3329122022'),
(41379, 'Randusari', 27, '3329122021'),
(41378, 'Randegan', 27, '3329122020'),
(41377, 'Prapag Lor', 27, '3329122019'),
(41376, 'Prapag Kidul', 27, '3329122018'),
(41375, 'Pengabean', 27, '3329122017'),
(41374, 'Pekauman', 27, '3329122016'),
(41373, 'Negla', 27, '3329122015'),
(41372, 'Losari Lor', 27, '3329122014'),
(41371, 'Losari Kidul', 27, '3329122013'),
(41369, 'Kedungneng', 27, '3329122011'),
(41368, 'Kecipir', 27, '3329122010'),
(41367, 'Karangsambung', 27, '3329122009'),
(41366, 'Karangjunti', 27, '3329122008'),
(41365, 'Karangdempel', 27, '3329122007'),
(41364, 'Kalibuntu', 27, '3329122006'),
(41362, 'Dukuhsalam', 27, '3329122004'),
(41361, 'Bojongsari', 27, '3329122003'),
(41360, 'Blubuk', 27, '3329122002'),
(41359, 'Babakan', 27, '3329122001'),
(41357, 'Sutamaja', 24, '3329112013'),
(41356, 'Sindangjaya', 24, '3329162017'),
(41354, 'Limbangan', 24, '3329122012'),
(41353, 'Kubangpari', 24, '3329112009'),
(41352, 'Kramatsampang', 24, '3329112008'),
(41351, 'Kersana', 24, '3329112007'),
(41350, 'Kradenan', 24, '3329112006'),
(41349, 'Kemukten', 24, '3329112005'),
(41348, 'Jagapura', 24, '3329112004'),
(41347, 'Cikandang', 24, '3329112003'),
(41346, 'Cigedog', 24, '3329112002'),
(41345, 'Ciampel', 24, '3329112001'),
(41343, 'Songgom Lor', 31, '3329102010'),
(41342, 'Jatimakmur', 31, '3329102009'),
(41341, 'Gegerkunci', 31, '3329102008'),
(41340, 'Wanatawang', 31, '3329102007'),
(41339, 'Wanacala', 31, '3329102006'),
(41338, 'Songgom', 31, '3329102005'),
(41337, 'Karang Sembung', 31, '3329102004'),
(41336, 'Jatirokeh', 31, '3329102003'),
(41335, 'Dukuhmaja', 31, '3329102002'),
(41334, 'Cenang', 31, '3329102001'),
(41332, 'Wangandalem', 20, '3329092023'),
(41331, 'Terlangu', 20, '3329092022'),
(41330, 'Tengki', 20, '3329092021'),
(41329, 'Sigambir', 20, '3329092020'),
(41328, 'Randusanga Wetan', 20, '3329092019'),
(41327, 'Randusanga Kulon', 20, '3329092018'),
(41326, 'Pulosari', 20, '3329092017'),
(41325, 'Pemaron', 20, '3329092016'),
(41324, 'Pagejugan', 20, '3329092014'),
(41323, 'Padasugih', 20, '3329092013'),
(41322, 'Lembarawa', 20, '3329092010'),
(41321, 'Krasak', 20, '3329092009'),
(41320, 'Kedunguter', 20, '3329092008'),
(41319, 'Kaliwlingi', 20, '3329092007'),
(41318, 'Kalimati', 20, '3329092006'),
(41317, 'Kaligangsa Wetan', 20, '3329092005'),
(41316, 'Kaligangsa Kulon', 20, '3329092004'),
(41315, 'Banjaranyar', 20, '3329092001'),
(41314, 'Pasarbatang', 20, '0'),
(41313, 'Limbangan Wetan', 20, '0'),
(41312, 'Limbangan Kulon', 20, '0'),
(41311, 'Gandasuli', 20, '0'),
(41310, 'Brebes', 20, '0'),
(41308, 'Wanasari', 34, '3329082020'),
(41307, 'Tegalgandu', 34, '3329082019'),
(41306, 'Tanjungsari', 34, '3329082018'),
(41305, 'Siwungkuk', 34, '3329082017'),
(41304, 'Sisalam', 34, '3329082016'),
(41303, 'Sigentong', 34, '3329082015'),
(41302, 'Sidamulya', 34, '3329082014'),
(41301, 'Siasem', 34, '3329082013'),
(41300, 'Sawojajar', 34, '3329082012'),
(41299, 'Pesantunan', 34, '3329082011'),
(41298, 'Pebatan', 34, '3329082010'),
(41297, 'Lengkong', 34, '3329082009'),
(41296, 'Kupu', 34, '3329082008'),
(41295, 'Klampok', 34, '3329082007'),
(41294, 'Kertabesuki', 34, '3329082006'),
(41293, 'Keboledan', 34, '3329082005'),
(41292, 'Jagalempeni', 34, '3329082004'),
(41291, 'Dumeling', 34, '3329082003'),
(41290, 'Dukuhwringin', 34, '3329082002'),
(41289, 'Glonggong', 34, '3329082001'),
(41287, 'Tembelang', 23, '3329072022'),
(41286, 'Tegalwulung', 23, '3329072021'),
(41285, 'Rengasbandung', 23, '3329072020'),
(41284, 'Pedeslohor', 23, '3329072019'),
(41283, 'Pamengger', 23, '3329072018'),
(41282, 'Kramat', 23, '3329072017'),
(41281, 'Klikiran', 23, '3329072016'),
(41280, 'Klampis', 23, '3329072015'),
(41279, 'Kertasinduyasa', 23, '3329072014'),
(41278, 'Kendawa', 23, '3329072013'),
(41277, 'Kemiriamba', 23, '3329072012'),
(41276, 'Kedungtukang', 23, '3329072011'),
(41275, 'Kebonagung', 23, '3329072010'),
(41274, 'Kebogadung', 23, '3329072009'),
(41273, 'Karanglo', 23, '3329072008'),
(41272, 'Kalipucang', 23, '3329072007'),
(41271, 'Kalialang', 23, '3329072006'),
(41270, 'Jatibarang Lor', 23, '3329072005'),
(41269, 'Jatibarang Kidul', 23, '3329072004'),
(41268, 'Janegara', 23, '3329072003'),
(41267, 'Buaran', 23, '3329072002'),
(41266, 'Bojong', 23, '3329072001'),
(41264, 'Watujaya', 33, '3329062014'),
(41263, 'Tonjong', 33, '3329062013'),
(41262, 'Tanggeran', 33, '3329062012'),
(41261, 'Purwodadi', 33, '3329062011'),
(41260, 'Rajawetan', 33, '3329062010'),
(41259, 'Purbayasa', 33, '3329062009'),
(41258, 'Pepedan', 33, '3329062008'),
(41257, 'Negarayu', 33, '3329062007'),
(41256, 'Linggapura', 33, '3329062006'),
(41255, 'Kutayu', 33, '3329062005'),
(41254, 'Kutamendala', 33, '3329062004'),
(41253, 'Karangjongkeng', 33, '3329062003'),
(41252, 'Kalijurang', 33, '3329062002'),
(41251, 'Galuh Timur', 33, '3329062001'),
(41249, 'Wanareja', 30, '3329052013'),
(41248, 'Plompong', 30, '3329052012'),
(41247, 'Sridadi', 30, '3329052011'),
(41246, 'Mlayang', 30, '3329052010'),
(41245, 'Mendala', 30, '3329052009'),
(41244, 'Manggis', 30, '3329052008'),
(41243, 'Kaliloka', 30, '3329052007'),
(41242, 'Kaligiri', 30, '3329052006'),
(41241, 'Igirklanceng', 30, '3329052005'),
(41240, 'Dawuhan', 30, '3329052004'),
(41239, 'Buniwah', 30, '3329052003'),
(41238, 'Benda', 30, '3329052002'),
(41237, 'Batursari', 30, '3329052001'),
(41235, 'Winduaji', 28, '3329042012'),
(41234, 'Wanatirta', 28, '3329042011'),
(41233, 'Taraban', 28, '3329042010'),
(41232, 'Ragatunjung', 28, '3329042009'),
(41231, 'Pandansari', 28, '3329042008'),
(41230, 'Pakujati', 28, '3329042007'),
(41229, 'Paguyangan', 28, '3329042006'),
(41228, 'Pagojengan', 28, '3329042005'),
(41227, 'Kretek', 28, '3329042004'),
(41226, 'Kedungoleng', 28, '3329042003'),
(41225, 'Cipetung', 28, '3329042002'),
(41224, 'Cilibur', 28, '3329042001'),
(41222, 'Pruwatan', 22, '3329032015'),
(41221, 'Penggarutan', 22, '3329032014'),
(41220, 'Pamijen', 22, '3329032013'),
(41219, 'Negaradaha', 22, '3329032012'),
(41218, 'Laren', 22, '3329032011'),
(41217, 'Langkap', 22, '3329032010'),
(41216, 'Kaliwadas', 22, '3329032009'),
(41215, 'Kalisumur', 22, '3329032008'),
(41214, 'Kalinusu', 22, '3329032007'),
(41213, 'Kalilangkap', 22, '3329032006'),
(41212, 'Kalierang', 22, '3329032005'),
(41211, 'Jatisawit', 22, '3329122005'),
(41210, 'Dukuhturi', 22, '3329162020'),
(41209, 'Bumiayu', 22, '3329032002'),
(41208, 'Adisana', 22, '3329032001'),
(41206, 'Waru', 19, '3329022018'),
(41205, 'Telaga', 19, '3329022017'),
(41204, 'Terlaya', 19, '3329022016'),
(41203, 'Tambakserang', 19, '3329022015'),
(41202, 'Sindangwangi', 19, '3329022014'),
(41201, 'Pengarasan', 19, '3329022013'),
(41200, 'Pangebatan', 19, '3329022012'),
(41199, 'Legok', 19, '3329022011'),
(41198, 'Kebandungan', 19, '3329022010'),
(41197, 'Karangpari', 19, '3329022009'),
(41196, 'Jipang', 19, '3329022008'),
(41195, 'Ciomas', 19, '3329022007'),
(41194, 'Cinanas', 19, '3329022006'),
(41193, 'Cibentang', 19, '3329022005'),
(41192, 'Bantarwaru', 19, '3329022004'),
(41191, 'Bantarkawung', 19, '3329022003'),
(41190, 'Banjarsari', 19, '3329022002'),
(41189, 'Bangbayang', 19, '3329022001'),
(41187, 'Windusakti', 29, '3329012021'),
(41186, 'Winduasri', 29, '3329012020'),
(41185, 'Wanoja', 29, '3329012019'),
(41184, 'Tembongraja', 29, '3329012018'),
(41183, 'Salem', 29, '3329012017'),
(41182, 'Pasirpanjang', 29, '3329012016'),
(41181, 'Pabuaran', 29, '3329012015'),
(41180, 'Kadumanis', 29, '3329012014'),
(41179, 'Indrajaya', 29, '3329012013'),
(41178, 'Gunungtajem', 29, '3329012012'),
(41177, 'Gunungsugih', 29, '3329012011'),
(41176, 'Gununglarang', 29, '3329012010'),
(41175, 'Gunungjaya', 29, '3329012009'),
(41174, 'Gandoang', 29, '3329012008'),
(41173, 'Ganggawang', 29, '3329012007'),
(41172, 'Citimbang', 29, '3329012006'),
(41171, 'Ciputih', 29, '3329012005'),
(41170, 'Capar', 29, '3329012004'),
(41169, 'Bentarsari', 29, '3329012003'),
(41168, 'Bentar', 29, '3329012002'),
(41167, 'Banjaran', 29, '3329012001');

-- --------------------------------------------------------

--
-- Table structure for table `provinsi_provinsi`
--

CREATE TABLE `provinsi_provinsi` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `code` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `provinsi_provinsi`
--

INSERT INTO `provinsi_provinsi` (`id`, `name`, `code`) VALUES
(32676, 'Jawa Tengah', 33);

-- --------------------------------------------------------

--
-- Table structure for table `sequences_sequence`
--

CREATE TABLE `sequences_sequence` (
  `name` varchar(100) NOT NULL,
  `last` int(10) UNSIGNED NOT NULL CHECK (`last` >= 0)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_printcard`
--

CREATE TABLE `user_printcard` (
  `id` int(11) NOT NULL,
  `order` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_user`
--

CREATE TABLE `user_user` (
  `id` int(11) NOT NULL,
  `password` varchar(128) NOT NULL,
  `last_login` datetime(6) DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(50) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `mother_name` varchar(50) DEFAULT NULL,
  `nik` varchar(50) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `rt` varchar(50) DEFAULT NULL,
  `rw` varchar(50) DEFAULT NULL,
  `mobile_number` varchar(30) NOT NULL,
  `picture` varchar(100) NOT NULL,
  `ktp` varchar(100) NOT NULL,
  `from_web` tinyint(1) NOT NULL,
  `dob` date DEFAULT NULL,
  `gender` varchar(10) NOT NULL,
  `education` varchar(255) DEFAULT NULL,
  `kabupaten_id` int(11) DEFAULT NULL,
  `kecamatan_id` int(11) DEFAULT NULL,
  `kelurahan_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `print_counter` int(11) NOT NULL,
  `provinsi_id` int(11) DEFAULT NULL,
  `job` varchar(20) DEFAULT NULL,
  `marital_status` varchar(10) NOT NULL,
  `place_of_birth` varchar(50) DEFAULT NULL,
  `age_year` decimal(50,2) NOT NULL,
  `age_month` decimal(50,2) NOT NULL,
  `age_string` varchar(250) DEFAULT NULL,
  `is_pregnant` tinyint(1) NOT NULL,
  `is_breeding` tinyint(1) NOT NULL,
  `parent_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_user`
--

INSERT INTO `user_user` (`id`, `password`, `last_login`, `is_superuser`, `username`, `name`, `mother_name`, `nik`, `is_active`, `is_staff`, `rt`, `rw`, `mobile_number`, `picture`, `ktp`, `from_web`, `dob`, `gender`, `education`, `kabupaten_id`, `kecamatan_id`, `kelurahan_id`, `order`, `print_counter`, `provinsi_id`, `job`, `marital_status`, `place_of_birth`, `age_year`, `age_month`, `age_string`, `is_pregnant`, `is_breeding`, `parent_id`) VALUES
(1, 'pbkdf2_sha256$260000$qg4K6ty6XFUkZfJ1LI3FCc$FIl5lu7pX6IJ/hxlBwimGs55rildZCVIEapVDtxUmOo=', '2022-10-18 13:16:57.248347', 1, 'superadmin', NULL, NULL, NULL, 1, 1, NULL, NULL, '', '', '', 0, NULL, '', NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, 'Lajang', NULL, '0.00', '0.00', NULL, 0, 0, NULL),
(13, 'pbkdf2_sha256$260000$EZJB8UoNbVtfIapy0yCP6D$GVwU/OQj3zFsBsllt2zY+QOjKJGbJTVw9udhCIFbt74=', '2022-10-08 04:44:48.934038', 0, 'gununglarang', 'TRI MAE HARTINI', NULL, '3329014305780007', 1, 1, NULL, NULL, '', '', '', 0, '1978-05-03', 'P', NULL, 41165, 29, 41176, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '44.00', '533.00', '44 th, 5 bln', 0, 0, 1),
(12, 'pbkdf2_sha256$260000$fRw5gswApoHgjuP1Yt5Out$rzuJeI7NOcfKQxlNxY+JdWNpIUlhN+nKJawPaeB5B9E=', '2022-10-10 15:35:21.116337', 0, 'indrajaya', 'DINA FERLINA', NULL, '3329015302870005', 1, 1, NULL, NULL, '', '', '', 0, '1987-02-13', 'P', NULL, 41165, 29, 41179, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '35.00', '427.00', '35 th, 7 bln', 0, 0, 1),
(10, '', NULL, 0, '121212121212121', 'ALUDRA RUMAISHA', NULL, '3329014101210002', 1, 0, '04', '01', '', '', '', 0, '2021-01-01', 'P', NULL, 41165, 29, 41170, 0, 0, NULL, NULL, 'Lajang', 'Brebes', '1.00', '21.00', '1 th, 9 bln', 0, 0, 9),
(11, 'pbkdf2_sha256$260000$QnRjVVcxg0JmlGHLSdv6uN$5lRGqpUzd6GhfWGxBEIkGI4STp0aa8NLldnxWBSOaeI=', '2022-10-10 15:37:08.622386', 0, 'ganggawang', 'YUYUN WIDYAYANTI,S.ST', NULL, '123456557698', 1, 1, '01', '01', '', '', '', 0, '1989-06-09', 'P', NULL, 41165, 29, 41173, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '33.00', '399.00', '33 th, 3 bln', 0, 0, 1),
(9, 'pbkdf2_sha256$260000$RuTjxNNvvsuimggporMNFl$Bd2dInTv/X3WSRC70WQ0hEHPFQwkQu4fjZt14dme8Oc=', '2022-10-21 02:38:24.560738', 0, 'capar', 'TITI YUNIARUM', NULL, '3329014409910001', 1, 1, '05', '01', '', '', '', 0, '1991-09-04', 'P', NULL, 41165, 29, 41170, 0, 0, NULL, NULL, 'Lajang', 'Brbes', '31.00', '372.00', '31 th, 0 bln', 0, 0, 1),
(14, 'pbkdf2_sha256$260000$dawSDKhyqrS1RmdB1I6TqP$wz4h7iyp7csz8sw1BAixjXrZT+jfh1FAL+ppzqNmKWo=', '2022-10-10 15:34:41.373878', 0, 'gunungjaya', 'YULI KURNAENI', NULL, '3329016707860004', 1, 1, NULL, NULL, '', '', '', 0, '1986-07-27', 'P', NULL, 41165, 29, 41175, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '36.00', '434.00', '36 th, 2 bln', 0, 0, 1),
(15, 'pbkdf2_sha256$260000$U616JeF0ZWm7hwKomm32x4$KKWjN96uuD30p57AH4hG9r+g1X3QsIqvngrfhpm8wLA=', '2022-10-10 15:36:36.192761', 0, 'gunungsugih', 'YUYUN AGUSTINA', NULL, '3329016208830006', 1, 1, NULL, NULL, '', '', '', 0, '1983-08-22', 'P', NULL, 41165, 29, 41177, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '39.00', '469.00', '39 th, 1 bln', 0, 0, 1),
(16, '', NULL, 0, '7ca8cd199ac144e981fd57fc53c70e', 'BILQIS FASHIHATUL ALAWIYAH', NULL, '3329017108220001', 1, 0, '03', '01', '', '', '', 0, '2022-08-31', 'P', NULL, 41165, 29, 41170, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '0.00', '1.00', '0 th, 1 bln', 0, 0, 9),
(17, '', NULL, 0, '77f45c9982174617b6786302a52046', 'SVARGA AKIO UWAIS SANTOSO', NULL, '3329011103210004', 1, 0, '05', '01', '', '', '', 0, '2021-03-11', 'L', NULL, 41165, 29, 41170, 0, 0, NULL, NULL, 'Lajang', 'Brebes', '1.00', '19.00', '1 th, 7 bln', 0, 0, 9),
(18, '', NULL, 0, '6af52521b6dd4b6c957d717f18d6f3', 'DIRO ALI FAJAR', NULL, '3329012603210001', 1, 0, '04', '01', '', '', '', 0, '2021-03-26', 'L', NULL, 41165, 29, 41170, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '1.00', '18.00', '1 th, 6 bln', 0, 0, 9),
(19, '', NULL, 0, '9fc8abd596b047b3b4e4efc2397bea', 'M RAGIL RAMADHAN', NULL, '3329011904210001', 1, 0, '03', '01', '', '', '', 0, '2021-04-19', 'L', NULL, 41165, 29, 41170, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '1.00', '18.00', '1 th, 6 bln', 0, 0, 9),
(20, '', NULL, 0, 'fa29255062a443c89be4fc48cab968', 'FAHMI AL FAUZI', NULL, '3329012105210001', 1, 0, '05', '01', '', '', '', 0, '2021-05-21', 'L', NULL, 41165, 29, 41170, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '1.00', '17.00', '1 th, 5 bln', 0, 0, 9),
(21, '', NULL, 0, '20a333c67cee41ab915f2306f657b0', 'M ALWI AMMIL FAQIH', NULL, '3329011106210002', 1, 0, '04', '01', '', '', '', 0, '2021-06-11', 'L', NULL, 41165, 29, 41170, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '1.00', '16.00', '1 th, 4 bln', 0, 0, 9),
(22, '', NULL, 0, 'e64462df147d4db2861f314c8b4065', 'AIRA NUR FATIMAH', NULL, '3329015509210002', 1, 0, '02', '02', '', '', '', 0, '2021-09-15', 'P', NULL, 41165, 29, 41170, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '1.00', '13.00', '1 th, 1 bln', 0, 0, 9),
(23, '', NULL, 0, 'e43053df86544b4cbe70fe1030f884', 'RENALDI ALVARO', NULL, '3329010511210000', 1, 0, '04', '01', '', '', '', 0, '2021-11-05', 'L', NULL, 41165, 29, 41170, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '0.00', '11.00', '0 th, 11 bln', 0, 0, 9),
(24, '', NULL, 0, '914715ac91094b28932cadb6f2d804', 'TAUFAN ALGHANI', NULL, '3329012411210000', 1, 0, '04', '01', '', '', '', 0, '2021-11-24', 'L', NULL, 41165, 29, 41170, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '0.00', '10.00', '0 th, 10 bln', 0, 0, 9),
(25, '', NULL, 0, '9b47e5ffe2904e7eb794d453ca4389', 'CANDRA SAPUTRA', NULL, '3329010509170001', 1, 0, '02', '02', '', '', '', 0, '2017-09-05', 'L', NULL, 41165, 29, 41170, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '5.00', '61.00', '5 th, 1 bln', 0, 0, 9),
(26, '', NULL, 0, 'f9ac61e131f645fdbca8b1a9f42de0', 'KEZA AL FIRDAUS', NULL, '3329011709170001', 1, 0, '03', '01', '', '', '', 0, '2017-09-17', 'L', NULL, 41165, 29, 41170, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '5.00', '61.00', '5 th, 1 bln', 0, 0, 9),
(27, '', NULL, 0, '72cfa1d55f034d4ab7c5e5f5c58228', 'AISYAH NURAENI', NULL, '3329014410170003', 1, 0, '03', '01', '', '', '', 0, '2017-10-04', 'P', NULL, 41165, 29, 41170, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '5.00', '60.00', '5 th, 0 bln', 0, 0, 9),
(28, '', NULL, 0, 'e81f566fce5e4d0eab42d773caf854', 'TEMI ANUGERAH SAPUTRA', NULL, '3329012211170001', 1, 0, '01', '02', '', '', '', 0, '2017-11-22', 'L', NULL, 41165, 29, 41170, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '4.00', '58.00', '4 th, 10 bln', 0, 0, 9),
(29, '', NULL, 0, 'e98b2816827b4a2f9bd22ec15a2b85', 'SILPIA NUR KHOLIPAH', NULL, '3329015412170001', 1, 0, '01', '02', '', '', '', 0, '2017-12-14', 'P', NULL, 41165, 29, 41170, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '4.00', '58.00', '4 th, 10 bln', 0, 0, 9),
(30, '', NULL, 0, '589233dcf04a440aa02f7f6e10b449', 'MARYAM AINI KAMILA', NULL, '3329017012170002', 1, 0, '03', '01', '', '', '', 0, '2017-12-30', 'P', NULL, 41165, 29, 41170, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '4.00', '57.00', '4 th, 9 bln', 0, 0, 9),
(31, '', NULL, 0, '221959608569438da2778a7589b509', 'WINDI PUTRI LESTARI', NULL, '3329016701180001', 1, 0, '04', '01', '', '', '', 0, '2018-01-27', 'P', NULL, 41165, 29, 41170, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '4.00', '56.00', '4 th, 8 bln', 0, 0, 9),
(32, '', NULL, 0, '42a92adf9f9b4d90ae9f7c623b942e', 'DEVANO BRAMA SETIO', NULL, '3329010202180003', 1, 0, '05', '01', '', '', '', 0, '2018-02-02', 'L', NULL, 41165, 29, 41170, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '4.00', '56.00', '4 th, 8 bln', 0, 0, 9),
(33, '', NULL, 0, 'fc6db529c2f14c2d8b8716226f8399', 'AFIFAH FITRIAWATI', NULL, '3329014306180001', 1, 0, '03', '01', '', '', '', 0, '2018-06-03', 'P', NULL, 41165, 29, 41170, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '4.00', '52.00', '4 th, 4 bln', 0, 0, 9),
(34, '', NULL, 0, '82b4efd8e9154eb78b0f8b584b0e92', 'QUEENSA SYIFATUNNISA', NULL, '3329014706180001', 1, 0, '04', '01', '', '', '', 0, '2018-06-07', 'P', NULL, 41165, 29, 41170, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '4.00', '52.00', '4 th, 4 bln', 0, 0, 9),
(35, '', NULL, 0, 'efaf39a2b6b34b198235e3496e7057', 'ELSYA FITRIA R', NULL, '3329014906180002', 1, 0, '05', '01', '', '', '', 0, '2018-06-09', 'P', NULL, 41165, 29, 41170, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '4.00', '52.00', '4 th, 4 bln', 0, 0, 9),
(36, '', NULL, 0, 'b02ac112392f4d938bfbf7bfa5f9de', 'CIKA KAMELIA PUTRI', NULL, '3329016108180001', 1, 0, '01', '02', '', '', '', 0, '2018-08-21', 'P', NULL, 41165, 29, 41170, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '4.00', '50.00', '4 th, 2 bln', 0, 0, 9),
(37, '', NULL, 0, '8f54231bc457477694cfba335a0601', 'IMAN NUGROHO', NULL, '3329012308180001', 1, 0, '04', '01', '', '', '', 0, '2018-08-23', 'L', NULL, 41165, 29, 41170, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '4.00', '49.00', '4 th, 1 bln', 0, 0, 9),
(38, '', NULL, 0, '61439b7ac13b45c4a9d0cbdcec9ad4', 'RENITA PUTRI', NULL, '3329014309180001', 1, 0, '02', '02', '', '', '', 0, '2018-09-03', 'P', NULL, 41165, 29, 41170, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '4.00', '49.00', '4 th, 1 bln', 0, 0, 9),
(39, '', NULL, 0, '1f9409c78ef54fe496a7f396c4a4f2', 'KARINA BILQIS FATINA', NULL, '3329016810180001', 1, 0, '03', '01', '', '', '', 0, '2018-10-27', 'P', NULL, 41165, 29, 41170, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '3.00', '47.00', '3 th, 11 bln', 0, 0, 9),
(40, '', NULL, 0, 'e869e7aed29d459698d9ffd0b3d6d4', 'AZKA MAULANA', NULL, '3329011212180001', 1, 0, '01', '02', '', '', '', 0, '2018-12-12', 'L', NULL, 41165, 29, 41170, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '3.00', '46.00', '3 th, 10 bln', 0, 0, 9),
(41, '', NULL, 0, '7f1c212e154848bb8131b181b4052c', 'MASITOH ALMUNAWAROH', NULL, '3329015812180001', 1, 0, '02', '02', '', '', '', 0, '2018-12-18', 'P', NULL, 41165, 29, 41170, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '3.00', '46.00', '3 th, 10 bln', 0, 0, 9),
(42, '', NULL, 0, '2c14ed1d06174613be5a36b92a6567', 'RAFASYA RIZKI HANIFAH', NULL, '3329016412180001', 1, 0, '01', '01', '', '', '', 0, '2018-12-24', 'P', NULL, 41165, 29, 41170, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '3.00', '45.00', '3 th, 9 bln', 0, 0, 9),
(43, '', NULL, 0, '4e9e3bb5bad940eeaea54074f5cb4c', 'DZAKIRA NUR ALIFAH', NULL, '3329012901190002', 1, 0, '05', '01', '', '', '', 0, '2018-12-28', 'P', NULL, 41165, 29, 41170, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '3.00', '45.00', '3 th, 9 bln', 0, 0, 9),
(44, '', NULL, 0, '700da06c806d4cf68ebea3c81ff1a6', 'SYIFANAZIA PUTRI K', NULL, '3329015701190001', 1, 0, '04', '01', '', '', '', 0, '2019-01-17', 'P', NULL, 41165, 29, 41170, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '3.00', '45.00', '3 th, 9 bln', 0, 0, 9),
(45, '', NULL, 0, '10e6378f5c564f9ca695684a6a2d18', 'TIARA SERLINDA', NULL, '3329014105190001', 1, 0, '02', '02', '', '', '', 0, '2019-05-01', 'P', NULL, 41165, 29, 41170, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '3.00', '41.00', '3 th, 5 bln', 0, 0, 9),
(46, '', NULL, 0, '8e3047d8f1164110b5892120fa8ff2', 'ILHAM RAMADHAN', NULL, '3329012805190001', 1, 0, '01', '02', '', '', '', 0, '2019-05-28', 'L', NULL, 41165, 29, 41170, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '3.00', '40.00', '3 th, 4 bln', 0, 0, 9),
(47, '', NULL, 0, '93dcd83240604b5ba8417cdf2a7c95', 'ABDUL KHOLIK', NULL, '3329010206190002', 1, 0, '05', '01', '', '', '', 0, '2019-06-02', 'L', NULL, 41165, 29, 41170, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '3.00', '40.00', '3 th, 4 bln', 0, 0, 9),
(48, '', NULL, 0, 'f24e60922d094ec9a74d7e3765b19d', 'TAMARA DWI FITRIA', NULL, '3329014506190001', 1, 0, '04', '01', '', '', '', 0, '2019-06-05', 'P', NULL, 41165, 29, 41170, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '3.00', '40.00', '3 th, 4 bln', 0, 0, 9),
(49, '', NULL, 0, '3a9157ca9f5647e683d990bfba1e78', 'SULISTIAWATI', NULL, '3329015306190002', 1, 0, '03', '01', '', '', '', 0, '2019-06-13', 'P', NULL, 41165, 29, 41170, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '3.00', '40.00', '3 th, 4 bln', 0, 0, 9),
(50, '', NULL, 0, 'f7e50f102481470caa831ad1dc08b6', 'ARDIANSYAH RIZKI P', NULL, '3329011407190002', 1, 0, '01', '02', '', '', '', 0, '2019-07-14', 'L', NULL, 41165, 29, 41170, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '3.00', '39.00', '3 th, 3 bln', 0, 0, 9),
(51, '', NULL, 0, 'c12f47f151654397b02a5b2b9058cb', 'RENDI SAPUTRA', NULL, '3329010508190001', 1, 0, '02', '01', '', '', '', 0, '2019-08-05', 'L', NULL, 41165, 29, 41170, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '3.00', '38.00', '3 th, 2 bln', 0, 0, 9),
(52, '', NULL, 0, '72fc54770a104a8baa5744f729b1ba', 'SALMA RIZKI AMANDA', NULL, '3329016008190001', 1, 0, '03', '01', '', '', '', 0, '2019-08-20', 'P', NULL, 41165, 29, 41170, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '3.00', '38.00', '3 th, 2 bln', 0, 0, 9),
(53, '', NULL, 0, 'dd1123aeeeb342a286fbac324ba131', 'CHAIRA ARSYILIA SALMA', NULL, '3329010708190003', 1, 0, '01', '02', '', '', '', 0, '2019-08-31', 'P', NULL, 41165, 29, 41170, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '3.00', '37.00', '3 th, 1 bln', 0, 0, 9),
(54, '', NULL, 0, 'ec17f1e773674455adbfd0f8a8ba0b', 'M AZAM PADILAH', NULL, '3329010209190001', 1, 0, NULL, '01', '', '', '', 0, '2019-09-02', 'L', NULL, 41165, 29, 41170, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '3.00', '37.00', '3 th, 1 bln', 0, 0, 9),
(55, '', NULL, 0, 'ae2f7fbfe0494f4886b844cabe5315', 'KHOERUL M RIFKI', NULL, '3329012509190001', 1, 0, '02', '02', '', '', '', 0, '2019-09-25', 'L', NULL, 41165, 29, 41170, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '3.00', '36.00', '3 th, 0 bln', 0, 0, 9),
(56, '', NULL, 0, '24448d2b902b4eb5888ac276fa6e5c', 'ZULPA SELPI AMANDA', NULL, '3329014612190001', 1, 0, '05', '01', '', '', '', 0, '2019-12-06', 'P', NULL, 41165, 29, 41170, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '2.00', '34.00', '2 th, 10 bln', 0, 0, 9),
(57, '', NULL, 0, '3791dac01dab48e4b6b3a10934fe1c', 'RAZKIYA SILVIANA', NULL, '3329014601200001', 1, 0, '04', '01', '', '', '', 0, '2020-01-06', 'P', NULL, 41165, 29, 41170, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '2.00', '33.00', '2 th, 9 bln', 0, 0, 9),
(58, '', NULL, 0, 'a9e476478d2d40ac8cb4faef0d75ab', 'SELA AMANDA PUTRI', NULL, '332901570120002', 1, 0, '02', '01', '', '', '', 0, '2020-01-17', 'P', NULL, 41165, 29, 41170, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '2.00', '33.00', '2 th, 9 bln', 0, 0, 9),
(59, '', NULL, 0, 'fc35a5cde5c84483815a6aa3af7491', 'MUHAMMAD WISNU', NULL, '3329012902200001', 1, 0, '03', '01', '', '', '', 0, '2020-02-29', 'L', NULL, 41165, 29, 41170, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '2.00', '31.00', '2 th, 7 bln', 0, 0, 9),
(60, '', NULL, 0, '26f3fc888eed44b49456ddf86ce325', 'ZAHRA KHOERRUNNISA', NULL, '3329016003200002', 1, 0, '03', '01', '', '', '', 0, '2020-03-20', 'P', NULL, 41165, 29, 41170, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '2.00', '31.00', '2 th, 7 bln', 0, 0, 9),
(61, '', NULL, 0, 'd6fb0fc5576a4059b5dfe0756578ab', 'MUHAMMAD FATHAN', NULL, '3329011004200002', 1, 0, '03', '01', '', '', '', 0, '2020-04-10', 'L', NULL, 41165, 29, 41170, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '2.00', '30.00', '2 th, 6 bln', 0, 0, 9),
(62, '', NULL, 0, 'ce2f96ce2c75474db5692cba831870', 'TANIA AMILIA', NULL, '3329014306200001', 1, 0, '05', '01', '', '', '', 0, '2020-06-03', 'P', NULL, 41165, 29, 41170, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '2.00', '28.00', '2 th, 4 bln', 0, 0, 9),
(63, '', NULL, 0, '28a280077a5946bc99b7b722c2a0f3', 'M NIZARUL ARIFIN', NULL, '3329010809200001', 1, 0, '03', '01', '', '', '', 0, '2020-09-08', 'L', NULL, 41165, 29, 41170, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '2.00', '25.00', '2 th, 1 bln', 0, 0, 9),
(64, '', NULL, 0, '8ab778733c704519a4185c1355fa14', 'NUWAIRA SHANA A', NULL, '3329014410200001', 1, 0, '02', '02', '', '', '', 0, '2020-10-04', 'P', NULL, 41165, 29, 41170, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '2.00', '24.00', '2 th, 0 bln', 0, 0, 9),
(65, '', NULL, 0, 'b75419f2762244cd9224bb1bbec21e', 'M RENDRA PUTRA', NULL, '3329011010200003', 1, 0, '02', '01', '', '', '', 0, '2020-10-10', 'L', NULL, 41165, 29, 41170, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '2.00', '24.00', '2 th, 0 bln', 0, 0, 9),
(66, '', NULL, 0, '1a5f5441f22d4fe991de94d35e2a45', 'M IKHSAN MAULANA', NULL, '3329010411200002', 1, 0, '03', '01', '', '', '', 0, '2020-11-04', 'L', NULL, 41165, 29, 41170, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '1.00', '23.00', '1 th, 11 bln', 0, 0, 9),
(67, '', NULL, 0, '1bcbedd9681c46b2b716f8843978db', 'ALVINO ISMAIL HANAFI', NULL, '3329010511200001', 1, 0, '05', '01', '', '', '', 0, '2020-11-05', 'L', NULL, 41165, 29, 41170, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '1.00', '23.00', '1 th, 11 bln', 0, 0, 9),
(68, '', NULL, 0, '82798057828d4e5babe2191b65243e', 'M AZAN ALBANI', NULL, '33290111111101', 1, 0, '05', '01', '', '', '', 0, '2020-11-03', 'L', NULL, 41165, 29, 41170, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '1.00', '23.00', '1 th, 11 bln', 0, 0, 9),
(69, '', NULL, 0, '07c8f9a339a4418b875f4edcc74a9e', 'AWAN KURNIAWAN', NULL, '3329011511200001', 1, 0, '05', '01', '', '', '', 0, '2020-11-15', 'L', NULL, 41165, 29, 41170, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '1.00', '23.00', '1 th, 11 bln', 0, 0, 9),
(70, '', NULL, 0, '3cc04e7726354a60b4b5fb27e71389', 'AZKIA NUR LATIFAH', NULL, '1323565', 1, 0, '05', '01', '', '', '', 0, '2022-08-19', 'P', NULL, 41165, 29, 41170, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '0.00', '2.00', '0 th, 2 bln', 0, 0, 9),
(71, '', NULL, 0, '0e7d527f70944d16b322e24d3f1a28', 'TURDI M ARDIAN', NULL, '1234568', 1, 0, '03', '01', '', '', '', 0, '2022-05-24', 'L', NULL, 41165, 29, 41170, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '0.00', '4.00', '0 th, 4 bln', 0, 0, 9),
(72, '', NULL, 0, '65a751bc301d4878af1112898e8e12', 'CELLINE GEA ADINDA', NULL, '123456', 1, 0, '03', '01', '', '', '', 0, '2022-06-14', 'P', NULL, 41165, 29, 41170, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '0.00', '4.00', '0 th, 4 bln', 0, 0, 9),
(73, '', NULL, 0, '57ef1f7bb88246b8b3863a1703abdb', 'KHANZA NAURA BUDIANTO', NULL, '3329015403220001', 1, 0, '02', '02', '', '', '', 0, '2022-03-14', 'P', NULL, 41165, 29, 41170, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '0.00', '7.00', '0 th, 7 bln', 0, 0, 9),
(74, '', NULL, 0, 'd2058376a6a94eecb49923113e34f7', 'INES CAHAYA FEBRIANTI', NULL, '3329015502220001', 1, 0, '02', '02', '', '', '', 0, '2022-02-15', 'P', NULL, 41165, 29, 41170, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '0.00', '8.00', '0 th, 8 bln', 0, 0, 9),
(75, '', NULL, 0, 'e8f16546a6614d1e8303a6ef17ed21', 'ABILA KANAYA D', NULL, '123456FDH', 1, 0, '03', '01', '', '', '', 0, '2022-02-03', 'P', NULL, 41165, 29, 41170, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '0.00', '8.00', '0 th, 8 bln', 0, 0, 9),
(76, '', NULL, 0, 'f5256b884f7242aa9450aa3c3f0193', 'REDZA SATYA PRATAMA', NULL, '332901123', 1, 0, '01', '02', '', '', '', 0, '2021-12-05', 'L', NULL, 41165, 29, 41170, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '0.00', '10.00', '0 th, 10 bln', 0, 0, 9),
(77, '', NULL, 0, 'de9296f9c8924c599718dd3b9b7e8d', 'WILLIAM VALON TOVANO', NULL, '3329012301220001', 1, 0, '02', '02', '', '', '', 0, '2022-01-23', 'L', NULL, 41165, 29, 41170, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '0.00', '8.00', '0 th, 8 bln', 0, 0, 9),
(78, '', NULL, 0, '6c004ccbc2454f3eadac90062d3d74', 'TASWINI', NULL, '3329016308890005', 1, 0, '03', '01', '', '', '', 0, '1989-08-23', 'P', NULL, 41165, 29, 41170, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '33.00', '398.00', '33 th, 2 bln', 1, 0, 9),
(79, '', NULL, 0, '6bc5e288cc2a4758890eb314ea145f', 'IKA KARTIKA', NULL, '3329014306900007', 1, 0, '03', '01', '', '', '', 0, '1990-06-03', 'P', NULL, 41165, 29, 41170, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '32.00', '388.00', '32 th, 4 bln', 1, 0, 9),
(80, '', NULL, 0, '9bcfde69fc16458cb6488218d96d31', 'EHAYATI', NULL, '3329012206860001', 1, 0, '02', '01', '', '', '', 0, '1995-01-18', 'P', NULL, 41165, 29, 41170, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '27.00', '333.00', '27 th, 9 bln', 1, 0, 9),
(81, 'pbkdf2_sha256$260000$nQJ1bSvlOEAE5LP0FqCqYZ$QMqJ+w/Cwsn0l4B6ELcqH4C/kZiQZdgRgQnT1xEzgKg=', '2022-10-09 14:21:25.795616', 0, 'citimbang', 'IMAS ALIYUN MAFIDA', NULL, '123456789120', 1, 1, '01', '01', '', '', '', 0, '2415-01-11', 'P', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '-393.00', '-4710.00', '-393 th, 6 bln', 0, 0, 1),
(82, 'pbkdf2_sha256$260000$wbAZ0tGwyamgwaBlyDXe2B$2jbV98n58Ls6fhFIP3OvgYcYUwcdLh19WZlEkVpYAHo=', '2022-10-14 12:45:35.193251', 0, 'banjaran', 'DEDE MULYATI', NULL, '12356989', 1, 1, '03', '01', '', '', '', 0, '2000-01-01', 'P', NULL, 41165, 29, 41167, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '22.00', '273.00', '22 th, 9 bln', 0, 0, 1),
(83, 'pbkdf2_sha256$260000$N1WJyuJtYJ5rYWPjC6y45M$azLakYcrPw/iwAP+wGLb91sbWeK9NjxGgkjQYfrDtsU=', '2022-09-30 03:08:25.210939', 0, 'salem', 'BETTY YULISTIANA', NULL, '1233569', 1, 1, '01', '05', '', '', '', 0, '1900-01-01', 'P', NULL, 41165, 29, 41183, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '122.00', '1473.00', '122 th, 9 bln', 0, 0, 1),
(84, 'pbkdf2_sha256$260000$baRKeAQtFgwjkqtSj4R4m8$0GD6jQe/rvtSfotsKKWHXks5lpe6Zsr4mX2mX0bZ+6c=', '2022-09-30 03:05:39.134910', 0, 'tembongraja', 'YAYAH HOPIAH', NULL, '123563569', 1, 1, '02', '01', '', '', '', 0, '1856-01-01', 'P', NULL, 41165, 29, 41184, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '166.00', '2002.00', '166 th, 10 bln', 0, 0, 1),
(85, 'pbkdf2_sha256$260000$evrar4mDDGuVjsdMO8A2IN$42SAeA84BI6/D7kcuFTvIzIhdMD3xjywgaJKLR7JtNc=', '2022-09-30 03:05:29.363632', 0, 'gunungtajem', 'RINI SETYASTITI', NULL, '12345698', 1, 1, '01', '01', '', '', '', 0, '1989-01-01', 'P', NULL, 41165, 29, 41178, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '33.00', '405.00', '33 th, 9 bln', 0, 0, 1),
(86, 'pbkdf2_sha256$260000$4MbQ6YGfxK5mTCii0a7CwE$laMXWnlYixHqwwAiyQzNmLJl25qf9zA+P7qK2ZHyzks=', '2022-09-30 03:05:57.878367', 0, 'windusakti', 'MAYA ANZARSARI', NULL, '1354689', 1, 1, '01', '01', '', '', '', 0, '4510-12-12', 'P', NULL, 41165, 29, 41187, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '-2489.00', '-29878.00', '-2489 th, -10 bln', 0, 0, 1),
(87, 'pbkdf2_sha256$260000$gR6XWJ1D5fkhjDK1vYv6Li$b+jZihiIwxy1VbiZlpaJdNmDuAuPA0LuUsRNIs61TCM=', '2022-10-01 02:17:30.000557', 0, 'winduasri', 'YOSI ROSMAYANTI', NULL, '13248695', 1, 1, '01', '01', '', '', '', 0, '1990-01-01', 'P', NULL, 41165, 29, 41186, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '32.00', '393.00', '32 th, 9 bln', 0, 0, 1),
(88, '', NULL, 0, 'd0fbf4e326f549048f523c1da18357', 'SILVIA BINTANG BERLIAN', NULL, '134568', 1, 0, '01', '01', '', '', '', 0, '2022-07-28', 'P', NULL, 41165, 29, 41186, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '0.00', '2.00', '0 th, 2 bln', 0, 0, 87),
(89, '', NULL, 0, '57eaa7321bff4b28a203a5ddbad9b5', 'CANDRA JULIANA PRATAMA', NULL, '13223456', 1, 0, '01', '01', '', '', '', 0, '2022-07-16', 'L', NULL, 41165, 29, 41186, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '0.00', '2.00', '0 th, 2 bln', 0, 0, 87),
(90, '', NULL, 0, 'f7ed23591d1e46c7a64b4fe35db54b', 'M FAHRI AL FAQIH', NULL, '132456', 1, 0, '01', '01', '', '', '', 0, '2022-08-05', 'L', NULL, 41165, 29, 41186, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '0.00', '1.00', '0 th, 1 bln', 0, 0, 87),
(91, '', NULL, 0, '1cb1e5b62fd247169b2beb4df2063b', 'M HANIF MAULANA', NULL, '332901705220001', 1, 0, '01', '01', '', '', '', 0, '2022-05-17', 'L', NULL, 41165, 29, 41186, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '0.00', '4.00', '0 th, 4 bln', 0, 0, 87),
(92, '', NULL, 0, 'c34d89d75a04472f9eca755f6cdf62', 'TAHFI MALIK AL FATHAN', NULL, '123456658', 1, 0, '01', '01', '', '', '', 0, '2022-05-12', 'L', NULL, 41165, 29, 41186, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '0.00', '4.00', '0 th, 4 bln', 0, 0, 87),
(93, '', NULL, 0, 'bdf7466c4eab4275983f84d902e6a9', 'M DZIKRI AL MUJAHID', NULL, '3329010504220001', 1, 0, '01', '01', '', '', '', 0, '2022-04-05', 'L', NULL, 41165, 29, 41186, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '0.00', '5.00', '0 th, 5 bln', 0, 0, 87),
(94, '', NULL, 0, '954aa29d385547a0af38cccd2088c6', 'AL ZAHRA RADIA ULMIRA', NULL, '3329014203220001', 1, 0, '01', '01', '', '', '', 0, '2022-03-02', 'P', NULL, 41165, 29, 41186, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '0.00', '7.00', '0 th, 7 bln', 0, 0, 87),
(95, '', NULL, 0, 'c72c8607711a4cdd85a974731a311c', 'RIKI ALFIANSYAH', NULL, '3329012911210001', 1, 0, '01', '01', '', '', '', 0, '2021-11-29', 'L', NULL, 41165, 29, 41186, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '0.00', '10.00', '0 th, 10 bln', 0, 0, 87),
(96, '', NULL, 0, '4929402bd1094de8a22030d555cab8', 'RAFIKA OPIKA', NULL, '3329014310210001', 1, 0, '02', '01', '', '', '', 0, '2021-10-03', 'P', NULL, 41165, 29, 41186, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '0.00', '11.00', '0 th, 11 bln', 0, 0, 87),
(97, '', NULL, 0, 'cd9ecfebeb7d4e2d89be8840f62922', 'HAIKAL SEPTIANA', NULL, '3329011609210002', 1, 0, '01', '01', '', '', '', 0, '2021-09-17', 'L', NULL, 41165, 29, 41186, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '1.00', '12.00', '1 th, 0 bln', 0, 0, 87),
(98, '', NULL, 0, '7e74b28b8c2d497294e03748310e50', 'M GHANI AL FATHAN', NULL, '3329011509210001', 1, 0, '02', '01', '', '', '', 0, '2021-09-15', 'L', NULL, 41165, 29, 41186, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '1.00', '12.00', '1 th, 0 bln', 0, 0, 87),
(99, '', NULL, 0, 'c9ddc30e847a47e393d19f3feb05b0', 'FIKA RIANI PUTRI', NULL, '3329014906210001', 1, 0, '01', '01', '', '', '', 0, '2021-06-09', 'P', NULL, 41165, 29, 41186, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '1.00', '15.00', '1 th, 3 bln', 0, 0, 87),
(100, '', NULL, 0, '6dcf9fc9180b44a8be39c11e606ce2', 'GIBRAN RAFAEYZA ZIFARY', NULL, '3329012105210004', 1, 0, '01', '01', '', '', '', 0, '2021-05-21', 'L', NULL, 41165, 29, 41186, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '1.00', '16.00', '1 th, 4 bln', 0, 0, 87),
(101, '', NULL, 0, '461a60ef3f0b4c42981858debfe256', 'WILDAN ADI SAPUTRA', NULL, '3329012511170002', 1, 0, '01', '01', '', '', '', 0, '2017-11-25', 'L', NULL, 41165, 29, 41186, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '4.00', '58.00', '4 th, 10 bln', 0, 0, 87),
(102, '', NULL, 0, '1670ea9f730a4e82811aa4742f233c', 'ARDIANA SAPUTRA', NULL, '3329010205180001', 1, 0, '02', '01', '', '', '', 0, '2018-05-01', 'L', NULL, 41165, 29, 41186, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '4.00', '53.00', '4 th, 5 bln', 0, 0, 87),
(103, '', NULL, 0, '643e858fbae84899968bbff187aea7', 'SYIFA ALIATU ZULFA', NULL, '3329016805180001', 1, 0, '02', '01', '', '', '', 0, '2018-05-28', 'P', NULL, 41165, 29, 41186, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '4.00', '52.00', '4 th, 4 bln', 0, 0, 87),
(104, '', NULL, 0, '6f0d56e550454d0f9edfe5679a333a', 'M ALGIFARI', NULL, '3329010808180001', 1, 0, '02', '01', '', '', '', 0, '2018-08-08', 'L', NULL, 41165, 29, 41186, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '4.00', '49.00', '4 th, 1 bln', 0, 0, 87),
(105, '', NULL, 0, '72a6c0e80f2b4a65bb4458934f211d', 'DYLAN OKTAVIANDRA', NULL, '3329011210180002', 1, 0, '01', '01', '', '', '', 0, '2018-10-12', 'L', NULL, 41165, 29, 41186, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '3.00', '47.00', '3 th, 11 bln', 0, 0, 87),
(106, '', NULL, 0, '3bec4247c4bc4719915edaaa305af3', 'ALDI VEEREL ANDANA', NULL, '3329010411180001', 1, 0, '02', '01', '', '', '', 0, '2018-11-04', 'L', NULL, 41165, 29, 41186, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '3.00', '46.00', '3 th, 10 bln', 0, 0, 87),
(107, '', NULL, 0, 'd07c185f0a7b4b10bffaa76613df95', 'RAFANDRA ADI N', NULL, '3329011712180001', 1, 0, '02', '01', '', '', '', 0, '2018-12-17', 'L', NULL, 41165, 29, 41186, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '3.00', '45.00', '3 th, 9 bln', 0, 0, 87),
(108, '', NULL, 0, 'dd81395880c94b3ea03a41ff92a9b2', 'EGA REVIANA', NULL, '3329016301190001', 1, 0, '01', '01', '', '', '', 0, '2019-01-23', 'P', NULL, 41165, 29, 41186, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '3.00', '44.00', '3 th, 8 bln', 0, 0, 87),
(109, '', NULL, 0, '984707cd135940799ab07d2ee245b5', 'NAZIHA ALFATUNISA', NULL, '3329017001190001', 1, 0, '02', '01', '', '', '', 0, '2019-01-30', 'P', NULL, 41165, 29, 41186, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '3.00', '44.00', '3 th, 8 bln', 0, 0, 87),
(110, '', NULL, 0, '459899b3f7de4d60ab5420bb6f4efc', 'ARSYILA QIANA NADIRA', NULL, '3329015610170003', 1, 0, '02', '01', '', '', '', 0, '2017-10-16', 'P', NULL, 41165, 29, 41186, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '4.00', '59.00', '4 th, 11 bln', 0, 0, 87),
(111, '', NULL, 0, 'e6239cc33cf6427a813553b90bfba3', 'ALIFIA MECCA', NULL, '3329016203190001', 1, 0, '02', '01', '', '', '', 0, '2019-03-27', 'P', NULL, 41165, 29, 41186, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '3.00', '42.00', '3 th, 6 bln', 0, 0, 87),
(112, '', NULL, 0, '03ca61fce8c1405aa21b9261eb7ce9', 'NAYA AINI RAHMAH', NULL, '3329014405190001', 1, 0, '01', '01', '', '', '', 0, '2019-05-04', 'P', NULL, 41165, 29, 41186, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '3.00', '40.00', '3 th, 4 bln', 0, 0, 87),
(113, '', NULL, 0, 'a8183a6667564331aa448a1db394ca', 'M ARIF RAMADHAN', NULL, '3329011105190004', 1, 0, '01', '01', '', '', '', 0, '2019-05-11', 'L', NULL, 41165, 29, 41186, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '3.00', '40.00', '3 th, 4 bln', 0, 0, 87),
(114, '', NULL, 0, 'ede240610ee8444893ccbc1b5ae12b', 'AKMAR NADHIF R', NULL, '3329011205190006', 1, 0, '01', '01', '', '', '', 0, '2019-05-21', 'L', NULL, 41165, 29, 41186, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '3.00', '40.00', '3 th, 4 bln', 0, 0, 87),
(115, '', NULL, 0, 'c993ceed7803475599c7a7420840f8', 'TALITA HASNA H', NULL, '3329016606190002', 1, 0, '02', '01', '', '', '', 0, '2019-06-26', 'P', NULL, 41165, 29, 41186, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '3.00', '39.00', '3 th, 3 bln', 0, 0, 87),
(116, '', NULL, 0, '68ac16735372466a8648555017b26d', 'FIDA NAFISATUL A', NULL, '3329015206190001', 1, 0, '01', '01', '', '', '', 0, '2019-08-12', 'P', NULL, 41165, 29, 41186, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '3.00', '37.00', '3 th, 1 bln', 0, 0, 87),
(117, '', NULL, 0, 'ae5f2704ec8b4710aed173ed9c25ed', 'FIQIH ZULFIKAR', NULL, '3329012009190001', 1, 0, '01', '01', '', '', '', 0, '2019-09-20', 'L', NULL, 41165, 29, 41186, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '3.00', '36.00', '3 th, 0 bln', 0, 0, 87),
(118, '', NULL, 0, 'd05b203e2cc34246b5f74b007048fd', 'DAFFY ELVANO', NULL, '1235869/85', 1, 0, '01', '01', '', '', '', 0, '2019-10-31', 'L', NULL, 41165, 29, 41186, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '2.00', '35.00', '2 th, 11 bln', 0, 0, 87),
(119, '', NULL, 0, '56bd88cc0b374bcd96485b2b0d4b24', 'NOVA RIZKI NUGRAHA', NULL, '3329010411190001', 1, 0, '01', '01', '', '', '', 0, '2019-11-04', 'L', NULL, 41165, 29, 41186, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '2.00', '34.00', '2 th, 10 bln', 0, 0, 87),
(120, '', NULL, 0, '838ad8a1dbf34d68974146bcc27d99', 'REINALDI HAFIZ JANUAR', NULL, '3329012206200002', 1, 0, '01', '01', '', '', '', 0, '2020-06-22', 'L', NULL, 41165, 29, 41186, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '2.00', '27.00', '2 th, 3 bln', 0, 0, 87),
(121, '', NULL, 0, 'ab72937231fb414bb7288a41c432c5', 'WINDAH ARINNI', NULL, '3329016208200001', 1, 0, '02', '01', '', '', '', 0, '2020-08-22', 'P', NULL, 41165, 29, 41186, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '2.00', '25.00', '2 th, 1 bln', 0, 0, 87),
(122, '', NULL, 0, 'c6b5f36d1d154e20b5e25ddaf683c9', 'EXCEL HELDIANSYAH', NULL, '3329011206170002', 1, 0, '01', '01', '', '', '', 0, '2017-08-12', 'L', NULL, 41165, 29, 41186, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '5.00', '61.00', '5 th, 1 bln', 0, 0, 87),
(123, '', NULL, 0, '340245e2747e478cb7b8d492eac0c8', 'CARSIDAH', NULL, '3329014506950002', 1, 0, '01', '01', '', '', '', 0, '1995-06-05', 'P', NULL, 41165, 29, 41186, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '27.00', '328.00', '27 th, 4 bln', 1, 0, 87),
(124, '', NULL, 0, '01b95d653e8547e28895f978e48575', 'RUKIAH', NULL, '3329015611920001', 1, 0, '02', '01', '', '', '', 0, '1992-11-16', 'P', NULL, 41165, 29, 41186, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '29.00', '358.00', '29 th, 10 bln', 1, 0, 87),
(125, '', NULL, 0, '28f30daba9b34cada68e95e64026d8', 'DISMAN', NULL, '3329010402120000', 1, 0, NULL, NULL, '', '', '', 0, '2012-02-04', 'L', NULL, 41165, 29, 41170, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '10.00', '128.00', '10 th, 8 bln', 0, 0, 9),
(126, '', NULL, 0, 'edfcc188b787435687051056533288', 'M ZIDAN ALFHAYED', NULL, '3329010609110000', 1, 0, NULL, NULL, '', '', '', 0, '2011-09-06', 'L', NULL, 41165, 29, 41170, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '11.00', '133.00', '11 th, 1 bln', 0, 0, 9),
(127, '', NULL, 0, '087dfcf2444a492e9d90f32d6d0c31', 'NESYA APRIANA NINGSIH', NULL, '3329014304120000', 1, 0, NULL, NULL, '', '', '', 0, '2012-04-03', 'P', NULL, 41165, 29, 41170, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '10.00', '126.00', '10 th, 6 bln', 0, 0, 9),
(128, '', NULL, 0, 'c15a1d628e204f8295e9b62574679f', 'NURKHOLIFAH', NULL, '3329014409120000', 1, 0, NULL, NULL, '', '', '', 0, '2012-09-04', 'P', NULL, 41165, 29, 41170, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '10.00', '121.00', '10 th, 1 bln', 0, 0, 9),
(129, '', NULL, 0, '364b2162f1154070b016a97de5f466', 'PADLANI ROBI', NULL, '3329010402120001', 1, 0, NULL, NULL, '', '', '', 0, '2012-02-04', 'L', NULL, 41165, 29, 41170, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '10.00', '128.00', '10 th, 8 bln', 0, 0, 9),
(130, '', NULL, 0, 'c17dcd9d601248499ef4d397dc979b', 'REVAN APRIZQI P', NULL, '3329011502120000', 1, 0, NULL, NULL, '', '', '', 0, '2012-02-15', 'L', NULL, 41165, 29, 41170, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '10.00', '128.00', '10 th, 8 bln', 0, 0, 9),
(131, '', NULL, 0, '5a622ac1e05341fbbba6d29679b017', 'REHAN ALVIANSYAH', NULL, '3329011508110000', 1, 0, NULL, NULL, '', '', '', 0, '2011-08-15', 'L', NULL, 41165, 29, 41170, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '11.00', '134.00', '11 th, 2 bln', 0, 0, 9),
(132, '', NULL, 0, 'dc3282a6b37a492585d6339923fe9b', 'REZA EGI SAPUTRA', NULL, '3329012509110000', 1, 0, NULL, NULL, '', '', '', 0, '2011-09-25', 'L', NULL, 41165, 29, 41170, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '11.00', '132.00', '11 th, 0 bln', 0, 0, 9),
(133, '', NULL, 0, 'ddb0ffc56f4d4ee998bd052a635331', 'RIDHO AL GIPARI', NULL, '3329011209120000', 1, 0, NULL, NULL, '', '', '', 0, '2012-09-12', 'L', NULL, 41165, 29, 41170, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '10.00', '121.00', '10 th, 1 bln', 0, 0, 9),
(134, '', NULL, 0, '826763ccac554a60acc939f4efd4ef', 'SIKMATUL MAULA', NULL, '3329014202110000', 1, 0, NULL, NULL, '', '', '', 0, '2011-02-02', 'P', NULL, 41165, 29, 41170, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '11.00', '140.00', '11 th, 8 bln', 0, 0, 9),
(135, '', NULL, 0, '43c8f05388f8481187e681bf87e7ac', 'SINTA ELISA PUTRI', NULL, '3329015606110000', 1, 0, NULL, NULL, '', '', '', 0, '2011-06-16', 'P', NULL, 41165, 29, 41170, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '11.00', '136.00', '11 th, 4 bln', 0, 0, 9),
(136, '', NULL, 0, 'fe359b022e0d43c0aa890d61f604f8', 'KEZRA NURASFA', NULL, '3329014912210001', 1, 0, '02', '01', '', '', '', 0, '2021-12-09', 'P', NULL, 41165, 29, 41173, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '0.00', '10.00', '0 th, 10 bln', 0, 0, 11),
(137, '', NULL, 0, 'b6704db3f2a94461a6c2e29ed5238c', 'Raffasya elvano', NULL, '3329011310210002', 1, 0, '01', '02', '', '', '', 0, '2021-10-13', 'L', NULL, 41165, 29, 41175, 0, 0, NULL, NULL, 'Lajang', 'Brebes', '0.00', '11.00', '0 th, 11 bln', 0, 0, 14),
(138, '', NULL, 0, '0a4ce8008d3d4d179fde7a448aac12', 'ALIFA FITRIANI', NULL, '3329016312210003', 1, 0, '05', '01', '', '', '', 0, '2021-12-24', 'P', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '0.00', '9.00', '0 th, 9 bln', 0, 0, 81),
(143, '', NULL, 0, 'cf34f37211ff4926a7ebde4339d191', 'GILBY S AL BIRRU', NULL, '1351336', 1, 0, NULL, NULL, '', '', '', 0, '2017-10-08', 'L', NULL, 41165, 29, 41176, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '5.00', '60.00', '5 th, 0 bln', 0, 0, 13),
(142, '', NULL, 0, '146c634923e4421aac568ab23e050a', 'YUNI AYU LESTARI', NULL, '3329016909176321', 1, 0, NULL, NULL, '', '', '', 0, '2017-09-29', 'P', NULL, 41165, 29, 41176, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '5.00', '60.00', '5 th, 0 bln', 0, 0, 13),
(141, '', NULL, 0, 'f7d37813d22f48d2b9d62c948e11e4', 'arya satria agung', NULL, '3329011309170001', 1, 0, NULL, NULL, '', '', '', 0, '2017-09-13', 'L', NULL, 41165, 29, 41176, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '5.00', '60.00', '5 th, 0 bln', 0, 0, 13),
(144, '', NULL, 0, '373b1977d668477c9a06ccd0684892', 'KEYRA ATMA F', NULL, '32365869', 1, 0, NULL, NULL, '', '', '', 0, '2017-11-19', 'P', NULL, 41165, 29, 41176, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '4.00', '58.00', '4 th, 10 bln', 0, 0, 13),
(145, '', NULL, 0, 'b63f038416194d338a9afcf56ee85a', 'ARDI MAULANA Y', NULL, '1354698', 1, 0, NULL, NULL, '', '', '', 0, '2017-11-20', 'L', NULL, 41165, 29, 41176, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '4.00', '58.00', '4 th, 10 bln', 0, 0, 13),
(146, '', NULL, 0, '845e56e5aaa649e4a0722ee6313945', 'ABRIJAM AL FARIQ', NULL, '1234563526', 1, 0, NULL, NULL, '', '', '', 0, '2017-12-08', 'L', NULL, 41165, 29, 41176, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '4.00', '58.00', '4 th, 10 bln', 0, 0, 13),
(147, '', NULL, 0, 'e558511c8296401684922d90bbf9c3', 'KANIA ALZENA', NULL, '25463896', 1, 0, NULL, NULL, '', '', '', 0, '2018-01-01', 'P', NULL, 41165, 29, 41176, 0, 0, NULL, NULL, 'Lajang', '01-01-2018', '4.00', '57.00', '4 th, 9 bln', 0, 0, 13),
(148, '', NULL, 0, 'c10024baa8564092b694e1e4fcbf5a', 'NAFISA TALITA E', NULL, '132565896', 1, 0, NULL, NULL, '', '', '', 0, '2018-01-13', 'P', NULL, 41165, 29, 41176, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '4.00', '56.00', '4 th, 8 bln', 0, 0, 13),
(149, '', NULL, 0, 'dc8ac06471c64a818a03cb1e7c8f56', 'NIZAM ALAMSYAH', NULL, '1325689+885', 1, 0, NULL, NULL, '', '', '', 0, '2018-01-24', 'L', NULL, 41165, 29, 41176, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '4.00', '56.00', '4 th, 8 bln', 0, 0, 13),
(150, '', NULL, 0, '6fc37ecdc89547079c8c6e850ca392', 'JELITA GAYATRI', NULL, '34689+65', 1, 0, NULL, NULL, '', '', '', 0, '2018-01-25', 'P', NULL, 41165, 29, 41176, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '4.00', '56.00', '4 th, 8 bln', 0, 0, 13),
(151, '', NULL, 0, '2c1f358e428244979356e9508a7834', 'AHMAD AL A', NULL, '3254689', 1, 0, NULL, NULL, '', '', '', 0, '2018-02-05', 'L', NULL, 41165, 29, 41176, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '4.00', '56.00', '4 th, 8 bln', 0, 0, 13),
(152, '', NULL, 0, '61f869efe70c477797cee57cea2fcd', 'FELISA K PUTRI', NULL, '33569', 1, 0, NULL, NULL, '', '', '', 0, '2020-11-01', 'P', NULL, 41165, 29, 41176, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '1.00', '23.00', '1 th, 11 bln', 0, 0, 13),
(153, '', NULL, 0, 'df5e02f8baac486384383579b18a49', 'ABIYAN ATHAR', NULL, '32569896', 1, 0, NULL, NULL, '', '', '', 0, '2021-08-10', 'L', NULL, 41165, 29, 41176, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '1.00', '13.00', '1 th, 1 bln', 0, 0, 13),
(154, '', NULL, 0, '9048995ad59e44a8b4db4351bfde19', 'LAILA SILMI K', NULL, '6589+*8+63', 1, 0, NULL, NULL, '', '', '', 0, '2021-02-14', 'P', NULL, 41165, 29, 41176, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '1.00', '19.00', '1 th, 7 bln', 0, 0, 13),
(155, '', NULL, 0, '4adbde0c237442a3b0718e864eb88f', 'KING FAAZ AL KHATIM', NULL, '3329010212210001', 1, 0, '04', '01', '', '', '', 0, '2021-12-02', 'L', NULL, 41165, 29, 41173, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '0.00', '10.00', '0 th, 10 bln', 0, 0, 11),
(156, '', NULL, 0, '7c2136fd9ef4475d987c5fd3983869', 'ALZAD MUKAFFI', NULL, '3329012011210001', 1, 0, '01', '01', '', '', '', 0, '2021-11-20', 'L', NULL, 41165, 29, 41173, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '0.00', '10.00', '0 th, 10 bln', 0, 0, 11),
(157, '', NULL, 0, '291895dc2aa24b33b079c115a21c55', 'MUHAMMAD RAFKA BAIHAQI', NULL, '3329011811210001', 1, 0, '05', '01', '', '', '', 0, '2021-11-18', 'L', NULL, 41165, 29, 41173, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '0.00', '10.00', '0 th, 10 bln', 0, 0, 11),
(158, '', NULL, 0, '471a6d0d6d894ab786dfcc0b182f6a', 'INEZ MAUDY ZUNAIRA', NULL, '3329015511210001', 1, 0, '04', '01', '', '', '', 0, '2021-11-15', 'P', NULL, 41165, 29, 41173, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '0.00', '10.00', '0 th, 10 bln', 0, 0, 11),
(159, '', NULL, 0, 'e9fd940bd4a647ba9062ee191587e4', 'SAHNUM ADINDA YUMNA', NULL, '3329014610210001', 1, 0, '01', '01', '', '', '', 0, '2021-10-06', 'P', NULL, 41165, 29, 41173, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '1.00', '12.00', '1 th, 0 bln', 0, 0, 11),
(160, '', NULL, 0, '8a53a2d1d209413da11182b5b6e39a', 'M REYNAND HAVAAR S', NULL, '3329012109210003', 1, 0, '05', '01', '', '', '', 0, '2021-09-12', 'L', NULL, 41165, 29, 41173, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '1.00', '12.00', '1 th, 0 bln', 0, 0, 11),
(161, '', NULL, 0, '797cbf6634a1441e9a9bffc6a4a523', 'M HAIDAR ROHMAT', NULL, '3329010109210003', 1, 0, '04', '01', '', '', '', 0, '2021-09-01', 'L', NULL, 41165, 29, 41173, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '1.00', '13.00', '1 th, 1 bln', 0, 0, 11),
(162, '', NULL, 0, '8e6d824b84094ca18e1070649b6e42', 'MUHAMMAD ROYYAN AZIS', NULL, '3329010308210001', 1, 0, '01', '01', '', '', '', 0, '2021-08-03', 'L', NULL, 41165, 29, 41173, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '1.00', '14.00', '1 th, 2 bln', 0, 0, 11),
(163, '', NULL, 0, '550b2a8d4f4747e1af8c7cea7c5bef', 'NAMMIRA QIMATUL ULYA', NULL, '3329015007210002', 1, 0, '01', '01', '', '', '', 0, '2021-07-10', 'P', NULL, 41165, 29, 41173, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '1.00', '15.00', '1 th, 3 bln', 0, 0, 11),
(164, '', NULL, 0, '4adc822398fb4ec78848b3333b0002', 'MUHAMMAD RAFASYA GUMINDRA', NULL, '3329012806210001', 1, 0, '01', '01', '', '', '', 0, '2021-06-28', 'L', NULL, 41165, 29, 41173, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '1.00', '15.00', '1 th, 3 bln', 0, 0, 11),
(165, '', NULL, 0, '463966f4683a4d31bd3dc13cff6857', 'TRIE AYU ANJANI', NULL, '3329016006210002', 1, 0, '05', '01', '', '', '', 0, '2021-06-28', 'P', NULL, 41165, 29, 41173, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '1.00', '15.00', '1 th, 3 bln', 0, 0, 11),
(166, '', NULL, 0, '4230961f425743b0b8df4f232e8b97', 'MUHAMMAD ALFATH ROSADI', NULL, '3329011905210001', 1, 0, '04', '01', '', '', '', 0, '2021-05-19', 'L', NULL, 41165, 29, 41173, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '1.00', '16.00', '1 th, 4 bln', 0, 0, 11),
(167, '', NULL, 0, '64f42650b06b44b5bb9e7efa5c4e3d', 'DAFFA RAMDAN ALTHAF', NULL, '3329012604210002', 1, 0, '05', '01', '', '', '', 0, '2021-04-26', 'L', NULL, 41165, 29, 41173, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '1.00', '17.00', '1 th, 5 bln', 0, 0, 11),
(168, '', NULL, 0, 'd155e5a5122a4545981282c1731ced', 'SAHRAN DANADYAKSA Y', NULL, '3329012304210002', 1, 0, '05', '01', '', '', '', 0, '2021-04-23', 'L', NULL, 41165, 29, 41173, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '1.00', '17.00', '1 th, 5 bln', 0, 0, 11),
(169, '', NULL, 0, '176c29ee94b04238ace42631bdbcf7', 'GHAITSA SYAKILA N', NULL, '3329014504210002', 1, 0, '05', '01', '', '', '', 0, '2021-04-05', 'P', NULL, 41165, 29, 41173, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '1.00', '18.00', '1 th, 6 bln', 0, 0, 11),
(170, '', NULL, 0, '920346b902e24341bee69291135255', 'MAWAR', NULL, '3329016203210001', 1, 0, '05', '01', '', '', '', 0, '2021-03-22', 'P', NULL, 41165, 29, 41173, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '1.00', '18.00', '1 th, 6 bln', 0, 0, 11),
(171, '', NULL, 0, '5b147c0f650441eeb09c4bca30b92e', 'ASRAF ASEEGAF', NULL, '3329010602210003', 1, 0, '04', '01', '', '', '', 0, '2021-02-06', 'L', NULL, 41165, 29, 41173, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '1.00', '20.00', '1 th, 8 bln', 0, 0, 11),
(172, '', NULL, 0, 'd065f2a161a7429db7b1e48e2e7091', 'MUTAWALLI EL SARAWI', NULL, '3329010202210001', 1, 0, '01', '01', '', '', '', 0, '2021-02-02', 'L', NULL, 41165, 29, 41173, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '1.00', '20.00', '1 th, 8 bln', 0, 0, 11),
(173, '', NULL, 0, '5a75597325c14db7ac4cb316c5adf4', 'CHAYRA AININ QULAIBAH', NULL, '3329014102210001', 1, 0, '02', '01', '', '', '', 0, '2021-02-01', 'P', NULL, 41165, 29, 41173, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '1.00', '20.00', '1 th, 8 bln', 0, 0, 11),
(174, '', NULL, 0, 'fddc1129a00d461fb71d585b8a0671', 'MUHAMMAD ADZRA HAIDAR', NULL, '3329012801210001', 1, 0, '02', '01', '', '', '', 0, '2021-01-28', 'L', NULL, 41165, 29, 41173, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '1.00', '20.00', '1 th, 8 bln', 0, 0, 11),
(175, '', NULL, 0, '0da7c9c4e7d041a98c8a7eb9cbf38e', 'NAURA HASNA', NULL, '3329015501210003', 1, 0, '01', '01', '', '', '', 0, '2021-01-15', 'P', NULL, 41165, 29, 41173, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '1.00', '20.00', '1 th, 8 bln', 0, 0, 11),
(176, '', NULL, 0, '425b5e8b77ea489db4457ea03caebf', 'ADIBA HALWA', NULL, '3329014101210004', 1, 0, '02', '01', '', '', '', 0, '2021-01-01', 'P', NULL, 41165, 29, 41173, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '1.00', '21.00', '1 th, 9 bln', 0, 0, 11),
(177, '', NULL, 0, 'c683ff95b3854786ab008b9f2f23e1', 'GHAITSA AHLAAM MUNAAYA', NULL, '3329014101210003', 1, 0, '01', '01', '', '', '', 0, '2021-01-01', 'P', NULL, 41165, 29, 41173, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '1.00', '21.00', '1 th, 9 bln', 0, 0, 11),
(178, '', NULL, 0, '7928e2c8e26547f78e57701fa3d57a', 'ATIKA ZAHRA RATIFA', NULL, '3329016012200001', 1, 0, '04', '01', '', '', '', 0, '2020-12-20', 'P', NULL, 41165, 29, 41173, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '1.00', '21.00', '1 th, 9 bln', 0, 0, 11),
(179, '', NULL, 0, '8816fa08f0f34f609d133b3893cf9e', 'SULTAN ABQAR M', NULL, '3329011212200001', 1, 0, '02', '01', '', '', '', 0, '2020-12-12', 'L', NULL, 41165, 29, 41173, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '1.00', '21.00', '1 th, 9 bln', 0, 0, 11),
(180, '', NULL, 0, '53455d077338442c8208508cd88ee3', 'M. RAFASYA ADITYA', NULL, '3329011511200002', 1, 0, '05', '01', '', '', '', 0, '2020-11-15', 'L', NULL, 41165, 29, 41173, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '1.00', '22.00', '1 th, 10 bln', 0, 0, 11),
(181, '', NULL, 0, '9cf617f844da409081d1dea75402ef', 'M. SHIDQI ARSALAN', NULL, '3329012709200002', 1, 0, '05', '01', '', '', '', 0, '2020-09-27', 'L', NULL, 41165, 29, 41173, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '2.00', '24.00', '2 th, 0 bln', 0, 0, 11),
(182, '', NULL, 0, '74b05d58a4ec45c19d48b3baa77daa', 'AZQIYA IZZATUN NISA', NULL, '3329016509200001', 1, 0, '04', '01', '', '', '', 0, '2020-09-25', 'P', NULL, 41165, 29, 41173, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '2.00', '24.00', '2 th, 0 bln', 0, 0, 11),
(183, '', NULL, 0, '76f9f141cfa84336ade05e8dd0ccd0', 'M. GAVIN SYAHPUTRA', NULL, '3329011809200002', 1, 0, '04', '01', '', '', '', 0, '2020-09-19', 'L', NULL, 41165, 29, 41173, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '2.00', '24.00', '2 th, 0 bln', 0, 0, 11),
(184, '', NULL, 0, 'cabe2a6fe2484eb291586ea6f33958', 'MUHAMMAD MIRZA H', NULL, '3329011607200001', 1, 0, '03', '01', '', '', '', 0, '2020-07-16', 'L', NULL, 41165, 29, 41173, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '2.00', '26.00', '2 th, 2 bln', 0, 0, 11),
(185, '', NULL, 0, '9e1680176e5e49efb4ef39d6e2263d', 'AUDREY QUR\'ANI', NULL, '3329016806200001', 1, 0, '03', '01', '', '', '', 0, '2020-06-28', 'P', NULL, 41165, 29, 41173, 0, 0, NULL, NULL, 'Lajang', '3329016806200001', '2.00', '27.00', '2 th, 3 bln', 0, 0, 11),
(186, '', NULL, 0, '5a77d5beb3e7466a81c239499a6a97', 'ARUMI ABELIA', NULL, '3329014406200004', 1, 0, '01', '01', '', '', '', 0, '2020-06-03', 'P', NULL, 41165, 29, 41173, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '2.00', '28.00', '2 th, 4 bln', 0, 0, 11),
(187, '', NULL, 0, '8bcd62eae58246a19488e3c849c508', 'YANFA LINNAS', NULL, '3329012805200002', 1, 0, '02', '01', '', '', '', 0, '2020-05-28', 'L', NULL, 41165, 29, 41173, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '2.00', '28.00', '2 th, 4 bln', 0, 0, 11),
(188, '', NULL, 0, '44d9a71adf0b4ad099b1720821ce4d', 'M. AIZAN IRSYADUL', NULL, '3329012005200002', 1, 0, '01', '01', '', '', '', 0, '2020-05-20', 'L', NULL, 41165, 29, 41173, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '2.00', '28.00', '2 th, 4 bln', 0, 0, 11),
(189, '', NULL, 0, '841ac797ef7646609aef53e0584ea5', 'AMMARA AZALEA Z', NULL, '3329015205200001', 1, 0, '01', '01', '', '', '', 0, '2020-05-12', 'P', NULL, 41165, 29, 41173, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '2.00', '28.00', '2 th, 4 bln', 0, 0, 11),
(190, '', NULL, 0, '9471c3cf83dd40139d7367e1450e6e', 'NAFILAH YAUMI N Q', NULL, '3329015005200002', 1, 0, '01', '01', '', '', '', 0, '2020-05-10', 'P', NULL, 41165, 29, 41173, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '2.00', '29.00', '2 th, 5 bln', 0, 0, 11),
(191, '', NULL, 0, '49a02986fc3a450495b7269fe27304', 'RADINKA KEVAN', NULL, '3329011404200002', 1, 0, '03', '01', '', '', '', 0, '2020-04-14', 'L', NULL, 41165, 29, 41173, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '2.00', '29.00', '2 th, 5 bln', 0, 0, 11),
(192, '', NULL, 0, '1a3ce7c450c74814b341b2796ab149', 'DEVON MALIK A', NULL, '3329012003200001', 1, 0, '04', '01', '', '', '', 0, '2020-03-20', 'L', NULL, 41165, 29, 41173, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '2.00', '30.00', '2 th, 6 bln', 0, 0, 11),
(193, '', NULL, 0, '5536c24ea7714b30b16a6c0b419fa4', 'RAFFA HANIEF S', NULL, '3329012702200003', 1, 0, '01', '01', '', '', '', 0, '2020-02-27', 'L', NULL, 41165, 29, 41173, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '2.00', '31.00', '2 th, 7 bln', 0, 0, 11),
(194, '', NULL, 0, '9557876c96a94d5cb038f00e63e997', 'M. NADHIF MUJTABA', NULL, '3329012402200003', 1, 0, '01', '01', '', '', '', 0, '2020-02-24', 'L', NULL, 41165, 29, 41173, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '2.00', '31.00', '2 th, 7 bln', 0, 0, 11),
(195, '', NULL, 0, 'cd98f8b3791441358558932c312116', 'NADIA IZDIHAR F', NULL, '3329015302200002', 1, 0, '01', '01', '', '', '', 0, '2020-02-13', 'P', NULL, 41165, 29, 41173, 0, 0, NULL, NULL, 'Lajang', '3329015302200002', '2.00', '31.00', '2 th, 7 bln', 0, 0, 11),
(196, '', NULL, 0, '4fe462fc3c7d4092881dc0479f0ce5', 'QUEENA GHASNATUL', NULL, '3329015501200002', 1, 0, '04', '01', '', '', '', 0, '2020-01-15', 'P', NULL, 41165, 29, 41173, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '2.00', '32.00', '2 th, 8 bln', 0, 0, 11),
(197, '', NULL, 0, 'd274519cf01d45d082dc1cc38b90af', 'LILIQ LIQOI ROBBI', NULL, '3329015301200001', 1, 0, '04', '01', '', '', '', 0, '2020-01-13', 'P', NULL, 41165, 29, 41173, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '2.00', '32.00', '2 th, 8 bln', 0, 0, 11),
(198, '', NULL, 0, 'f920958f07b44c919cdd8f84ade117', 'IZZA ANNISA', NULL, '3329016210190003', 1, 0, '01', '01', '', '', '', 0, '2019-10-22', 'P', NULL, 41165, 29, 41173, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '2.00', '35.00', '2 th, 11 bln', 0, 0, 11),
(199, '', NULL, 0, '397b9ed578ad4ab888a6594f028541', 'DINAN KOSWARA', NULL, '3329011110190001', 1, 0, '01', '01', '', '', '', 0, '2019-10-11', 'L', NULL, 41165, 29, 41173, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '2.00', '36.00', '2 th, 12 bln', 0, 0, 11),
(200, '', NULL, 0, 'd53650fb66b6451786daa6ef17ec15', 'ABDUL MALIK KARIM A', NULL, '3329010810190002', 1, 0, '03', '01', '', '', '', 0, '2019-10-08', 'L', NULL, 41165, 29, 41173, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '3.00', '36.00', '3 th, 0 bln', 0, 0, 11);
INSERT INTO `user_user` (`id`, `password`, `last_login`, `is_superuser`, `username`, `name`, `mother_name`, `nik`, `is_active`, `is_staff`, `rt`, `rw`, `mobile_number`, `picture`, `ktp`, `from_web`, `dob`, `gender`, `education`, `kabupaten_id`, `kecamatan_id`, `kelurahan_id`, `order`, `print_counter`, `provinsi_id`, `job`, `marital_status`, `place_of_birth`, `age_year`, `age_month`, `age_string`, `is_pregnant`, `is_breeding`, `parent_id`) VALUES
(201, '', NULL, 0, '08d2093863b04869933bb6949e7f5f', 'AHKAM HAFIZ A', NULL, '3329012109190001', 1, 0, '01', '01', '', '', '', 0, '2019-09-21', 'L', NULL, 41165, 29, 41173, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '3.00', '36.00', '3 th, 0 bln', 0, 0, 11),
(202, '', NULL, 0, '63fe95b75a6141ca8d80348fbf9ef0', 'KAMILA N', NULL, '3329015409190001', 1, 0, '03', '01', '', '', '', 0, '2019-09-14', 'P', NULL, 41165, 29, 41173, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '3.00', '36.00', '3 th, 0 bln', 0, 0, 11),
(203, '', NULL, 0, '19f74b9411f5409a97124b00260632', 'M. AHSANUMAN K', NULL, '3329012208190001', 1, 0, '04', '01', '', '', '', 0, '2019-08-22', 'L', NULL, 41165, 29, 41173, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '3.00', '37.00', '3 th, 1 bln', 0, 0, 11),
(204, '', NULL, 0, 'd6da57eb799f4bf48e70253415e3a0', 'ANITA FIRDAUS', NULL, '3329015207190001', 1, 0, '02', '01', '', '', '', 0, '2019-07-12', 'P', NULL, 41165, 29, 41173, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '3.00', '38.00', '3 th, 2 bln', 0, 0, 11),
(205, '', NULL, 0, '185035ccd10043838d796df3697abb', 'RENDI RAMDANI', NULL, '3329011308170001', 1, 0, '02', '01', '', '', '', 0, '2017-08-13', 'L', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '5.00', '61.00', '5 th, 1 bln', 0, 0, 81),
(206, '', NULL, 0, '4c530f06ffe54142aff116ad5a30e5', 'KHOERIL RAFKI', NULL, '000000', 1, 0, '04', '01', '', '', '', 0, '2021-11-23', 'L', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '0.00', '10.00', '0 th, 10 bln', 0, 0, 81),
(207, '', NULL, 0, '14d6c720bd4444b98b6e1fbd123ee5', 'MUHAMAD RAIKAL KUNTARA', NULL, '3329010909190001', 1, 0, '05', '01', '', '', '', 0, '2019-09-09', 'L', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '3.00', '37.00', '3 th, 1 bln', 0, 0, 81),
(208, '', NULL, 0, 'dcb2f2adad8f4225b02e8759d1a55c', 'SELGI PRAMUDITA', NULL, '0000', 1, 0, '02', '02', '', '', '', 0, '2020-10-16', 'L', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '1.00', '23.00', '1 th, 11 bln', 0, 0, 81),
(209, '', NULL, 0, '76ba1cbda4a3433aab52b31a61638d', 'M. AZRIEL BAHARUN', NULL, '3329011106190002', 1, 0, '01', '01', '', '', '', 0, '2019-06-11', 'L', NULL, 41165, 29, 41173, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '3.00', '40.00', '3 th, 4 bln', 0, 0, 11),
(210, '', NULL, 0, '71363c46342c4917a07d311a74cebf', 'MARLINDA YASMIN MAHALANI', NULL, '00000000', 1, 0, '02', '02', '', '', '', 0, '2021-01-25', 'P', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '1.00', '20.00', '1 th, 8 bln', 0, 0, 81),
(211, '', NULL, 0, 'df3fc2f3145e4ce0ae243c967dbd8d', 'NELIA DWI FELISA', NULL, '332901111210001', 1, 0, '05', '02', '', '', '', 0, '2021-11-01', 'P', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '0.00', '11.00', '0 th, 11 bln', 0, 0, 81),
(212, '', NULL, 0, 'a157fbf7570f4f8baa1395448cfc6a', 'ISMAIL HADI R', NULL, '3329012605190002', 1, 0, '04', '01', '', '', '', 0, '2019-05-26', 'L', NULL, 41165, 29, 41173, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '3.00', '40.00', '3 th, 4 bln', 0, 0, 11),
(213, '', NULL, 0, '40814a50336b412c80c06a4a412ece', 'TRESNA ARISTA', NULL, '3329015510210002', 1, 0, '03', '02', '', '', '', 0, '2021-10-15', 'P', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '0.00', '11.00', '0 th, 11 bln', 0, 0, 81),
(214, '', NULL, 0, 'b8c59785515948f7a66787ba451865', 'FIYONA SAPUTRI', NULL, '3329015310210003', 1, 0, '04', '02', '', '', '', 0, '2021-10-13', 'P', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '0.00', '11.00', '0 th, 11 bln', 0, 0, 81),
(215, '', NULL, 0, 'c84357773a054820bab2fb96415bc1', 'CHERYL ARSY TANIA', NULL, '3329016709210001', 1, 0, '01', '02', '', '', '', 0, '2021-09-27', 'P', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '1.00', '12.00', '1 th, 0 bln', 0, 0, 81),
(216, '', NULL, 0, 'b53c1a4827534147a553bc1c0834f3', 'M. AIJUL RAMDHANI', NULL, '3329012305190002', 1, 0, '04', '01', '', '', '', 0, '2019-05-23', 'L', NULL, 41165, 29, 41173, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '3.00', '40.00', '3 th, 4 bln', 0, 0, 11),
(217, '', NULL, 0, 'c1cdcea1b6214990abb42084cece57', 'MUHAMAD IZZAN AL ATTAR', NULL, '3329010908210002', 1, 0, '01', '02', '', '', '', 0, '2021-08-09', 'L', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '1.00', '14.00', '1 th, 2 bln', 0, 0, 81),
(218, '', NULL, 0, '894c5557767a4f078437c2e2eb1bf4', 'YASMIN AUDIA RISA', NULL, '3329014508210002', 1, 0, '05', '01', '', '', '', 0, '2021-08-05', 'P', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '1.00', '14.00', '1 th, 2 bln', 0, 0, 81),
(219, '', NULL, 0, '15a59059b0504c3e901b6815ecfa64', 'FELIA AZZA REYNA', NULL, '3329015307210002', 1, 0, '04', '01', '', '', '', 0, '2021-07-13', 'P', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '1.00', '14.00', '1 th, 2 bln', 0, 0, 81),
(220, '', NULL, 0, '47db28761ebd4763bbaa949571e0e8', 'CHAYRA MUNA R', NULL, '3329015505190001', 1, 0, '04', '01', '', '', '', 0, '2019-05-15', 'P', NULL, 41165, 29, 41173, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '3.00', '40.00', '3 th, 4 bln', 0, 0, 11),
(221, '', NULL, 0, '74a53c706d644b1ea0a0abdedb4498', 'RASYA ABRISAM BAHTIAR', NULL, '3329010807210001', 1, 0, '05', '01', '', '', '', 0, '2021-07-08', 'L', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '1.00', '15.00', '1 th, 3 bln', 0, 0, 81),
(222, '', NULL, 0, '2999b186c2824f8090a49d4d7e4a95', 'FATAN', NULL, '3329012306210001', 1, 0, '03', '02', '', '', '', 0, '2021-06-22', 'L', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '1.00', '15.00', '1 th, 3 bln', 0, 0, 81),
(223, '', NULL, 0, '41329cf8dd894bc0af9c2a34d8a95e', 'LAILATUL AYUDIA INARA', NULL, '3329016905210001', 1, 0, '04', '01', '', '', '', 0, '2021-05-29', 'P', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '1.00', '16.00', '1 th, 4 bln', 0, 0, 81),
(224, '', NULL, 0, 'd914719ff71d459c8a8544993ee382', 'MI\'RAZ ALFIANSYAH', NULL, '3329010204190002', 1, 0, '02', '01', '', '', '', 0, '2020-04-02', 'L', NULL, 41165, 29, 41173, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '2.00', '30.00', '2 th, 6 bln', 0, 0, 11),
(225, '', NULL, 0, '69d5eaa761e449bdafb31cde8efb0a', 'ASROF ZAHIRUL U', NULL, '3329010104190001', 1, 0, '03', '01', '', '', '', 0, '2019-04-01', 'L', NULL, 41165, 29, 41173, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '3.00', '42.00', '3 th, 6 bln', 0, 0, 11),
(226, '', NULL, 0, '426143fbede249a5836b50e3043158', 'ATHAFARIS RADEAL FADHIL', NULL, '000000000', 1, 0, '01', '02', '', '', '', 0, '2022-09-27', 'L', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '0.00', '0.00', '0th, 0bln', 0, 0, 81),
(227, '', NULL, 0, '8dd0bee8882c404aa6d1261e7f09d0', 'JASMINE AYESHA PRAYOGA', NULL, '0000000', 1, 0, '05', '01', '', '', '', 0, '2022-09-10', 'P', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '0.00', '0.00', '0th, 0bln', 0, 0, 81),
(228, '', NULL, 0, 'e93b0b5d6ea246c9a408cee0235489', 'ANFAU LINNASI', NULL, '3329012703190002', 1, 0, '05', '01', '', '', '', 0, '2019-03-27', 'L', NULL, 41165, 29, 41173, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '3.00', '42.00', '3 th, 6 bln', 0, 0, 11),
(229, '', NULL, 0, 'f196ec1de1e54361a6a80a684043e8', 'ALWI PUTRA ABIZAR', NULL, '00000000000', 1, 0, '01', '01', '', '', '', 0, '2022-09-06', 'L', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '0.00', '1.00', '0 th, 1 bln', 0, 0, 81),
(230, '', NULL, 0, '81554e96c62040709e73f42dc4554c', 'SAFFANA NUR FAUZIA', NULL, '000000000000000', 1, 0, '01', '01', '', '', '', 0, '2022-08-30', 'P', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '0.00', '1.00', '0 th, 1 bln', 0, 0, 81),
(231, '', NULL, 0, '813fb34a2f434cb08ecac40a390769', 'MUHAMMAD NAUFAL AFKAR AL HUSAIN', NULL, '3329011608220001', 1, 0, '01', '01', '', '', '', 0, '2022-08-16', 'L', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '0.00', '1.00', '0 th, 1 bln', 0, 0, 81),
(232, '', NULL, 0, 'e997287809bf41be9ef8a6eacf0084', 'GRICHELLE ZIHAN ELNATTA', NULL, '0000000000000000', 1, 0, '05', '01', '', '', '', 0, '2022-08-12', 'P', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '0.00', '1.00', '0 th, 1 bln', 0, 0, 81),
(233, '', NULL, 0, 'a9f29c4930e844a586ffc7d2ddf900', 'SHAKIYA NABHA S', NULL, '3329016302190001', 1, 0, '05', '01', '', '', '', 0, '2019-02-23', 'P', NULL, 41165, 29, 41173, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '3.00', '43.00', '3 th, 7 bln', 0, 0, 11),
(234, '', NULL, 0, '5b0550e1f24143a08d90d9199af74a', 'ANINDITA HAFIZA GHANIA', NULL, '3329014108220001', 1, 0, '04', '02', '', '', '', 0, '2022-08-01', 'P', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '0.00', '2.00', '0 th, 2 bln', 0, 0, 81),
(235, '', NULL, 0, '286787a6b91b44febf91a5008cacb0', 'DIRGA JULIAN PURNAMA', NULL, '3329011207220001', 1, 0, '03', '01', '', '', '', 0, '2022-07-12', 'L', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '0.00', '2.00', '0 th, 2 bln', 0, 0, 81),
(236, '', NULL, 0, '420445935d6a47df8695dd9e827883', 'SYAKILA ALZAHRA', NULL, '00000000000000', 1, 0, '05', '01', '', '', '', 0, '2022-06-29', 'P', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '0.00', '3.00', '0 th, 3 bln', 0, 0, 81),
(237, '', NULL, 0, '0b24b5740b5c424bb87afedfc928b0', 'AISHWA NAMIRA', NULL, '3329014202190001', 1, 0, '04', '01', '', '', '', 0, '2019-02-02', 'L', NULL, 41165, 29, 41173, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '3.00', '44.00', '3 th, 8 bln', 0, 0, 11),
(238, '', NULL, 0, '0acdf591a1c642e58b3a8268ff0847', 'NYSA WIRA SYPA', NULL, '01', 1, 0, '01', '01', '', '', '', 0, '2022-06-28', 'P', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '0.00', '3.00', '0 th, 3 bln', 0, 0, 81),
(239, '', NULL, 0, '2883f9a4662d43f5ba4b033685c5f8', 'VIONA AKHALEENA MOZA', NULL, '001', 1, 0, '02', '01', '', '', '', 0, '2022-05-03', 'P', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '0.00', '5.00', '0 th, 5 bln', 0, 0, 81),
(240, '', NULL, 0, '422ea002970c400596d1ab0aaba6ef', 'HAJAN SAMUD', NULL, '3329011601190001', 1, 0, '02', '01', '', '', '', 0, '2019-01-16', 'L', NULL, 41165, 29, 41173, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '3.00', '44.00', '3 th, 8 bln', 0, 0, 11),
(241, '', NULL, 0, '227c380710c943c0bc263848df5229', 'LU\'LU\'UL MA\'WA', NULL, '3329015303220001', 1, 0, '02', '02', '', '', '', 0, '2022-03-12', 'P', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '0.00', '6.00', '0 th, 6 bln', 0, 0, 81),
(242, '', NULL, 0, 'db15248c935a4d32b8fe5737f08ad2', 'KEYRA ZEA AZZAHRA', NULL, '3329015212180001', 1, 0, '05', '01', '', '', '', 0, '2018-12-12', 'P', NULL, 41165, 29, 41173, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '3.00', '45.00', '3 th, 9 bln', 0, 0, 11),
(243, '', NULL, 0, '1e575633201144f1b82ad419f79f08', 'ABYAN KAMIL ALFARIQ', NULL, '3329011902228739', 1, 0, '05', '01', '', '', '', 0, '2022-02-19', 'L', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '0.00', '7.00', '0 th, 7 bln', 0, 0, 81),
(244, '', NULL, 0, 'bb955c817baa4acdb53e7b5accc6a2', 'CHAISYA ZEA ALIFIA', NULL, '11', 1, 0, '02', '02', '', '', '', 0, '2022-02-01', 'P', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'Brebes', '0.00', '8.00', '0 th, 8 bln', 0, 0, 81),
(245, '', NULL, 0, '35fc2a4a202b4f56a8ee2e00cb4de4', 'RAZAN MUHAMAD AFNAN', NULL, '3329011101220001', 1, 0, '05', '01', '', '', '', 0, '2022-01-11', 'L', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'Brebes', '0.00', '8.00', '0 th, 8 bln', 0, 0, 81),
(246, '', NULL, 0, 'abb1cd64b4224dcf975b8bd348f614', 'SHEZA AQILA NADRIA', NULL, '111', 1, 0, '04', '01', '', '', '', 0, '2021-12-12', 'P', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '0.00', '9.00', '0 th, 9 bln', 0, 0, 81),
(247, '', NULL, 0, 'b1cac228a20d4dab8d38abf2c427a8', 'SYAHLLA NURANI KHUMAIRA', NULL, '3329016705210001', 1, 0, '03', '02', '', '', '', 0, '2021-05-27', 'P', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '1.00', '16.00', '1 th, 4 bln', 0, 0, 81),
(248, '', NULL, 0, 'd2f85e34312f4bc2b5cf593cbbb568', 'MEIVLIN ELVAR ROBIN', NULL, '3329012505210001', 1, 0, '05', '01', '', '', '', 0, '2021-05-25', 'L', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '1.00', '16.00', '1 th, 4 bln', 0, 0, 81),
(249, '', NULL, 0, '43d9c5e07df748dcaf48f348193de4', 'GLADYS MEISYA JOSEPHIRA', NULL, '3329016405210002', 1, 0, '04', '01', '', '', '', 0, '2021-05-24', 'P', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '1.00', '16.00', '1 th, 4 bln', 0, 0, 81),
(250, '', NULL, 0, '80aabd4a65544373844fd14ddd8326', 'RAFARDHAN VIRENDRA', NULL, '3329011105210003', 1, 0, '02', '01', '', '', '', 0, '2021-05-11', 'L', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '1.00', '16.00', '1 th, 4 bln', 0, 0, 81),
(251, '', NULL, 0, 'efa99b7ef72c41dc9e348c67c7d44e', 'TAMI AULIA BORU SINAGA', NULL, '3329014505210001', 1, 0, '02', '02', '', '', '', 0, '2021-05-05', 'P', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '1.00', '17.00', '1 th, 5 bln', 0, 0, 81),
(252, '', NULL, 0, '4e95a4ba10a14a6e83f686653330e3', 'CANDIKA DIAR PRATAMA', NULL, '3329011904210003', 1, 0, '05', '02', '', '', '', 0, '2021-04-19', 'L', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '1.00', '17.00', '1 th, 5 bln', 0, 0, 81),
(253, '', NULL, 0, '6d16be0c952b4a97b6aca829b0588e', 'REGINA ASMITHA', NULL, '3329014810210003', 1, 0, '02', '01', '', '', '', 0, '2021-03-08', 'P', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '1.00', '19.00', '1 th, 7 bln', 0, 0, 81),
(254, '', NULL, 0, 'e438a52fc9ca45e5a432010cfd1e45', 'ATHALA BARRA FAHREZI', NULL, '3329012502210001', 1, 0, '01', '01', '', '', '', 0, '2021-02-25', 'L', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '1.00', '19.00', '1 th, 7 bln', 0, 0, 81),
(255, '', NULL, 0, 'aafeb1dcdff642aca876ad4d450902', 'TALISA PUTRI PANESA', NULL, '3329016402210002', 1, 0, '05', '02', '', '', '', 0, '2021-02-24', 'P', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '1.00', '19.00', '1 th, 7 bln', 0, 0, 81),
(256, '', NULL, 0, '539b74e969a34f2ebfb0d53530818a', 'MARVEL GINO PRATAMA', NULL, '3329011902210002', 1, 0, '04', '02', '', '', '', 0, '2021-02-19', 'L', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '1.00', '19.00', '1 th, 7 bln', 0, 0, 81),
(257, '', NULL, 0, 'dd1dc2b8a6024ba98815b5db1c4762', 'DARRA SHAFIRA', NULL, '3329014612180002', 1, 0, '01', '01', '', '', '', 0, '2018-12-06', 'P', NULL, 41165, 29, 41173, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '3.00', '46.00', '3 th, 10 bln', 0, 0, 11),
(258, '', NULL, 0, 'cc8390679791441c81bf671c8a4339', 'ZIFA ALULA MAGNOLYA', NULL, '3329015202210004', 1, 0, '02', '02', '', '', '', 0, '2021-02-12', 'P', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '1.00', '19.00', '1 th, 7 bln', 0, 0, 81),
(259, '', NULL, 0, '2c6671f9f1ce4a9baf1e21b297d085', 'SENJA HAIKAL AKSAR', NULL, '3329012201210001', 1, 0, '03', '01', '', '', '', 0, '2021-01-22', 'L', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '1.00', '20.00', '1 th, 8 bln', 0, 0, 81),
(260, '', NULL, 0, '6cb68e630cc64dceb9a25695460833', 'KHAFSATUL AHLA', NULL, '3329015311180001', 1, 0, '03', '01', '', '', '', 0, '2018-11-13', 'P', NULL, 41165, 29, 41173, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '3.00', '46.00', '3 th, 10 bln', 0, 0, 11),
(261, '', NULL, 0, 'defd216bf14447b981407f72c035e7', 'M. ARVINO M', NULL, '3329010910180002', 1, 0, '05', '01', '', '', '', 0, '2018-10-09', 'L', NULL, 41165, 29, 41173, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '4.00', '48.00', '4 th, 0 bln', 0, 0, 11),
(262, '', NULL, 0, '674f669702c94f9588014c9b8bee9a', 'YUNA ZIA SHAKAYLA', NULL, '3329015501210001', 1, 0, '04', '01', '', '', '', 0, '2021-01-15', 'P', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'Brebes', '1.00', '20.00', '1 th, 8 bln', 0, 0, 81),
(263, '', NULL, 0, 'da53d6435bda470fbc26d9ba8cfe93', 'HUMAIRA ZOYA D', NULL, '3329017009180001', 1, 0, '04', '01', '', '', '', 0, '2018-09-30', 'P', NULL, 41165, 29, 41173, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '4.00', '48.00', '4 th, 0 bln', 0, 0, 11),
(264, '', NULL, 0, '15fdcb545ac146678538e705e215e6', 'BANU SYIHAM AL\'HASBI', NULL, '3329010601210001', 1, 0, '03', '01', '', '', '', 0, '2021-01-06', 'L', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'Brebes', '1.00', '21.00', '1 th, 9 bln', 0, 0, 81),
(265, '', NULL, 0, '77e2a06eca804fe8894c258f684946', 'SHANUM AYUDIA F', NULL, '3329016909180002', 1, 0, '03', '01', '', '', '', 0, '2018-09-29', 'P', NULL, 41165, 29, 41173, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '4.00', '48.00', '4 th, 0 bln', 0, 0, 11),
(266, '', NULL, 0, '193cf0257cc64070977c24d32d99b5', 'NINO ALDEBARAN', NULL, '3329013012200001', 1, 0, '03', '02', '', '', '', 0, '2020-12-30', 'L', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'Brebes', '1.00', '21.00', '1 th, 9 bln', 0, 0, 81),
(267, '', NULL, 0, '4f4736cfd2884335b6fdbca6c1f1a8', 'ARKANA ADIJAYA', NULL, '3329012112210001', 1, 0, '03', '01', '', '', '', 0, '2020-12-21', 'L', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '1.00', '21.00', '1 th, 9 bln', 0, 0, 81),
(268, '', NULL, 0, 'fa292614a51c450d80d1c3d483d467', 'KIANO DIRGANTARA', NULL, '3329012012200002', 1, 0, '05', '01', '', '', '', 0, '2020-12-20', 'L', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '1.00', '21.00', '1 th, 9 bln', 0, 0, 81),
(269, '', NULL, 0, '2afcada88b71435ca65c980fb87990', 'LAIBA KHAIRA UTAMI', NULL, '3329015811200001', 1, 0, '05', '01', '', '', '', 0, '2020-11-18', 'P', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '1.00', '22.00', '1 th, 10 bln', 0, 0, 81),
(270, '', NULL, 0, '379c6d66d9014cfcac313acfd19c54', 'KENAN NABIL', NULL, '3329011510200001', 1, 0, '04', '01', '', '', '', 0, '2020-10-15', 'L', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '1.00', '23.00', '1 th, 11 bln', 0, 0, 81),
(271, '', NULL, 0, '8d62ed33c66347ad9a040f9a1c4756', 'GHAISAN ATALAH TIANDRA', NULL, '3329012909200002', 1, 0, '01', '02', '', '', '', 0, '2020-09-29', 'L', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '2.00', '24.00', '2 th, 0 bln', 0, 0, 81),
(272, '', NULL, 0, '055a9c11ab754887864080b0fe31d5', 'KHALIFAH QUROTUL', NULL, '3329016308180003', 1, 0, '02', '01', '', '', '', 0, '2018-08-23', 'P', NULL, 41165, 29, 41173, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '4.00', '49.00', '4 th, 1 bln', 0, 0, 11),
(273, '', NULL, 0, '7ee914a5ffe747e0b05475219a6507', 'CHOIRUNNISA SALSABILA PUTRI', NULL, '3329016909200003', 1, 0, '02', '01', '', '', '', 0, '2020-09-29', 'P', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '2.00', '24.00', '2 th, 0 bln', 0, 0, 81),
(274, '', NULL, 0, 'cc53d2783cc641f6bdf546d870a542', 'BJORKA ATHAR A', NULL, '3329011607180001', 1, 0, '04', '01', '', '', '', 0, '2018-07-16', 'L', NULL, 41165, 29, 41173, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '4.00', '50.00', '4 th, 2 bln', 0, 0, 11),
(275, '', NULL, 0, '56e630916cb54b2e95f6ee8e3ad489', 'BINTANG FALIZA LESTARI', NULL, '3329016009200003', 1, 0, '03', '02', '', '', '', 0, '2020-09-20', 'P', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '2.00', '24.00', '2 th, 0 bln', 0, 0, 81),
(276, '', NULL, 0, '33536eeb9b6141dfa0b9b4c7ba1b58', 'M. ABIZHAR RAMDHANI', NULL, '3329012805180001', 1, 0, '01', '01', '', '', '', 0, '2018-05-28', 'L', NULL, 41165, 29, 41173, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '4.00', '52.00', '4 th, 4 bln', 0, 0, 11),
(277, '', NULL, 0, 'e41f0e73ae8d4dec9a8c3205d92711', 'MUHAMAD RAZAN FATURAHMAN', NULL, '3329010709200002', 1, 0, '04', '02', '', '', '', 0, '2020-09-07', 'L', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '2.00', '25.00', '2 th, 1 bln', 0, 0, 81),
(278, '', NULL, 0, '3b31581184c94ee7bd7b4c30f8c864', 'RAJA BIJAK', NULL, '3329011304180002', 1, 0, '01', '01', '', '', '', 0, '2018-04-13', 'L', NULL, 41165, 29, 41173, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '4.00', '53.00', '4 th, 5 bln', 0, 0, 11),
(279, '', NULL, 0, '748c32dc63a141c690ca343ccb933e', 'CHERRY AJENG WINTANIIA', NULL, '3329014208200002', 1, 0, '03', '01', '', '', '', 0, '2020-08-02', 'P', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '2.00', '26.00', '2 th, 2 bln', 0, 0, 81),
(280, '', NULL, 0, '81ca48766d9a45b49131b0ac51bdb9', 'LITA MILKA KOMARI', NULL, '3329016003180001', 1, 0, '02', '01', '', '', '', 0, '2018-03-20', 'L', NULL, 41165, 29, 41173, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '4.00', '54.00', '4 th, 6 bln', 0, 0, 11),
(281, '', NULL, 0, 'd0eb2dc80de746efb8f3f466ebb804', 'ARUMY ISMAYANTI S', NULL, '3329015707200002', 1, 0, '03', '01', '', '', '', 0, '2020-07-05', 'P', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '2.00', '27.00', '2 th, 3 bln', 0, 0, 81),
(282, '', NULL, 0, '84712e1cfaa84e5d859b194a01bfa2', 'QUINARA AZELA', NULL, '3329014507200004', 1, 0, '03', '02', '', '', '', 0, '2020-07-05', 'P', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '2.00', '27.00', '2 th, 3 bln', 0, 0, 81),
(283, '', NULL, 0, '1a21bea586a44288a1a60d5537c89e', 'ERGI ALIF FAHREZA', NULL, '3329012206200003', 1, 0, '03', '02', '', '', '', 0, '2020-06-22', 'L', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '2.00', '27.00', '2 th, 3 bln', 0, 0, 81),
(284, '', NULL, 0, '8a7aa5a1eaeb4ee7ad270c68357d39', 'IBRAHIM AL HAFIZ', NULL, '3329011103180001', 1, 0, '02', '01', '', '', '', 0, '2018-03-11', 'L', NULL, 41165, 29, 41173, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '4.00', '55.00', '4 th, 7 bln', 0, 0, 11),
(285, '', NULL, 0, '7a57b096635847d68320c6eb76cebd', 'SHELINA ZAHWA ALIANI', NULL, '3329015106200001', 1, 0, '03', '02', '', '', '', 0, '2020-06-11', 'P', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '2.00', '27.00', '2 th, 3 bln', 0, 0, 81),
(286, '', NULL, 0, '7cf43bbac04747259e3c99e73e56a1', 'AKMAL FAUZAN A', NULL, '3329010803180001', 1, 0, '04', '01', '', '', '', 0, '2018-03-08', 'L', NULL, 41165, 29, 41173, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '4.00', '55.00', '4 th, 7 bln', 0, 0, 11),
(287, '', NULL, 0, 'd73cdac2614e4cc58ccd0ac66f45d9', 'MISYA PUTRI ALVIANA', NULL, '3329014904200002', 1, 0, '03', '02', '', '', '', 0, '2020-04-09', 'P', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '2.00', '30.00', '2 th, 6 bln', 0, 0, 81),
(288, '', NULL, 0, 'a815307ae3ca4b138d22e9da74c8e2', 'TANISTHA ZARA M', NULL, '3329014612170001', 1, 0, '01', '01', '', '', '', 0, '2017-12-06', 'P', NULL, 41165, 29, 41173, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '4.00', '58.00', '4 th, 10 bln', 0, 0, 11),
(289, '', NULL, 0, '476756f8510a4ea79743621fc373c1', 'AIRA BELLVANIA PUTRI', NULL, '3329014704200002', 1, 0, '05', '01', '', '', '', 0, '2020-04-07', 'P', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '2.00', '30.00', '2 th, 6 bln', 0, 0, 81),
(290, '', NULL, 0, '4d3a904cc32242a59e755bdff852b4', 'M.ARFI ABRISYAM', NULL, '3329011910170002', 1, 0, '04', '01', '', '', '', 0, '2017-10-19', 'L', NULL, 41165, 29, 41173, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '4.00', '59.00', '4 th, 11 bln', 0, 0, 11),
(291, '', NULL, 0, '7a7666f345114e758c528fe9431a77', 'TANISA TANZELA', NULL, '3329014403200002', 1, 0, '02', '02', '', '', '', 0, '2020-03-04', 'P', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '2.00', '31.00', '2 th, 7 bln', 0, 0, 81),
(292, '', NULL, 0, '0085b90ae36740e3971544fc5428d1', 'BERYL HAMIZAN RABBANI', NULL, '3329012702200002', 1, 0, '03', '02', '', '', '', 0, '2020-02-27', 'L', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '2.00', '31.00', '2 th, 7 bln', 0, 0, 81),
(293, '', NULL, 0, 'e3ee9694b05e41869f53ee7e002094', 'ALVAN GHAISAN', NULL, '3329012102200001', 1, 0, '01', '01', '', '', '', 0, '2020-02-21', 'L', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '2.00', '31.00', '2 th, 7 bln', 0, 0, 81),
(294, '', NULL, 0, '1356d512aea54f2eb60fe55ae8c579', 'GALUH PUGI YASINTA', NULL, '332901600200001', 1, 0, '05', '01', '', '', '', 0, '2020-02-20', 'P', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '2.00', '31.00', '2 th, 7 bln', 0, 0, 81),
(295, '', NULL, 0, '5ecd2f155af6426c82fede8d7babaf', 'NAURA ALFAT', NULL, '3329015902200001', 1, 0, '03', '01', '', '', '', 0, '2020-02-19', 'P', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '2.00', '31.00', '2 th, 7 bln', 0, 0, 81),
(296, '', NULL, 0, '668a83d6e1f0462b9f27e750ea6520', 'ADIBA SYAKILA ATMARINI', NULL, '3329014702200007', 1, 0, '05', '02', '', '', '', 0, '2020-02-07', 'P', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '2.00', '32.00', '2 th, 8 bln', 0, 0, 81),
(297, '', NULL, 0, 'f85997fe17954d37a4adc2ae17b000', 'ALLEN ALIA PUTRI', NULL, '3329015310170001', 1, 0, '03', '01', '', '', '', 0, '2017-10-13', 'P', NULL, 41165, 29, 41173, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '4.00', '59.00', '4 th, 11 bln', 0, 0, 11),
(298, '', NULL, 0, '77a8f36026554205b4712933c20709', 'FABIAN ALHANAN', NULL, '3329010502200007', 1, 0, '03', '01', '', '', '', 0, '2020-02-05', 'L', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '2.00', '32.00', '2 th, 8 bln', 0, 0, 81),
(299, '', NULL, 0, '2b6b05a75eb2489796047b8427c66d', 'REYNA ZALFA AURELIA', NULL, '3329016001200002', 1, 0, '02', '01', '', '', '', 0, '2020-01-20', 'P', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '2.00', '32.00', '2 th, 8 bln', 0, 0, 81),
(300, '', NULL, 0, 'be10039825b24e2ea58ef5951bd33c', 'ANINDA FAUZIYAH', NULL, '3329011510170001', 1, 0, '05', '01', '', '', '', 0, '2017-10-11', 'P', NULL, 41165, 29, 41173, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '4.00', '60.00', '4 th, 12 bln', 0, 0, 11),
(301, '', NULL, 0, 'b82431dbe92345e7ac48ce76e0b6dd', 'MUHAMMAD FAUZAN', NULL, '3329011110170001', 1, 0, '05', '01', '', '', '', 0, '2017-10-11', 'L', NULL, 41165, 29, 41173, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '4.00', '60.00', '4 th, 12 bln', 0, 0, 11),
(302, '', NULL, 0, 'e786a94e941c4ccc9a66b99f2a06cd', 'NADIVA YASNA FAUZIAH', NULL, '3329014901200002', 1, 0, '05', '02', '', '', '', 0, '2020-01-09', 'P', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '2.00', '33.00', '2 th, 9 bln', 0, 0, 81),
(303, '', NULL, 0, '246833e650c3499e8dbb379194476e', 'DARA OKTAVIA', NULL, '3329015810190002', 1, 0, '03', '01', '', '', '', 0, '2019-10-18', 'P', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '2.00', '35.00', '2 th, 11 bln', 0, 0, 81),
(304, '', NULL, 0, 'a989e378aa1e4cb486e10e91980724', 'TANISA KANSA AULIA', NULL, '3329016509190002', 1, 0, '01', '02', '', '', '', 0, '2019-09-25', 'P', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '3.00', '36.00', '3 th, 0 bln', 0, 0, 81),
(305, '', NULL, 0, '02b4c225af894e81991bc192d5045b', 'AHZA PRADIPTA', NULL, '', 1, 0, '01', '01', '', '', '', 0, '2022-07-21', 'L', NULL, 41165, 29, 41173, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '0.00', '2.00', '0 th, 2 bln', 0, 0, 11),
(306, '', NULL, 0, 'f42b6fb868424d4fa283aaa6b17428', 'RANFI AL ADHA', NULL, '3329010608190002', 1, 0, '01', '01', '', '', '', 0, '2019-08-06', 'L', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '3.00', '38.00', '3 th, 2 bln', 0, 0, 81),
(307, '', NULL, 0, '1812e5d169df4ef7b8b62f53d62b6f', 'M FAREL ARDAFA', NULL, '3329010108190002', 1, 0, '02', '01', '', '', '', 0, '2019-08-01', 'L', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '3.00', '38.00', '3 th, 2 bln', 0, 0, 81),
(308, '', NULL, 0, 'a69b021825da40fcbe7eb5b13b17e6', 'BRILIAN ALDAMA BUDIMAN', NULL, '3329011507190001', 1, 0, '01', '01', '', '', '', 0, '2019-07-15', 'L', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '3.00', '38.00', '3 th, 2 bln', 0, 0, 81),
(309, '', NULL, 0, '7cdf2ad8fb624de5853b5809e655f2', 'AMMAR A', NULL, '3329012606190002', 1, 0, '01', '01', '', '', '', 0, '2019-06-26', 'L', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '3.00', '39.00', '3 th, 3 bln', 0, 0, 81),
(310, '', NULL, 0, '6d54fd5e959043aab65590b76ddcb9', 'ALAYYA AKIFA GHAVA PUTRI', NULL, '3329016206190002', 1, 0, '04', '01', '', '', '', 0, '2019-06-22', 'P', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '3.00', '39.00', '3 th, 3 bln', 0, 0, 81),
(311, '', NULL, 0, '4d0e79ea4e1847b7b01daf005d9978', 'RAINA KHUMAIRA JEKAL', NULL, '3329015706190002', 1, 0, '01', '01', '', '', '', 0, '2019-06-18', 'P', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '3.00', '39.00', '3 th, 3 bln', 0, 0, 81),
(312, '', NULL, 0, 'e2c727356b31417b8c7a475cb3ca72', 'ERLAN GIOVANNO', NULL, '3329011606190002', 1, 0, '05', '02', '', '', '', 0, '2019-06-16', 'L', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '3.00', '39.00', '3 th, 3 bln', 0, 0, 81),
(313, '', NULL, 0, '59b8fd8312074c4a8568eb755d6c94', 'ERLIN GIOVANNY', NULL, '3329015606190003', 1, 0, '05', '01', '', '', '', 0, '2019-06-16', 'P', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '3.00', '39.00', '3 th, 3 bln', 0, 0, 81),
(314, '', NULL, 0, 'fe13a1508377495cb1ff106e31ec14', 'DELVIN REIFANSYAH', NULL, '3329010706190001', 1, 0, '04', '01', '', '', '', 0, '2019-06-07', 'L', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '3.00', '40.00', '3 th, 4 bln', 0, 0, 81),
(315, '', NULL, 0, '224acc1efc094849b03005f3a21ff7', 'BAYU ANGGARA', NULL, '3329012405190002', 1, 0, '01', '01', '', '', '', 0, '2019-05-23', 'L', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '3.00', '40.00', '3 th, 4 bln', 0, 0, 81),
(316, '', NULL, 0, '80edce943f984fe68c297aa4b55160', 'ILYAS', NULL, '3329012105190007', 1, 0, '03', '02', '', '', '', 0, '2019-05-21', 'L', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '3.00', '40.00', '3 th, 4 bln', 0, 0, 81),
(317, '', NULL, 0, '3ca9b98c123e4d2781d8642a00bea2', 'WENDA MAULANA', NULL, '3329010405190002', 1, 0, '04', '01', '', '', '', 0, '2019-05-04', 'L', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '3.00', '41.00', '3 th, 5 bln', 0, 0, 81),
(318, '', NULL, 0, 'b271119d97f347d281c3276806ddd5', 'WAFIQ UQAIL', NULL, '3329012004190004', 1, 0, '01', '01', '', '', '', 0, '2019-04-20', 'L', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '3.00', '41.00', '3 th, 5 bln', 0, 0, 81),
(319, '', NULL, 0, '9230687a2e6c47b3b9d6d5fe568a3c', 'SEINA KLARA APRILIA', NULL, '3329015704190002', 1, 0, '02', '01', '', '', '', 0, '2019-04-17', 'P', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '3.00', '41.00', '3 th, 5 bln', 0, 0, 81),
(320, '', NULL, 0, 'b3400e370a25468397a21815babf2f', 'KESYA JAVANKA', NULL, '3329015304190003', 1, 0, '02', '02', '', '', '', 0, '2019-04-13', 'P', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '3.00', '41.00', '3 th, 5 bln', 0, 0, 81),
(321, '', NULL, 0, '0df2700564f7471e88b74471640344', 'MUHAMMAD SHAFIQ ASYURA', NULL, '3329011403190001', 1, 0, '01', '01', '', '', '', 0, '2019-03-14', 'L', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '3.00', '42.00', '3 th, 6 bln', 0, 0, 81),
(322, '', NULL, 0, '84f7aada75e0478ca459a2e20406f2', 'ARKA', NULL, '3329012402190001', 1, 0, '04', '01', '', '', '', 0, '2019-02-24', 'L', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '3.00', '43.00', '3 th, 7 bln', 0, 0, 81),
(323, '', NULL, 0, '91db6e8941484648946eb56f1ca7b5', 'M WIJAYA', NULL, '3329011802190003', 1, 0, '02', '01', '', '', '', 0, '2019-02-18', 'L', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '3.00', '43.00', '3 th, 7 bln', 0, 0, 81),
(324, '', NULL, 0, '449a7abcc1084585badbefb9550afe', 'DELIMA REFALINA', NULL, '3329015502190001', 1, 0, '04', '01', '', '', '', 0, '2019-02-16', 'P', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '3.00', '43.00', '3 th, 7 bln', 0, 0, 81),
(325, '', NULL, 0, '60f96d908e174622af86503e8453a0', 'RESA ARDIANSYAH', NULL, '3329011202190001', 1, 0, '04', '01', '', '', '', 0, '2019-02-12', 'L', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '3.00', '43.00', '3 th, 7 bln', 0, 0, 81),
(326, '', NULL, 0, '0ea4a0766f984bdaa4a041fa8dd7e9', 'DELISHA FAZA MAUDIYA', NULL, '3329016501190002', 1, 0, '05', '01', '', '', '', 0, '2019-01-25', 'P', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '3.00', '44.00', '3 th, 8 bln', 0, 0, 81),
(327, '', NULL, 0, 'fa3631c9a0604d7580841bf6332675', 'ARSYILA AIRA N', NULL, '3329016112180002', 1, 0, '02', '01', '', '', '', 0, '2018-12-21', 'P', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '3.00', '45.00', '3 th, 9 bln', 0, 0, 81),
(328, '', NULL, 0, 'a8c984ce3abc49e7824d45c46e3652', 'KETLIN AURELIA', NULL, '3329014112180002', 1, 0, '04', '02', '', '', '', 0, '2018-12-10', 'P', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '3.00', '45.00', '3 th, 9 bln', 0, 0, 81),
(329, '', NULL, 0, '24ed5d348ab749ba8cbd20dfadea64', 'FAHMI IMAMUL HAKIM', NULL, '3329012811180004', 1, 0, '02', '01', '', '', '', 0, '2018-11-28', 'L', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '3.00', '46.00', '3 th, 10 bln', 0, 0, 81),
(330, '', NULL, 0, '7d47cfec882c492f9c9a15fc1c211a', 'LALA SINTIA BELA', NULL, '3329015711180001', 1, 0, '05', '01', '', '', '', 0, '2018-11-17', 'P', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '3.00', '46.00', '3 th, 10 bln', 0, 0, 81),
(331, '', NULL, 0, '93d7586a8e0e4b1a83e02dc854623f', 'WIBI IVAN DAMARES', NULL, '3329011711180003', 1, 0, '04', '01', '', '', '', 0, '2018-10-17', 'L', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '3.00', '47.00', '3 th, 11 bln', 0, 0, 81),
(332, '', NULL, 0, '7705d2c8be95448b936718b415f04a', 'JAHRA MANUHARA', NULL, '3329016809180002', 1, 0, '02', '02', '', '', '', 0, '2018-09-28', 'P', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '4.00', '48.00', '4 th, 0 bln', 0, 0, 81),
(333, '', NULL, 0, 'ca9d4667760e42d3830bff87961584', 'RIZAM ZIKRILLAH', NULL, '3329012109180001', 1, 0, '02', '01', '', '', '', 0, '2018-09-21', 'L', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '4.00', '48.00', '4 th, 0 bln', 0, 0, 81),
(334, '', NULL, 0, '72c3f7e833af4aae9c292f57cfcfce', 'HERVI NUR SHAKILLA', NULL, '3329014909180001', 1, 0, '03', '01', '', '', '', 0, '2018-09-09', 'P', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '4.00', '49.00', '4 th, 1 bln', 0, 0, 81),
(335, '', NULL, 0, '954b587618a6429485a8275fdceda2', 'M ROJAK', NULL, '3329010509180001', 1, 0, '05', '01', '', '', '', 0, '2018-09-05', 'L', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '4.00', '49.00', '4 th, 1 bln', 0, 0, 81),
(336, '', NULL, 0, '260d28cc4ba44d349ca5538b1bc8b4', 'TRI LESTARI', NULL, '3329014808180002', 1, 0, '04', '02', '', '', '', 0, '2018-08-08', 'P', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '4.00', '50.00', '4 th, 2 bln', 0, 0, 81),
(337, '', NULL, 0, '6f7d05ffc6e74002a6006451918ac9', 'CUCU DWIYANTI', NULL, '3329016407180003', 1, 0, '04', '01', '', '', '', 0, '2018-07-24', 'P', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '4.00', '50.00', '4 th, 2 bln', 0, 0, 81),
(338, '', NULL, 0, 'b1bd07a6b48b4e669ac109125fc1f5', 'NAZWA AULIA', NULL, '3329015407180001', 1, 0, '01', '02', '', '', '', 0, '2018-07-14', 'P', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '4.00', '50.00', '4 th, 2 bln', 0, 0, 81),
(339, '', NULL, 0, 'a2cef5b9ee804870873347f9931201', 'MUHAMMAD SYAMIL DZIKRULLAH', NULL, '3329012806180001', 1, 0, '05', '01', '', '', '', 0, '2018-06-28', 'L', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '4.00', '51.00', '4 th, 3 bln', 0, 0, 81),
(340, '', NULL, 0, 'ab95e8171c7a4ecea8bceeefa5a594', 'FAQIH ADI MULIYA', NULL, '3329010306180004', 1, 0, '04', '01', '', '', '', 0, '2018-06-03', 'L', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'Brebes', '4.00', '52.00', '4 th, 4 bln', 0, 0, 81),
(341, '', NULL, 0, '064313170d0d49bbbd696973ac7f00', 'CLEON ABI FAHREZI', NULL, '3329012705180002', 1, 0, '01', '01', '', '', '', 0, '2018-05-27', 'L', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'Brebes', '4.00', '52.00', '4 th, 4 bln', 0, 0, 81),
(342, '', NULL, 0, '0da46ad1e24e420084eb50fc1df69f', 'ELSI PUTRI', NULL, '3329015505180001', 1, 0, '05', '02', '', '', '', 0, '2018-05-14', 'P', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'Brebes', '4.00', '52.00', '4 th, 4 bln', 0, 0, 81),
(343, '', NULL, 0, 'e5b3de9a700d480586ab79b6cf7c22', 'KEIA READATHUL Z', NULL, '3329015305180001', 1, 0, '01', '01', '', '', '', 0, '2018-05-13', 'P', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'Brebes', '4.00', '52.00', '4 th, 4 bln', 0, 0, 81),
(344, '', NULL, 0, '3ccc3c13ff3b4742b73962a3911e5b', 'DHAFIS ALPIAN', NULL, '3329011105180003', 1, 0, '04', '01', '', '', '', 0, '2018-05-11', 'L', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'Brebes', '4.00', '52.00', '4 th, 4 bln', 0, 0, 81),
(345, '', NULL, 0, '009473fc4c7547bcaae6dfeaee809b', 'CHITRA AYUDIANTI', NULL, '3329024105180001', 1, 0, '05', '01', '', '', '', 0, '2018-05-01', 'P', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'Brebes', '4.00', '53.00', '4 th, 5 bln', 0, 0, 81),
(346, '', NULL, 0, '9d7382099b894cd592676f67ea7935', 'CHANDA KAVIA ANABELLA', NULL, '3329017004180001', 1, 0, '02', '02', '', '', '', 0, '2018-04-30', 'P', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '4.00', '53.00', '4 th, 5 bln', 0, 0, 81),
(347, '', NULL, 0, '747ac14a2fb34c1f99f35b6eb52830', 'M LATIF FAUZI', NULL, '3329012704180002', 1, 0, '02', '01', '', '', '', 0, '2018-04-27', 'L', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '4.00', '53.00', '4 th, 5 bln', 0, 0, 81),
(348, '', NULL, 0, '40794cc3f91c406fbadee187729a27', 'DEVAN ELGI PARI', NULL, '3329012704180003', 1, 0, '01', '01', '', '', '', 0, '2018-04-27', 'L', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'Brebes', '4.00', '53.00', '4 th, 5 bln', 0, 0, 81),
(349, '', NULL, 0, '4fc45da79b844fdcacc9b32bdecfc5', 'CLARISA PUTRI NADIFA', NULL, '3329014804180003', 1, 0, '03', '02', '', '', '', 0, '2018-04-08', 'P', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'Brebes', '4.00', '54.00', '4 th, 6 bln', 0, 0, 81),
(350, '', NULL, 0, 'd8d5ac7211414652b12996812a29a1', 'RACHEL GIMANA N', NULL, '3329016603180001', 1, 0, '05', '02', '', '', '', 0, '2018-03-26', 'P', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'Brebes', '4.00', '54.00', '4 th, 6 bln', 0, 0, 81),
(351, '', NULL, 0, '2d42f701a2db482c9e4637e600323a', 'WAFI ARSAD ALWI', NULL, '3329011703180004', 1, 0, '02', '01', '', '', '', 0, '2018-03-17', 'L', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '4.00', '54.00', '4 th, 6 bln', 0, 0, 81),
(352, '', NULL, 0, '4b4e7c7929c54fbdb080b653998c6a', 'RAHEL ANANDA P', NULL, '3329015003180001', 1, 0, '04', '01', '', '', '', 0, '2018-03-10', 'P', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '4.00', '55.00', '4 th, 7 bln', 0, 0, 81),
(353, '', NULL, 0, '4ec71cebabb64457bdbfc8b9471741', 'RADEVA SELIKA PUTRI', NULL, '3329014903180001', 1, 0, '01', '01', '', '', '', 0, '2018-03-09', 'P', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'Brebes', '4.00', '55.00', '4 th, 7 bln', 0, 0, 81),
(354, '', NULL, 0, '390e6d1d842141d7be7cda29c4835f', 'ALDO MAHESA', NULL, '3329011102180001', 1, 0, '04', '01', '', '', '', 0, '2018-02-11', 'L', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'Brebes', '4.00', '55.00', '4 th, 7 bln', 0, 0, 81),
(355, '', NULL, 0, '41a631cdc3b34f2e80cfd4d44faaa1', 'TIASA MIKAILA AL KANZA', NULL, '3329014702180002', 1, 0, '01', '01', '', '', '', 0, '2018-02-07', 'P', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'Brebes', '4.00', '56.00', '4 th, 8 bln', 0, 0, 81),
(356, '', NULL, 0, 'a4a1659669c24c0eae3c13ba5cd1ee', 'ALMIRA PITALOKA', NULL, '3329027101180001', 1, 0, '01', '01', '', '', '', 0, '2018-01-31', 'P', NULL, 41165, 29, 41171, 0, 0, NULL, NULL, 'Lajang', 'Brebes', '4.00', '56.00', '4 th, 8 bln', 0, 0, 81),
(357, '', NULL, 0, 'cf4d72d3f13f4577b06fe62682482f', 'AL FIKRI', NULL, '3329012101180001', 1, 0, '03', '02', '', '', '', 0, '2018-01-21', 'L', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'Brebes', '4.00', '56.00', '4 th, 8 bln', 0, 0, 81),
(358, '', NULL, 0, '2d7d91a7366a4d5897ed14a1ab57cd', 'DICARI AMORA', NULL, '3329014501180002', 1, 0, '05', '02', '', '', '', 0, '2018-01-05', 'P', NULL, 41165, 29, 41172, 0, 0, NULL, NULL, 'Lajang', 'Brebes', '4.00', '57.00', '4 th, 9 bln', 0, 0, 81),
(359, '', NULL, 0, '3574794edf0f4a56959324a87df1f4', 'RAFFASYA HANAN', NULL, '3329012006220002', 1, 0, '07', '02', '', '', '', 0, '2022-06-20', 'L', NULL, 41165, 29, 41173, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '0.00', '3.00', '0 th, 3 bln', 0, 0, 11),
(360, '', NULL, 0, '7d689484039d470090bd3e729ca7e7', 'ARSYLA MARWAH A', NULL, '3329015406220002', 1, 0, '08', '02', '', '', '', 0, '2022-06-14', 'P', NULL, 41165, 29, 41173, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '0.00', '3.00', '0 th, 3 bln', 0, 0, 11),
(361, '', NULL, 0, 'dfc953ae4ea94ddea81e6e346272d5', 'ALLENA ZAHRA M', NULL, '3329014107220007', 1, 0, '06', '02', '', '', '', 0, '2022-07-01', 'P', NULL, 41165, 29, 41173, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '0.00', '3.00', '0 th, 3 bln', 0, 0, 11),
(362, '', NULL, 0, '9d3d39aa672f43c2a69f2ebf281a1d', 'AHMAD KAIS AL HASAN', NULL, '3329012205220001', 1, 0, '06', '02', '', '', '', 0, '2022-05-22', 'L', NULL, 41165, 29, 41173, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '0.00', '4.00', '0 th, 4 bln', 0, 0, 11),
(363, '', NULL, 0, 'acf78aee87554a72b1bb28ebb7171a', 'M.HISYAM JAUHAR', NULL, '3329011605220001', 1, 0, '08', '02', '', '', '', 0, '2022-05-16', 'L', NULL, 41165, 29, 41173, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '0.00', '4.00', '0 th, 4 bln', 0, 0, 11),
(364, '', NULL, 0, 'd362a0c819b74001beb2d6b3b95af0', 'RAYINA ADZRA AURORA', NULL, '3329016604220001', 1, 0, '08', '02', '', '', '', 0, '2022-04-26', 'P', NULL, 41165, 29, 41173, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '0.00', '5.00', '0 th, 5 bln', 0, 0, 11),
(365, '', NULL, 0, 'c579907ac410423b9aab2f742e2e9e', 'ERSYA ADIVA ARSYLA', NULL, '3329016104220002', 1, 0, '06', '02', '', '', '', 0, '2022-04-21', 'P', NULL, 41165, 29, 41173, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '0.00', '5.00', '0 th, 5 bln', 0, 0, 11),
(366, '', NULL, 0, '42656c1d24c94fa99a7e4504cab491', 'WIDIASARI APRILIANI', NULL, '3329014904220001', 1, 0, '07', '02', '', '', '', 0, '2022-04-09', 'P', NULL, 41165, 29, 41173, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '0.00', '6.00', '0 th, 6 bln', 0, 0, 11),
(367, '', NULL, 0, '753935959ceb4e68b92b238b51d03d', 'SHEILA KHOERUNNISA', NULL, '3329014701220001', 1, 0, '08', '02', '', '', '', 0, '2022-01-07', 'P', NULL, 41165, 29, 41173, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '0.00', '9.00', '0 th, 9 bln', 0, 0, 11),
(368, '', NULL, 0, '98a8af7ce69e460eafdcfb894b4299', 'KANIA CAHYA ARINI', NULL, '3329014201220001', 1, 0, '07', '02', '', '', '', 0, '2022-01-02', 'P', NULL, 41165, 29, 41173, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '0.00', '9.00', '0 th, 9 bln', 0, 0, 11),
(369, '', NULL, 0, 'ad591540c763411fa8367ebd447b83', 'M.DAVA AL FARIZI', NULL, '3329011412210001', 1, 0, '07', '02', '', '', '', 0, '2021-12-14', 'L', NULL, 41165, 29, 41173, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '0.00', '9.00', '0 th, 9 bln', 0, 0, 11),
(370, '', NULL, 0, '55f600ebc5724656b03adc8a8e0115', 'NAZEEYA ALMAIRA A', NULL, '3329014509210002', 1, 0, '06', '02', '', '', '', 0, '2021-09-05', 'P', NULL, 41165, 29, 41173, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '1.00', '13.00', '1 th, 1 bln', 0, 0, 11),
(371, '', NULL, 0, 'e8c82e5d422047949a696f38033804', 'AMALUNA SAHIRA', NULL, '3329017008210001', 1, 0, '08', '02', '', '', '', 0, '2021-08-30', 'P', NULL, 41165, 29, 41173, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '1.00', '13.00', '1 th, 1 bln', 0, 0, 11),
(372, '', NULL, 0, 'e5dd2fab1b604e2a801c72185a06d5', 'A.ABDULLAH MAHERZA', NULL, '3329012108210001', 1, 0, '07', '02', '', '', '', 0, '2021-08-21', 'L', NULL, 41165, 29, 41173, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '1.00', '13.00', '1 th, 1 bln', 0, 0, 11),
(373, '', NULL, 0, 'df29427c769c498fa21b2433632151', 'M.KAFFA AL BIRUNI', NULL, '3329011208210001', 1, 0, '08', '02', '', '', '', 0, '2021-08-12', 'L', NULL, 41165, 29, 41173, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '1.00', '13.00', '1 th, 1 bln', 0, 0, 11),
(374, '', NULL, 0, 'd862c4ac20704ff9b71736a499496a', 'M.JAYYAN MUSTOFAINAL', NULL, '3329012707210001', 1, 0, '08', '02', '', '', '', 0, '2021-07-27', 'L', NULL, 41165, 29, 41173, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '1.00', '14.00', '1 th, 2 bln', 0, 0, 11),
(375, '', NULL, 0, '2c2862298db743de997dcc5dd2c948', 'ASYKARA BINTANG A', NULL, '3329015806210001', 1, 0, '07', '02', '', '', '', 0, '2021-06-18', 'P', NULL, 41165, 29, 41173, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '1.00', '15.00', '1 th, 3 bln', 0, 0, 11),
(376, '', NULL, 0, '59b603c7c31842a38e3c9d93b89930', 'M.ARSYAD MUZZAKI', NULL, '3329011306210001', 1, 0, '07', '02', '', '', '', 0, '2021-06-13', 'L', NULL, 41165, 29, 41173, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '1.00', '15.00', '1 th, 3 bln', 0, 0, 11),
(377, '', NULL, 0, '72e3ae9e0a9345d481ae0ebf6d2b59', 'KHAYRAN ARYA BIMA', NULL, '3329011105210001', 1, 0, '06', '02', '', '', '', 0, '2021-05-11', 'L', NULL, 41165, 29, 41173, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '1.00', '16.00', '1 th, 4 bln', 0, 0, 11),
(378, '', NULL, 0, '94df0231a6c04717940894caf7bb7a', 'RIZKY YUDISTIRA', NULL, '3329012803210001', 1, 0, '08', '02', '', '', '', 0, '2021-03-28', 'L', NULL, 41165, 29, 41173, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '1.00', '18.00', '1 th, 6 bln', 0, 0, 11),
(379, '', NULL, 0, '036e35c1fb414522b22f4c987b220c', 'SYAKIRA ILYA MAOLIYA', NULL, '3329015702210001', 1, 0, '06', '02', '', '', '', 0, '2021-02-17', 'P', NULL, 41165, 29, 41173, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '1.00', '19.00', '1 th, 7 bln', 0, 0, 11),
(380, '', NULL, 0, '387e88222bae4f5e8580625b359940', 'M ASKA VIRENDRA', NULL, '3329013101210001', 1, 0, '06', '02', '', '', '', 0, '2021-01-31', 'L', NULL, 41165, 29, 41173, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '1.00', '20.00', '1 th, 8 bln', 0, 0, 11),
(381, '', NULL, 0, '0c7629bf6d87485a9170998efa13d9', 'ANDHRA ABYAN ATHAR', NULL, '3329012612200001', 1, 0, '07', '02', '', '', '', 0, '2020-12-26', 'L', NULL, 41165, 29, 41173, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '1.00', '21.00', '1 th, 9 bln', 0, 0, 11),
(382, '', NULL, 0, 'b017b5e37c994adfb9aa19d78c0532', 'M DEFIN ARIZKI', NULL, '3329011106210001', 1, 0, '06', '02', '', '', '', 0, '2021-06-11', 'L', NULL, 41165, 29, 41173, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '1.00', '15.00', '1 th, 3 bln', 0, 0, 11),
(383, '', NULL, 0, 'fbd5ba015cea499fbd86430e6bdfb0', 'NAILA KHOIRUL M', NULL, '3329015712200001', 1, 0, '06', '02', '', '', '', 0, '2020-12-17', 'P', NULL, 41165, 29, 41173, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '1.00', '21.00', '1 th, 9 bln', 0, 0, 11),
(384, '', NULL, 0, '3415663752e64b0db926b61adc8404', 'SULTAN ELFANSYAH P', NULL, '3329012609200002', 1, 0, '07', '02', '', '', '', 0, '2020-09-26', 'L', NULL, 41165, 29, 41173, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '2.00', '24.00', '2 th, 0 bln', 0, 0, 11),
(385, '', NULL, 0, 'eb42950bf56942d989de5a38e6c3e4', 'NADHIFA MEHRA N', NULL, '3329016009200001', 1, 0, '06', '02', '', '', '', 0, '2020-09-20', 'L', NULL, 41165, 29, 41173, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '2.00', '24.00', '2 th, 0 bln', 0, 0, 11),
(386, '', NULL, 0, '0e8d99f73c8845a5bc03e784cfab00', 'KENZO DIRGANTARA', NULL, '3329010512200001', 1, 0, '06', '02', '', '', '', 0, '2020-09-03', 'L', NULL, 41165, 29, 41173, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '2.00', '25.00', '2 th, 1 bln', 0, 0, 11),
(387, '', NULL, 0, '63a6189ea3b8476fbb5d2c1ad84edb', 'M. ALVARO JULIAN', NULL, '3329011607200002', 1, 0, '07', '02', '', '', '', 0, '2020-07-16', 'L', NULL, 41165, 29, 41173, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '2.00', '26.00', '2 th, 2 bln', 0, 0, 11),
(388, '', NULL, 0, 'fa88124d42634f1cabc3245ea04737', 'SORAYA MEIRA IBADI', NULL, '3329016605200002', 1, 0, '08', '02', '', '', '', 0, '2020-05-26', 'L', NULL, 41165, 29, 41173, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '2.00', '28.00', '2 th, 4 bln', 0, 0, 11),
(389, '', NULL, 0, 'fae83be830e34f04a588310dcc005e', 'RAFKA ADHI PRATAMA', NULL, '3329010805200002', 1, 0, '07', '02', '', '', '', 0, '2020-05-08', 'L', NULL, 41165, 29, 41173, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '2.00', '29.00', '2 th, 5 bln', 0, 0, 11),
(390, '', NULL, 0, '73d68cbce453459d870d475994deb7', 'AISYAH APRILIA R', NULL, '3329016304200003', 1, 0, '06', '02', '', '', '', 0, '2020-04-24', 'P', NULL, 41165, 29, 41173, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '2.00', '29.00', '2 th, 5 bln', 0, 0, 11),
(391, '', NULL, 0, '3746c7b363524484823205442bc1b1', 'M. ARDA AFRIZA', NULL, '3329012104200001', 1, 0, '06', '02', '', '', '', 0, '2020-04-21', 'L', NULL, 41165, 29, 41173, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '2.00', '29.00', '2 th, 5 bln', 0, 0, 11),
(392, '', NULL, 0, '91f7f43fd6e24929905c0c3c8105c8', 'TALITHA HASNA H', NULL, '3329015904200002', 1, 0, '06', '02', '', '', '', 0, '2020-04-19', 'P', NULL, 41165, 29, 41173, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '2.00', '29.00', '2 th, 5 bln', 0, 0, 11),
(400, '', NULL, 0, 'd1d37870cc324b4fafe42662cf05db', 'Lansia', NULL, 'Lansia', 1, 0, '1', '1', '', '', '', 0, '1970-01-01', 'L', NULL, 41165, 29, 41183, 0, 0, NULL, NULL, 'Lajang', 'Brebe', '52.00', '633.00', '52 th, 9 bln', 0, 0, 397),
(399, '', NULL, 0, '988928f359124ffa8381d95cbdae63', 'Balita', NULL, '1212121', 1, 0, '1', '1', '', '', '', 0, '2019-01-01', 'L', NULL, 41165, 29, 41183, 0, 0, NULL, NULL, 'Lajang', 'Brebe', '3.00', '45.00', '3 th, 9 bln', 0, 0, 397),
(398, 'pbkdf2_sha256$260000$XE1AoOb3qZGLvydiKypLj6$poiPyt5nWEdMiII2eKwTgkRzZxL7J1yKdrFU3/AI8U4=', '2022-10-18 13:22:49.719099', 0, 'bidan2', 'Bu Bidan 2', NULL, '1212121212121', 1, 1, '1', '1', '', '', '', 0, '1998-01-01', 'P', NULL, 41165, 29, 41183, 0, 0, NULL, NULL, 'Lajang', 'Brebes', '24.00', '297.00', '24 th, 9 bln', 0, 0, 1);
INSERT INTO `user_user` (`id`, `password`, `last_login`, `is_superuser`, `username`, `name`, `mother_name`, `nik`, `is_active`, `is_staff`, `rt`, `rw`, `mobile_number`, `picture`, `ktp`, `from_web`, `dob`, `gender`, `education`, `kabupaten_id`, `kecamatan_id`, `kelurahan_id`, `order`, `print_counter`, `provinsi_id`, `job`, `marital_status`, `place_of_birth`, `age_year`, `age_month`, `age_string`, `is_pregnant`, `is_breeding`, `parent_id`) VALUES
(397, 'pbkdf2_sha256$260000$QQ3Flxx4oPErgqrTBy9r4T$K49yPOJMo95UpuGq56gVG9Z2WGjmLWBfV4MJev89TsQ=', '2022-10-18 13:19:09.493925', 0, 'bidan1', 'Bu Bidan 1', NULL, '1223232323232', 1, 1, '1', '1', '', '', '', 0, '1990-01-01', 'P', NULL, 41165, 29, 41183, 0, 0, NULL, NULL, 'Lajang', 'Brebes', '32.00', '393.00', '32 th, 9 bln', 0, 0, 1),
(401, '', NULL, 0, '168e3da5fae84b899e0e26cfe932f4', 'DARKONI', NULL, '332 901 010 753 0026', 1, 0, '01', '01', '', '', '', 0, '1953-07-01', 'L', NULL, 41165, 29, 41170, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '69.00', '832.00', '69 th, 4 bln', 0, 0, 9),
(402, '', NULL, 0, '8a1d8fdbfbd04c0f81e470732fea4a', 'SUPARTA', NULL, '332 901 060 757 0002', 1, 0, '02', '01', '', '', '', 0, '1957-07-06', 'L', NULL, 41165, 29, 41170, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '65.00', '784.00', '65 th, 4 bln', 0, 0, 9),
(403, '', NULL, 0, 'abdc84fc05084d5c93619c960cba9e', 'DASIH', NULL, '332 901 460 851 0002', 1, 0, '02', '01', '', '', '', 0, '1951-08-06', 'P', NULL, 41165, 29, 41170, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '71.00', '855.00', '71 th, 3 bln', 0, 0, 9),
(404, '', NULL, 0, '0133622fcbed4e2f97d035a8ca9c74', 'WARSAH', NULL, '332 901 420 961 0002', 1, 0, '03', '01', '', '', '', 0, '1961-09-02', 'P', NULL, 41165, 29, 41170, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '61.00', '734.00', '61 th, 2 bln', 0, 0, 9),
(405, '', NULL, 0, 'c86c7c0212b04e129e461ac94911d9', 'RASTI', NULL, '332 901 430 253 0004', 1, 0, '03', '01', '', '', '', 0, '1953-02-03', 'P', NULL, 41165, 29, 41170, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '69.00', '837.00', '69 th, 9 bln', 0, 0, 9),
(406, '', NULL, 0, '0d17d753edc44b5f8eddbc8e70c5be', 'WIHANTA', NULL, '332 901 110 460 0001', 1, 0, '03', '01', '', '', '', 0, '1960-04-11', 'L', NULL, 41165, 29, 41170, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '62.00', '750.00', '62 th, 6 bln', 0, 0, 9),
(407, '', NULL, 0, '077f197a01e1429c804a2a85a0c59f', 'DASNO', NULL, '332 901 220 255 0001', 1, 0, '03', '01', '', '', '', 0, '1955-02-22', 'L', NULL, 41165, 29, 41170, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '67.00', '812.00', '67 th, 8 bln', 0, 0, 9),
(408, '', NULL, 0, 'c905fa80b6864077be4f35f2117cb5', 'KUSNADI', NULL, '332 901 020 151 0001', 1, 0, '03', '01', '', '', '', 0, '1951-01-02', 'L', NULL, 41165, 29, 41170, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '71.00', '862.00', '71 th, 10 bln', 0, 0, 9),
(409, '', NULL, 0, '6a84dc6aacf742c884454fea0c83bb', 'TARNI', NULL, '332 901 520 257 0004', 1, 0, '03', '01', '', '', '', 0, '1957-02-12', 'P', NULL, 41165, 29, 41170, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '65.00', '788.00', '65 th, 8 bln', 0, 0, 9),
(410, '', NULL, 0, 'e77d6c6f11d54c86a4839fb6a6f818', 'WARKINI', NULL, '332 901 500 861 0004', 1, 0, '03', '01', '', '', '', 0, '1961-08-10', 'P', NULL, 41165, 29, 41170, 0, 0, NULL, NULL, 'Lajang', 'BREBES', '61.00', '734.00', '61 th, 2 bln', 0, 0, 9);

-- --------------------------------------------------------

--
-- Table structure for table `user_user_groups`
--

CREATE TABLE `user_user_groups` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_user_user_permissions`
--

CREATE TABLE `user_user_user_permissions` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `auth_group`
--
ALTER TABLE `auth_group`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_group_permissions_group_id_permission_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  ADD KEY `auth_group_permissions_group_id_b120cbf9` (`group_id`),
  ADD KEY `auth_group_permissions_permission_id_84c5c92e` (`permission_id`);

--
-- Indexes for table `auth_permission`
--
ALTER TABLE `auth_permission`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_permission_content_type_id_codename_01ab375a_uniq` (`content_type_id`,`codename`),
  ADD KEY `auth_permission_content_type_id_2f476e4b` (`content_type_id`);

--
-- Indexes for table `django_admin_log`
--
ALTER TABLE `django_admin_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `django_admin_log_content_type_id_c4bce8eb` (`content_type_id`),
  ADD KEY `django_admin_log_user_id_c564eba6` (`user_id`);

--
-- Indexes for table `django_content_type`
--
ALTER TABLE `django_content_type`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `django_content_type_app_label_model_76bd3d3b_uniq` (`app_label`,`model`);

--
-- Indexes for table `django_migrations`
--
ALTER TABLE `django_migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `django_session`
--
ALTER TABLE `django_session`
  ADD PRIMARY KEY (`session_key`),
  ADD KEY `django_session_expire_date_a5c62663` (`expire_date`);

--
-- Indexes for table `django_site`
--
ALTER TABLE `django_site`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `django_site_domain_a2e37b91_uniq` (`domain`);

--
-- Indexes for table `django_summernote_attachment`
--
ALTER TABLE `django_summernote_attachment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kabupaten_kabupaten`
--
ALTER TABLE `kabupaten_kabupaten`
  ADD PRIMARY KEY (`id`),
  ADD KEY `kabupaten_kabupaten_provinsi_id_fd3548f0` (`provinsi_id`);

--
-- Indexes for table `kecamatan_kecamatan`
--
ALTER TABLE `kecamatan_kecamatan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `kecamatan_kecamatan_kabupaten_id_a84efc27` (`kabupaten_id`);

--
-- Indexes for table `kelurahan_kelurahan`
--
ALTER TABLE `kelurahan_kelurahan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `kelurahan_kelurahan_kecamatan_id_4268ba9f` (`kecamatan_id`);

--
-- Indexes for table `provinsi_provinsi`
--
ALTER TABLE `provinsi_provinsi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sequences_sequence`
--
ALTER TABLE `sequences_sequence`
  ADD PRIMARY KEY (`name`);

--
-- Indexes for table `user_printcard`
--
ALTER TABLE `user_printcard`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_printcard_user_id_c4422e21_uniq` (`user_id`);

--
-- Indexes for table `user_user`
--
ALTER TABLE `user_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `nik` (`nik`),
  ADD KEY `user_user_kabupaten_id_d495e58f` (`kabupaten_id`),
  ADD KEY `user_user_kecamatan_id_f056b9af` (`kecamatan_id`),
  ADD KEY `user_user_kelurahan_id_1dcd92a1` (`kelurahan_id`),
  ADD KEY `user_user_provinsi_id_ea7fe129` (`provinsi_id`),
  ADD KEY `user_user_parent_id_ee02bf5a` (`parent_id`);

--
-- Indexes for table `user_user_groups`
--
ALTER TABLE `user_user_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_user_groups_user_id_group_id_bb60391f_uniq` (`user_id`,`group_id`),
  ADD KEY `user_user_groups_user_id_13f9a20d` (`user_id`),
  ADD KEY `user_user_groups_group_id_c57f13c0` (`group_id`);

--
-- Indexes for table `user_user_user_permissions`
--
ALTER TABLE `user_user_user_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_user_user_permissions_user_id_permission_id_64f4d5b8_uniq` (`user_id`,`permission_id`),
  ADD KEY `user_user_user_permissions_user_id_31782f58` (`user_id`),
  ADD KEY `user_user_user_permissions_permission_id_ce49d4de` (`permission_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `auth_group`
--
ALTER TABLE `auth_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `auth_permission`
--
ALTER TABLE `auth_permission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- AUTO_INCREMENT for table `django_admin_log`
--
ALTER TABLE `django_admin_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=311;

--
-- AUTO_INCREMENT for table `django_content_type`
--
ALTER TABLE `django_content_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `django_migrations`
--
ALTER TABLE `django_migrations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT for table `django_site`
--
ALTER TABLE `django_site`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `django_summernote_attachment`
--
ALTER TABLE `django_summernote_attachment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `kabupaten_kabupaten`
--
ALTER TABLE `kabupaten_kabupaten`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41166;

--
-- AUTO_INCREMENT for table `kecamatan_kecamatan`
--
ALTER TABLE `kecamatan_kecamatan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `kelurahan_kelurahan`
--
ALTER TABLE `kelurahan_kelurahan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41480;

--
-- AUTO_INCREMENT for table `provinsi_provinsi`
--
ALTER TABLE `provinsi_provinsi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32677;

--
-- AUTO_INCREMENT for table `user_printcard`
--
ALTER TABLE `user_printcard`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_user`
--
ALTER TABLE `user_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=411;

--
-- AUTO_INCREMENT for table `user_user_groups`
--
ALTER TABLE `user_user_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_user_user_permissions`
--
ALTER TABLE `user_user_user_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
