from django.apps import AppConfig


class KecamatanConfig(AppConfig):
    name = 'kartanu.apps.kecamatan'
