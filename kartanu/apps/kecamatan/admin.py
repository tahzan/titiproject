from django.contrib import admin
from import_export.admin import ImportExportModelAdmin
from .models import Kecamatan


class CustomAdmin(ImportExportModelAdmin, admin.ModelAdmin):
    search_fields = ('name', 'kabupaten__name')
    # list_display = ('name', 'dapil')
    # list_filter = ('dapil', 'kabupaten_kota')
    # autocomplete_fields = (
    #     'coordinator', 'previous_coordinator', 'inputer_suara_caleg',
    #     'inputer_suara_partai', 'kabupaten_kota')

admin.site.register(Kecamatan, CustomAdmin)
