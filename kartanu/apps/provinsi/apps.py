from django.apps import AppConfig


class ProvinsiConfig(AppConfig):
    name = 'kartanu.apps.provinsi'
