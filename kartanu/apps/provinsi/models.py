from django.db import models


class Provinsi(models.Model):
    name = models.CharField(max_length=50)
    code = models.IntegerField(null=True, default=0)

    def __str__(self):
        return self.name
