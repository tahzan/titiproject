from django.contrib import admin

# Register your models here.

from .models import Provinsi
from import_export.admin import ImportExportModelAdmin


class CustomAdmin(ImportExportModelAdmin, admin.ModelAdmin):
    search_fields = ('name',)

admin.site.register(Provinsi, CustomAdmin)
