from django.db import models
# Create your models here.


class Kelurahan(models.Model):
    name = models.CharField(max_length=254)
    code = models.CharField(max_length=20, null=True, default="0")
    kecamatan = models.ForeignKey(
        'kecamatan.Kecamatan', on_delete=models.CASCADE)

    def __str__(self):
        return self.name
