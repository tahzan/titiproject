from django.apps import AppConfig


class KelurahanConfig(AppConfig):
    name = 'kartanu.apps.kelurahan'
