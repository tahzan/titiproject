# Generated by Django 3.2 on 2022-09-03 07:02

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('user', '0014_user_is_pregnant'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='is_breeding',
            field=models.BooleanField(default=False, verbose_name='is_breeding'),
        ),
    ]
