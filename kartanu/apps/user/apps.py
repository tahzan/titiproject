from django.apps import AppConfig


class UserConfig(AppConfig):
    name = 'kartanu.apps.user'
