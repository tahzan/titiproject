import random
import string
import uuid
from datetime import date, datetime, timedelta

from django.apps.registry import apps
from django.contrib.auth.models import (AbstractBaseUser, PermissionsMixin,
                                        UserManager)
from django.conf import settings
from django.db import models
from django.db.models import Q
from django.utils import timezone
from kartanu.core.utils import FilenameGenerator
from kartanu.core.middleware import get_kecamatan_id, check_is_superuser, is_login
from kartanu.apps.kabupaten.models import Kabupaten
from kartanu.apps.provinsi.models import Provinsi

from model_utils import Choices
from sequences import get_next_value


class CustomUserManager(UserManager):
    def create_user(self, username, password, **extra_fields):
        now = timezone.now()
        user = self.model(
            username=username,
            is_active=True, is_superuser=False,
            last_login=now,  **extra_fields)

        if password:
            user.set_password(password)
        user.save(using=self._db)

        return user

    def create_superuser(self, username, password, **extra_fields):
        user = self.create_user(username, password, **extra_fields)
        user.is_staff = True
        user.is_active = True
        user.is_superuser = True
        user.save(using=self._db)
        return user


    def get_queryset(self):
        return super(CustomUserManager, self).get_queryset().filter(
            is_active=True
        )


class User(AbstractBaseUser, PermissionsMixin):
    username = models.CharField(max_length=50, unique=True, db_index=True)
    name = models.CharField(max_length=50, null=True)
    mother_name = models.CharField(max_length=50, null=True)
    nik = models.CharField(max_length=50, unique=True, null=True)
    is_active = models.BooleanField('active', default=True)
    is_staff = models.BooleanField('Status Staff', default=False)
    provinsi = models.ForeignKey(
        'provinsi.Provinsi', on_delete=models.PROTECT, blank=True, null=True)
    kabupaten = models.ForeignKey(
        'kabupaten.Kabupaten', on_delete=models.PROTECT, blank=True, null=True)
    kecamatan = models.ForeignKey(
        'kecamatan.Kecamatan', on_delete=models.PROTECT, null=True)
    kelurahan = models.ForeignKey(
        'kelurahan.Kelurahan', on_delete=models.PROTECT, null=True)
    rt = models.CharField(max_length=50, blank=True, null=True)
    rw = models.CharField(max_length=50, blank=True, null=True)
    mobile_number = models.CharField(max_length=30, default='', blank=True)
    picture = models.ImageField(upload_to=FilenameGenerator(prefix='user'), default='', blank=True)
    ktp = models.ImageField(upload_to=FilenameGenerator(prefix='user'), default='', blank=True)
    place_of_birth = models.CharField(max_length=50, null=True)
    from_web = models.BooleanField(default=False)
    dob = models.DateField(blank=True, null=True)
    GENDER = Choices(("L", "Laki Laki"), ("P", "Perempuan"))
    gender = models.CharField(
        max_length=10, choices=GENDER)
    MARITAL_STATUS = Choices(("Kawin", "Kawin"), ("Lajang", "Lajang"))
    marital_status = models.CharField(
        max_length=10, choices=MARITAL_STATUS, default="Lajang")
    job = models.CharField(max_length=20, blank=True, null=True)
    age_year = models.DecimalField(max_digits=50, decimal_places=2,  default=0)
    age_month = models.DecimalField(max_digits=50, decimal_places=2, default=0)
    age_string = models.CharField(max_length=250, null=True)
    EDUCATION = Choices(
        ("SD", "SD"),
        ("SMP", "SMP"),
        ("SLTP", "SLTP"),
        ("SMA", "SMA"),
        ("SLTA", "SLTA"),
        ("D3", "D3"),
        ("S1", "S1"),
        ("S2", "S2"),
        ("S3", "S3"),
        ("Lainnya", "Lainnya")
    )
    education = models.CharField(max_length=255, blank=True, null=True, choices=EDUCATION)
    order = models.IntegerField(default=0, blank=True)
    print_counter = models.IntegerField(default=0)
    is_pregnant = models.BooleanField('is_pregnant', default=False)
    is_breeding = models.BooleanField('is_breeding', default=False)
    parent = models.ForeignKey('User', on_delete=models.CASCADE, null=True)

    USERNAME_FIELD = 'username'
    objects = CustomUserManager()

    class Meta:
        verbose_name = 'user'
        verbose_name_plural = 'users'

    def __str__(self):
        return '%s - %s' % (self.username, self.name)

    def __unicode__(self):
        return self.name or self.username or 'User #%d' % (self.id)
    
    def calculate_age(self):
        if not self.dob:
            return 0
        today = date.today()
        born = self.dob
        try: 
            birthday = born.replace(year=today.year)
        except ValueError: # raised when birth date is February 29 and the current year is not a leap year
            birthday = born.replace(year=today.year, month=born.month+1, day=1)
        if birthday > today:
            return today.year - born.year - 1
        else:
            return today.year - born.year
    
    def calculate_month(self):
        if not self.dob:
            return 0
        today = date.today()
        born = self.dob
        time_difference = today - born
        D = time_difference.days
        M = int((D/365)*12)
        return M

    def save(self, *args, **kwargs):
        self.age_month = self.calculate_month()
        self.age_year = self.calculate_age()

        if self.age_month:
            self.age_string = f"{self.age_year} th, {self.age_month - self.age_year * 12} bln"  
        else:
            self.age_string = "0th, 0bln"
                  
        super(User, self).save(*args, **kwargs)

    # def save(self, *args, **kwargs):
    #     exist_id = self.id
    #     super(User, self).save(*args, **kwargs)
    #     self.kabupaten = Kabupaten.objects.first()
    #     self.provinsi = Provinsi.objects.first()
    #     if not exist_id:
    #         self.order = get_next_value("order_user")
    #         last_counter = get_next_value(self.kelurahan.code) + 1
    #         self.id_card_number = f"{self.kelurahan.code}-{str(last_counter).zfill(5)}"
    #         PrintCard.objects.create(user=self, order=self.order)
    #     self.save()


class PrintCard(models.Model):
    user = models.ForeignKey(
        'user.User', on_delete=models.CASCADE, unique=True)
    order = models.IntegerField(default=0)

