from django.apps import AppConfig


class KabupatenConfig(AppConfig):
    name = 'kartanu.apps.kabupaten'
