from django.db import models

# Create your models here.


class Kabupaten(models.Model):
    code = models.IntegerField(null=True, default=0)
    name = models.CharField(max_length=50)
    provinsi = models.ForeignKey('provinsi.Provinsi', on_delete=models.CASCADE)

    def __str__(self):
        return self.name
