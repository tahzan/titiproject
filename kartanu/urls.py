from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static
from django.conf.urls import url

from kartanu.apps.kecamatan.models import Kecamatan
from kartanu.apps.kelurahan.models import Kelurahan
from kartanu.apps.kabupaten.models import Kabupaten
from kartanu.backoffice.autocomplete import (
    KecamatanAutocomplete,
    KelurahanAutocomplete,
    KabupatenAutocomplete,
)


admin.site.site_header = "kartanu Admin"
admin.site.site_title = "kartanu Admin Portal"
admin.site.index_title = "Welcome to kartanu Admin Portal"

urlpatterns = [
    path('my-admin/', admin.site.urls),
    path('', include('kartanu.backoffice.urls', namespace='backoffice')),
    url("kabupaten-autocomplete/$", KabupatenAutocomplete.as_view(model=Kabupaten),
        name='kabupaten-autocomplete',),
    url("kecamatan-autocomplete/$", KecamatanAutocomplete.as_view(model=Kecamatan),
        name='kecamatan-autocomplete',),
    url("kelurahana-utocomplete/$", KelurahanAutocomplete.as_view(model=Kelurahan),
        name='kelurahan-autocomplete',),

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

# di server saat settings debug true
if settings.DEBUG:
    urlpatterns + static(settings.STATIC_URL,
                         document_root=settings.STATIC_ROOT)
