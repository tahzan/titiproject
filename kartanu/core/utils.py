import os
from django.conf import settings
from slugify import slugify
from django.utils import timezone
from time import time

class FilenameGenerator(object):

    def __init__(self, prefix):
        self.prefix = prefix

    def __call__(self, instance, filename):
        filepath = os.path.basename(filename)
        filename, extension = os.path.splitext(filepath)
        filename = slugify(filename)

        path = "/".join([
            'static_files',
            'backuped',
            self.prefix,
            str(instance.kecamatan.name),
            str(instance.kelurahan.name),
            filename + extension
        ])
        return path

try:
    from django.utils.deconstruct import deconstructible
    FilenameGenerator = deconstructible(FilenameGenerator)
except ImportError:
    pass



def custom_slugify(string):
    string = "%s-%s" % (string, str(time())[11:])
    return slugify(string)
