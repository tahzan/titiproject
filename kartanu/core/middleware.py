from threading import local
THREAD_LOCALS = local()


def set_current_user(user):
    THREAD_LOCALS.user = user


def get_current_request():
    """ returns the request object for this thread """
    return getattr(THREAD_LOCALS, "request", None)


def get_current_user():
    if hasattr(THREAD_LOCALS, 'user'):
        return getattr(THREAD_LOCALS, 'user', None)
    else:
        """ returns the current user, if exist, otherwise returns None """
        request = get_current_request()
        if request:
            return getattr(request, "user", None)


def get_kecamatan_id():
    if get_current_user() is not None:
        try:
            user = get_current_user()
            return user.kecamatan_id
        except Exception as e:
            print(e)
            pass
    return None


def check_is_superuser():
    if get_current_user() is not None:
        try:
            user = get_current_user()
            return user.is_superuser
        except Exception as e:
            pass
    return False


def is_staff():
    if get_current_user() is not None:
        try:
            user = get_current_user()
            return user.is_staff
        except Exception as e:
            pass
    return False

def is_login():
    if get_current_user() is not None:
        return True
    return False


class SimpleMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response
        # One-time configuration and initialization.

    def __call__(self, request):
        # Code to be executed for each request before
        # the view (and later middleware) are called.
        THREAD_LOCALS.request = request
        set_current_user(getattr(request, 'user', None))

        response = self.get_response(request)

        # Code to be executed for each request/response after
        # the view is called.

        return response

# def simple_middleware(get_response):
#     def middleware(request):
#         # THREAD_LOCALS.request = request
#         # set_current_user(getattr(request, 'user', None))
#         response = get_response(request)
#         return response
#     return middleware
