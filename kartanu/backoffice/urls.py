from django.urls import path, include
from .views import *

app_name = "backoffice"
urlpatterns = [
    path('', dashboard, name='dashboard'),
    path('user', users, name='users'),
    path('add-user/', add_user, name='add_user'),
    path('<int:id>/edit/', edit_user, name='edit_user'),
    path('<int:id>/preview/', user_preview, name='user_preview'),
    path('login/', login_view, name='login'),
    path('log-out/', log_out, name='log_out'),
    path('bayi', bayi, name='bayi'),
    path('balita', balita, name='balita'),
    path('remaja', remaja, name='remaja'),
    path('lansia', lansia, name='lansia'),
    path('ibu-hamil', hamil, name='hamil'),
    path('ibu-menyusui', menyusui, name='menyusui'),
    path('export-csv', export_csv, name='export_csv'),
    path("change-password", change_password, name="change_password"),
    path("<int:id>/delete", delete, name="delete"),
]
