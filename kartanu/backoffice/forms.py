from django import forms
from django.contrib import auth
from django.forms.forms import BaseForm
from dal import autocomplete
from kartanu.apps.user.models import User
import uuid


class BaseUserForm(forms.ModelForm):
    nik = forms.CharField(widget=forms.TextInput(
        attrs={'class': 'form-control'}), required=False)
    username = forms.CharField(widget=forms.TextInput(
        attrs={'class': 'form-control'}), required=False)
    password = forms.CharField(widget=forms.TextInput(
        attrs={'class': 'form-control'}), required=False)
    place_of_birth = forms.CharField(widget=forms.TextInput(
        attrs={'class': 'form-control'}))
    dob = forms.DateField(widget=forms.TextInput(
        attrs={'class': 'form-control', 'type':'date'}))
    name = forms.CharField(widget=forms.TextInput(
        attrs={'class': 'form-control', }))
    gender = forms.ChoiceField(widget=forms.Select(
        attrs={'class': 'form-control'}), choices=[("L", "Laki2"), ("P", "Perempuan")])
    
    class Meta:
        model = User
        fields = (
            'name',
            'nik',
            'gender',
            'dob',
            'place_of_birth',
            'kabupaten',
            'kecamatan',
            'kelurahan',
            'rt',
            'rw',
            'is_pregnant',
            'is_breeding',
            'is_staff',
            'username',
            'password'
        )
        widgets = {
            'kabupaten': autocomplete.ModelSelect2(url='kabupaten-autocomplete', forward=('provinsi',), attrs={'data-placeholder': 'Pilih Kabupaten', 'class': 'form-control', 'required': True},),
            'kecamatan': autocomplete.ModelSelect2(url='kecamatan-autocomplete', forward=('kabupaten',), attrs={'data-placeholder': 'Pilih Kecamatan', 'class': 'form-control', 'required': True},),
            'kelurahan': autocomplete.ModelSelect2(url='kelurahan-autocomplete', forward=('kecamatan',), attrs={'data-placeholder': 'Pilih Kelurahan', 'class': 'form-control', 'required': True}),
        }
        # initial_fields = ['kecmatan']

    def __init__(self, *args, **kwargs):
        super(BaseUserForm, self).__init__(*args, **kwargs)

    
    def clean_username(self):
        username = self.cleaned_data['username']
        if not username or username == "None":
            username = uuid.uuid4().hex[:30]
        return username


class UserCreateForm(BaseUserForm):

    def __init__(self, *args, **kwargs):
        super(UserCreateForm, self).__init__(*args, **kwargs)


    def save(self, *args, **kwargs):
        user = super(UserCreateForm, self).save(*args, **kwargs)
        
        username = self.cleaned_data['username']
        if not username or username == "None":
            user.username = uuid.uuid4().hex[:30]

        if self.cleaned_data['password']:
            user.set_password(self.cleaned_data['password'])

        user.save()
        return user


# class UserEditForm(BaseUserForm):

#     def __init__(self, *args, **kwargs):
#         super(UserEditForm, self).__init__(*args, **kwargs)
#         self.user = kwargs['instance']

#     def save(self, *args, **kwargs):
#         self.user.save()
#         return self.user


class LoginForm(auth.forms.AuthenticationForm):
    username = forms.CharField(max_length=30)
    password = forms.CharField(max_length=30)

    def clean(self):
        return super(LoginForm, self).clean()
