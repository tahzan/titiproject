import csv, io, os
from django.core.files import File
from django.apps.registry import apps
from django.contrib import messages
from django.contrib.auth import login, logout
from django.shortcuts import redirect
from django.template.response import TemplateResponse
from kartanu.apps.user.models import User, PrintCard
from kartanu.apps.kecamatan.models import Kecamatan
from kartanu.apps.kelurahan.models import Kelurahan
from kartanu.apps.user.decorators import login_validate
from kartanu.backoffice.forms import LoginForm, UserCreateForm
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from django.db.models import Q
from django.urls import reverse
from django.db import transaction
from django.http import JsonResponse
from datetime import date
from django.shortcuts import get_object_or_404
from django.http import HttpResponse
from django.contrib.auth.forms import PasswordChangeForm


get_model = apps.get_model

def login_view(request):
    auth_form = LoginForm(data=request.POST or None)
    if auth_form.is_valid():
        login(request, auth_form.get_user())
        auth_form.get_user()
        return redirect("backoffice:dashboard")
    else:
        messages.error(request, "Username and Password are incorrect")
    list(messages.get_messages(request))
    context = dict(auth_form=auth_form,)
    return TemplateResponse(request, "backoffice/login.html", context)


def log_out(request):
    logout(request)
    return redirect("backoffice:login")


@login_validate
def dashboard(request):
    users = User.objects.all().exclude(is_superuser=True)\
        .exclude(is_staff=True).filter(parent=request.user)

    for user in users:
        user.save()

    users = User.objects.all().exclude(is_superuser=True)\
        .exclude(is_staff=True).filter(parent=request.user)

    bayi_0_5 = users.filter(age_month__gte=0).filter(age_month__lte=5)
    bayi_6_11 = users.filter(age_month__gte=6).filter(age_month__lte=11)

    balita_12_24 = users.filter(age_month__gte=12).filter(age_month__lte=24)
    balita_25_35 = users.filter(age_month__gte=25).filter(age_month__lte=35)
    balita_36_60 = users.filter(age_month__gte=36).filter(age_month__lte=60)

    remaja_10_14 = users.filter(age_year__gte=10).filter(age_year__lte=14)
    remaja_15_14 = users.filter(age_year__gte=15).filter(age_year__lte=19)
    
    lansia_45_59 = users.filter(age_year__gte=45).filter(age_year__lte=59)
    lansia_60_69 = users.filter(age_year__gte=60).filter(age_year__lte=69)
    lansia_70_200 = users.filter(age_year__gte=70)


    hamil = users.filter(is_pregnant=True)
    menyusui = users.filter(is_breeding=True)

    context = dict(
        to_print = users.filter(print_counter=0).count(),
        users_count = users.count(),
        already_printed= users.filter(print_counter__gt=0).count(),
        hamil = hamil.count(),
        menyusui = menyusui.count(),
        bidan= request.user,
        bayi_0_5=bayi_0_5.count(),
        bayi_6_11=bayi_6_11.count(),
        balita_12_24 = balita_12_24.count(),
        balita_25_35 = balita_25_35.count(),
        balita_36_60 = balita_36_60.count(),
        remaja_10_14 = remaja_10_14.count(),
        remaja_15_14 = remaja_15_14.count(),
        lansia_45_59 = lansia_45_59.count(),
        lansia_60_69 = lansia_60_69.count(),
        lansia_70_200 = lansia_70_200.count()
    )
    return TemplateResponse(request, "backoffice/index.html", context)

@login_validate
def users(request):
    users = User.objects.all().exclude(is_superuser=True).filter(parent=request.user).order_by('age_month')

    page = request.GET.get("page")
    search = request.GET.get("search", "")
    
    if search:
        users = users.filter(Q(name__icontains=search) | Q(nik__icontains=search))
    
    counter = request.GET.get("counter", "")
    if counter:
        users = users.filter(print_counter=int(counter))

    results_per_page = 20
    
    paginator = Paginator(users, results_per_page)
    try:
        users = paginator.get_page(page)
    except PageNotAnInteger:
        users = paginator.get_page(1)
    except EmptyPage:
        users = paginator.get_page(paginator.num_pages)

    context = {
        "users": users,
        "title": "Users",
        "filter": {"search": search, "counter": counter},
    }
    return TemplateResponse(request, "backoffice/users.html", context)


@login_validate
def user_preview(request, id):
    user = User.objects.all().get(id=id)
    context = {
        "user": user,
    }
    return TemplateResponse(request, "backoffice/user-detail.html", context)


@login_validate
def add_user(request):
    form = UserCreateForm(request.POST or None, request.FILES or None)
    
    if form.is_valid():
        user = form.save()
        user.parent = request.user
        user.save()
        messages.success(request, 'success')
        return redirect(reverse('backoffice:users'))
    else:
        print(form.errors)

    context = dict(
        edit=False,
        form=form,
        title='Tambah Data'
    )
    return TemplateResponse(request, 'backoffice/add-user.html', context)


@login_validate
def edit_user(request, id):
    instance = get_object_or_404(User, pk=id)
    form = UserCreateForm(request.POST or None, request.FILES or None, instance=instance)
    
    if form.is_valid():
        form.save()
        messages.success(request, 'success')
        return redirect(reverse('backoffice:users'))
    else:
        print(form.errors)

    context = dict(
        edit=False,
        form=form,
        title='Edit Data'
    )
    return TemplateResponse(request, 'backoffice/add-user.html', context)

@login_validate
def bayi(request):
    users = User.objects.all().exclude(is_superuser=True)\
        .exclude(is_staff=True).filter(parent=request.user)
    users = users.filter(age_month__gte=0).filter(age_month__lte=11)

    page = request.GET.get("page")
    search = request.GET.get("search", "")

    if search:
        users = users.filter(
            Q(name__icontains=search) | Q(mobile_number__icontains=search) |
            Q(dealer_name__icontains=search)
        )

    gender = request.GET.get("gender", "")
    if gender:
       users = users.filter(gender=gender)

    age = request.GET.get("age")
    if age:
        filter_age = age.split("-")
        users = users.filter(age_month__gte=int(filter_age[0])).filter(age_month__lte=int(filter_age[1]))

    results_per_page = 20
   
    paginator = Paginator(users, results_per_page)
    try:
        users = paginator.get_page(page)
    except PageNotAnInteger:
        users = paginator.get_page(1)
    except EmptyPage:
        users = paginator.get_page(paginator.num_pages)

    context = {
        "users": users,
        "title": "Bayi",
        "filter_data": "Bayi",
        "filter": {
            "search": search, 
            "gender": gender,
            "age": age
        },
    }
    return TemplateResponse(request, "backoffice/bayi.html", context)


@login_validate
def balita(request):
    users = User.objects.all().exclude(is_superuser=True)\
        .exclude(is_staff=True).filter(parent=request.user)
    users = users.filter(age_month__gte=12).filter(age_month__lte=72)

    page = request.GET.get("page")
    search = request.GET.get("search", "")
    
    if search:
        users = users.filter(Q(name__icontains=search) | Q(nik__icontains=search))
    
    gender = request.GET.get("gender", "")
    if gender:
       users = users.filter(gender=gender)

    age = request.GET.get("age")
    if age:
        filter_age = age.split("-")
        users = users.filter(age_month__gte=int(filter_age[0])).filter(age_month__lte=int(filter_age[1]))
    
    results_per_page = 20
   
    paginator = Paginator(users, results_per_page)
    try:
        users = paginator.get_page(page)
    except PageNotAnInteger:
        users = paginator.get_page(1)
    except EmptyPage:
        users = paginator.get_page(paginator.num_pages)

    context = {
        "users": users,
        "title": "Balita",
        "filter_data": "balita",
       "filter": {
            "search": search, 
            "gender": gender,
            "age": age
        },
    }
    return TemplateResponse(request, "backoffice/balita.html", context)


@login_validate
def remaja(request):
    users = User.objects.all().exclude(is_superuser=True)\
        .exclude(is_staff=True).filter(parent=request.user)
    users = users.filter(age_year__gte=10).filter(age_year__lte=19)

    page = request.GET.get("page")
    search = request.GET.get("search", "")
    
    if search:
        users = users.filter(Q(name__icontains=search) | Q(nik__icontains=search))
    
    gender = request.GET.get("gender", "")
    if gender:
       users = users.filter(gender=gender)

    age = request.GET.get("age")
    if age:
        filter_age = age.split("-")
        users = users.filter(age_year__gte=int(filter_age[0])).filter(age_year__lte=int(filter_age[1]))
    

    results_per_page = 20
   
    paginator = Paginator(users, results_per_page)
    try:
        users = paginator.get_page(page)
    except PageNotAnInteger:
        users = paginator.get_page(1)
    except EmptyPage:
        users = paginator.get_page(paginator.num_pages)

    context = {
        "users": users,
        "title": "Remaja",
        "filter_data": "remaja",
        "filter": {
            "search": search, 
            "gender": gender,
            "age": age
        },
    }
    return TemplateResponse(request, "backoffice/remaja.html", context)


@login_validate
def lansia(request):
    users = User.objects.all().exclude(is_superuser=True)\
        .exclude(is_staff=True).filter(parent=request.user)
    users = users.filter(age_year__gte=45)

    page = request.GET.get("page")
    search = request.GET.get("search", "")
    
    gender = request.GET.get("gender", "")
    if gender:
       users = users.filter(gender=gender)

    age = request.GET.get("age")
    if age:
        filter_age = age.split("-")
        users = users.filter(age_year__gte=int(filter_age[0])).filter(age_year__lte=int(filter_age[1]))

    results_per_page = 20
   
    paginator = Paginator(users, results_per_page)
    try:
        users = paginator.get_page(page)
    except PageNotAnInteger:
        users = paginator.get_page(1)
    except EmptyPage:
        users = paginator.get_page(paginator.num_pages)

    context = {
        "users": users,
        "title": "Lansia",
        "filter_data": "lansia",
         "filter": {
            "search": search, 
            "gender": gender,
            "age": age
        },
    }
    return TemplateResponse(request, "backoffice/lansia.html", context)


@login_validate
def hamil(request):
    users = User.objects.all().exclude(is_superuser=True)\
        .exclude(is_staff=True).filter(parent=request.user)
    users = users.filter(is_pregnant=True)

    page = request.GET.get("page")
    search = request.GET.get("search", "")
    
    if search:
        users = users.filter(Q(name__icontains=search) | Q(nik__icontains=search))
    
    counter = request.GET.get("counter", "")
    if counter:
        users = users.filter(print_counter=int(counter))

    results_per_page = 20
   
    paginator = Paginator(users, results_per_page)
    try:
        users = paginator.get_page(page)
    except PageNotAnInteger:
        users = paginator.get_page(1)
    except EmptyPage:
        users = paginator.get_page(paginator.num_pages)

    context = {
        "users": users,
        "title": "Ibu Hamil",
        "filter_data": "hamil",
        "filter": {"search": search, "counter": counter},
    }
    return TemplateResponse(request, "backoffice/tabel.html", context)


@login_validate
def menyusui(request):
    users = User.objects.all().exclude(is_superuser=True)\
        .exclude(is_staff=True).filter(parent=request.user)
    users = users.filter(is_breeding=True)

    page = request.GET.get("page")
    search = request.GET.get("search", "")
    
    if search:
        users = users.filter(Q(name__icontains=search) | Q(nik__icontains=search))
    
    counter = request.GET.get("counter", "")
    if counter:
        users = users.filter(print_counter=int(counter))

    results_per_page = 20
   
    paginator = Paginator(users, results_per_page)
    try:
        users = paginator.get_page(page)
    except PageNotAnInteger:
        users = paginator.get_page(1)
    except EmptyPage:
        users = paginator.get_page(paginator.num_pages)

    context = {
        "users": users,
        "title": "Ibu Menyusui",
        "filter_data": "menyusui",
        "filter": {"search": search, "counter": counter},
    }
    return TemplateResponse(request, "backoffice/tabel.html", context)


@login_validate
def change_password(request):
    user = request.user
    form = PasswordChangeForm(data=request.POST or None, user=user)
    if request.method == 'POST':
        if form.is_valid():
            form.save()
            return redirect('backoffice:users')

    context = dict(
        form=form,
        title='Change password'
    )
    return TemplateResponse(request, 'backoffice/change_password.html', context)


@login_validate
def delete(request, id):
    user = get_object_or_404(User, id=id)
    try:
        user.delete()
    except:
        user.is_active = False
        user.save()

    return redirect('backoffice:users')


def export_csv(request):
    users = User.objects.all().order_by("id").exclude(is_superuser=True).exclude(is_staff=True).filter(parent=request.user)
    filter_param = request.GET['data']
    if filter_param == "balita":
        users = users.filter(age_month__gte=0).filter(age_month__lte=72)
    elif filter_param == "remaja":
        users = users.filter(age_year__gte=10).filter(age_year__lte=19)
    elif filter_param == "lansia":
        users = users.filter(age_year__gte=45)
    elif filter_param == "hamil":
        users = users.filter(is_pregnant=True)
    elif filter_param == "menyusui":
        users = users.filter(is_breeding=True)

    # Create the HttpResponse object with the appropriate CSV header.
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="csv_database_write.csv"'

    writer = csv.writer(response)
    writer.writerow(['nama', 'nik', 'rt/rw', 'kelurahan', 'kecamatan', 'kabupaten', 'tanggal lahir', 'umur'])

    for user in users:
        writer.writerow([
            user.name, user.nik,f"{user.rt}/{user.rw}", user.kelurahan, user.kecamatan,
            user.kabupaten, user.dob, user.age_string
        ])

    return response